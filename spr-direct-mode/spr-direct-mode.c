//********************************************************// 
//Test program for Smart Packet Radio
//Template to implement DIRECT mode
//Version 1: Transfers a test byte to the RFM22 
//		to be transmitted
//--------------------------------------------------------//
//Author(s): 
//	Samuel Pang 	     [slspang1990(at)gmail.com]
//	Nick D'Ademo 	     [nickdademo(at)gmail.com]
//--------------------------------------------------------//
//Code based off original "SmartPacketRadio.c"
//Author(s):
//	Jordan Guest 	     [jordannguest(at)gmail.com]
//	Nick D'Ademo 	     [nickdademo(at)gmail.com]
//	Benjamin Foo 	     [bsmf91(at)gmail(dot)com]
//	Felix Shzu-Jursachek [fjuraschek(at)gmail(dot)com]
//********************************************************// 

//=====================//
//Included header files//
//=====================//
#include <util/delay.h>				//For delays
#include <stdbool.h>				//For Boolean logic
// AVR headers
#include <avr/power.h>				//For clock prescaler
#include <avr/wdt.h>				//For AVR watchdog timers
#include <avr/interrupt.h>			//Enable interrupts
// SPI setup
#include "LUFA/Drivers/Peripheral/SPI.h"	//For SPI with peripheral devices

//=======//
//Defines//
//=======//
//LED1 colours
#define TURNLED1_OFF		PORTC |= 0x01; PORTE |= 0x03;
#define TOGGLELED1_RED 		PORTE ^= 0x01;
#define TOGGLELED1_GREEN	PORTE ^= 0x02;
#define TOGGLELED1_BLUE  	PORTC ^= 0x01;
#define TURNLED1_RED		TURNLED1_OFF; PORTE &= 0xFE; PORTC |= 0x01; 
#define TURNLED1_GREEN		TURNLED1_OFF; PORTE &= 0xFD; PORTC |= 0x01;
#define TURNLED1_BLUE		TURNLED1_OFF; PORTE |= 0x03; PORTC &= 0xFE;
#define TURNLED1_CYAN		TURNLED1_OFF; PORTE &= 0xFD; PORTC &= 0xFE;
#define TURNLED1_MAGENTA	TURNLED1_OFF; PORTE &= 0xFE; PORTC &= 0xFE;
#define TURNLED1_YELLOW		TURNLED1_OFF; PORTE &= 0xFC; PORTC |= 0x01; 

//================//
//Global variables//
//================//
int txBit;			//Bits for TX byte
int bitPos = 8; 		//Start at MSB
int ISRcount = 0; 		//Count in ISR
bool TX_BYTE_SENT = false;
unsigned char TestByte = 0xAA;

//===================//
//Function prototypes//
//===================//
void SetupHardware(void); 			//Initiallises hardware
void AVR_Init(void);				//Initiallises AVR microcontroller
void DirectMode_Setup(void);			//Initiallises direct mode on rfm22
void rfm22_init(void);				//Initiallises rfm22 module
void rfm22_write(uint8_t address, char data);	//Writes to the rfm22 registers using SPI

//************//
//MAIN PROGRAM//
//************//
int main(void)
{
    // Initiallize hardware
    SetupHardware();

    // Enable global interrupts
    sei();

    // Initiallize rfm22 module
    DirectMode_Setup();

    // Turn off SPI
    SPCR = 0;

    //~~Force PD0 high (diagnostic)~~
    PORTD |= 0x01; 

    // Forever loop
    for(;;)
    {	
        PORTD |= 0x04; //PD2
	if(TX_BYTE_SENT) { TURNLED1_GREEN; }

    /********************************************************/

    /* SPR waits until it receives something on the antenna */

    /********************************************************/

    }
}

//=========//
//Functions//
//=========//
//-------------------//
//Initialize hardware//
//-------------------//
void SetupHardware(void)
{
    // Disable watchdog if enabled by bootloader/fuses
    MCUSR &= ~(1 << WDRF);
    wdt_disable();

    // Disable clock division
    clock_prescale_set(clock_div_1);

    // Initiallize AVR
    AVR_Init();
    //Initiallize rfm22
    rfm22_init();
}

//------------------------------//
//Initialize AVR microcontroller//
//------------------------------//
void AVR_Init(void)
{
    // Initialises SPI for communication with RFM22B module
    SPI_Init(SPI_SPEED_FCPU_DIV_2 | SPI_ORDER_MSB_FIRST | SPI_SCK_LEAD_RISING | SPI_SAMPLE_LEADING | SPI_MODE_MASTER);

    // Initialize AVR ports
    //~~Port D is output port (for testing)~~
    DDRD |= 0xFF;

    // LED output port pins
    //Output pins for LED1(PE0 - R, PE1 - G, PC0 - B)
    DDRE |= 0x03;
    PORTE |= 0x03;
    DDRC |= 0x01;
    PORTC |= 0x01;

    // Serial communications pins
    //~~~NOTE: CLEAN UP THIS CODE LATER~~~//
    DDRB |= 0x04; 	//PB2 is MOSI line (output)
    PORTB |= 0x40;	//Set PB2 high
    DDRB |= 0x01;	//PB0 is SS (output)
    PORTB |= 0x01;	//SS will be high
    DDRB |= 0x10; 	//PB4 is SDN pin (output)

    PORTD |= 0x01; 	//TEST (PD0)
    PORTB &= !(0x10);   //Enables RFM22 chip

    // Interrupt pins
    DDRE |= 0x00;	//PE6 is input pin
    PORTE |= 0x40;	//Enable pull-up on PE6 to be used for intererr
    //Sets pin PE6 to receive neg edge interrupt from RFM22B module
    EICRA=0x00;	
    EICRB=0x20;		//INT6 to generate on falling edge	
    EIMSK=0x40;		//Enable INT6 interrupt
    EIFR=0x40;		//Clear interrupt flag for INT6
}

//-------------------------------------------------//
//Writes specified data to address on RFM22B module//
//-------------------------------------------------//
void rfm22_write(uint8_t address, char data)
{
    address |= 0x80;        // Sets MSB to 1 for write
    
    PORTB &= 0xFE;          // Selects RFM22B module by setting SS low
    PORTD &= 0xFE; //~~TEST (PD0)~~
    SPI_SendByte (address); // Sends address to write to
    SPI_SendByte (data);    // Writes data to selected address
    PORTB |= 0x01;          // Deselects module
    PORTD |= 0x01; //~~TEST (PD0)~~
}

//-------------------------------------//
//Initialize the RFM22 for transmitting//
//-------------------------------------//
void rfm22_init(void)
{
    //=======================//
    //Register initialisation//
    //=======================//
    //Address Read/Write - Function

    _delay_ms(50);
    rfm22_write(0x07, 0x81);  // Reset register values on RFM22B
    _delay_ms(50);

    //0x00 R - Device type
    //0x01 R - Device version
    //0x02 R - Device status
    //0x03 R - Interrupt status 1
    //0x04 R - Interrupt status 2
    //0x05 R/W - Interrupt Enable 1   
    rfm22_write(0x05, 0x00);          // Disable all interrupts
    //0x06 R/W - Interrupt Enable 2
    rfm22_write(0x06, 0x00);          // Disable all interrupts but RSSI/CCA
    //0x07 R/W - Operating function and control 2
    rfm22_write(0x07, 0x01);          // Set READY mode
    //0x08 R/W - Operating function and control 2
    //0x09 R/W - Crystal oscillator load capacitance        
    rfm22_write(0x09, 0x7F);          // Cap = 12.5pF
    //0x0A R/W - Microcontroller output clock
    rfm22_write(0x0A, 0x05);          // Clk output is 2MHz
    //0x0B R/W - GPIO_0 configuration       
    //rfm22_write(0x0B, 0xF4);          // GPIO0 is for RX data output
    //0x0C R/W - GPIO_1 configuration
    //rfm22_write(0x0C, 0xEF);          // GPIO1 is TX/RX data CLK output
    //0x0D R/W - GPIO_2 configuration        
    rfm22_write(0x0D, 0x00);              
    //0x0E R/W - I/O port configuration
    rfm22_write(0x0E, 0x00);              
    //0x0F R/W - ADC configuration        
    rfm22_write(0x0F, 0x70);             
    //0x10 R/W - ADC sensor amplifier offset
    rfm22_write(0x10, 0x00);              
    //0x11 R - ADC value
    //0x12 R/W - Temperature sensor control
    rfm22_write(0x12, 0x00);              
    //0x13 R/W - Temperature offset value
    rfm22_write(0x13, 0x00);              
    //0x14 R/W - Wake-up timer period 1        
    //0x15 R/W - Wake-up timer period 2
    //0x16 R/W - Wake-up timer period 3
    //0x17 R - Wake-up timer value 1
    //0x18 R - Wake-up timer value 2
    //0x19 R/W - Low-duty cycle mode duration
    //0x1A R/W - Low battery detector threshold
    //0x1B R - Battery voltage level
    //0x1C R/W - IF filter bandwidth
    rfm22_write(0x1C, 0x88);        // 200kbps 
    //rfm22_write(0x1C, 0x83);    // 150kbps
    //rfm22_write(0x1C, 0x9A);    // 100kbps              
    //0x1D R/W - AFC loop gearshift override
    rfm22_write(0x1D, 0x3C);        // 200kbps & 150kbps & 100kbps       
    //0x1E R/W - AFC timing control
    rfm22_write(0x1E, 0x02);        // 200kbps & 150kbps & 100kbps 
    //0x1F R/W - Clock recovery gearshift override
    rfm22_write(0x1F, 0x03);        // 200kbps & 150kbps & 100kbps
    //0x20 R/W - Clock recovery oversampling ratio        
    rfm22_write(0x20, 0x3C);        // 200kbps  
    //  rfm22_write(0x20, 0x50);    // 150kbps
    //rfm22_write(0x20, 0x3C);    // 100kbps           
    //0x21 R/W Clock recovery offset 2 
    rfm22_write(0x21, 0x02);    // 200kbps 
    //rfm22_write(0x21, 0x01);    // 150kbps
    //rfm22_write(0x21, 0x02);    // 100kbps       
    //0x22 R/W - Clock recovery offset 1
    rfm22_write(0x22, 0x22);    // 200kbps
    //rfm22_write(0x22, 0x99);    // 150kbps
    //rfm22_write(0x22, 0x22);    // 100kbps
    //0x23 R/W - Clock recovery offset 0
    rfm22_write(0x23, 0x22);    // 200kbps
    //rfm22_write(0x23, 0x9A);    // 150kbps 
    //rfm22_write(0x23, 0x22);    // 100kbps        
    //0x24 R/W - Clock recovery timing loop gain 1
    rfm22_write(0x24, 0x07);        // 200kbps & 150 kbps & 100kbps       
    //0x25 R/W - Clock recovery timing loop gain 0
    rfm22_write(0x25, 0xFF);        // 200kbps & 150 kbps & 100kbps              
    //0x26 R - Received signal strength indicator
    //0x27 R/W - RSSI threshold for clear channel indicator
    rfm22_write(0x27, 0x45);
    //0x28 R - Antenna dicersity register 1       
    //0x29 R - Antenna dicersity register 2
    //0x2A R/W - AFC limiter
    rfm22_write(0x2A, 0xFF);        // 200kbps & 150 kbps & 100kbps
    //0x2B R - AFC correction read
    //0x2C R/W - OOK counter value 1
    rfm22_write(0x2C, 0x00);
    //0x2D R/W - OOK counter value 2
    rfm22_write(0x2D, 0x00);
    //0x2E R/W Slicer peak hold
    rfm22_write(0x2E, 0x00);
    //0x2F - RESERVED
    //0x30 R/W - Data access control              
    rfm22_write(0x30, 0x8C);              
    //0x31 R - EzMAC Status
    //0x32 R/W - Header control 1        
    rfm22_write(0x32, 0x88);         // Check header byte 3 and enable broadcast byte (0xFF)          
    //0x33 R/W - Header control 2        
    rfm22_write(0x33, 0x10);         // Sets length of header to 1 byte       
    //0x34 R/W - Preamble length        
    rfm22_write(0x34, 4);            // 4 nibble = 2 byte preamble
    //0x35 R/W - Preamble detection control
    rfm22_write(0x35, 0x20);              
    //0x36 R/W - Sync word 3
    rfm22_write(0x36, 0x2D);              
    //0x37 R/W - Sync word 2
    //0x38 R/W - Sync word 1
    //0x39 R/W - Sync word 0
    //0x3A R/W - Transmit header 3   //Transmit header set dynamically in transmit function           
    //0x3B R/W - Transmit header 2
    //0x3C R/W - Transmit header 1
    //0x3D R/W - Transmit header 0
    //0x3E R/W - Packet length       //Packet length is set dynamically in transmit function
    //0x3F R/W - Check header 3
    rfm22_write(0x3F, 0xFF);	     //Source address = 0xFF
    //0x40 R/W - Check header 2
    //0x41 R/W - Check header 1
    //0x42 R/W - Check header 0
    //0x43 R/W - Header enable 3
    rfm22_write(0x43, 0xFF);        // Check all bits
    //0x44 R/W - Header enable 2
    //0x45 R/W - Header enable 1
    //0x46 R/W - Header enable 0
    //0x47 R - Received header 3
    //0x48 R - Received header 2
    //0x49 R - Received header 1
    //0x4A R - Received header 0        
    //0x4B R - Received packet length
    //0x4C to 0x4E - RESERVED
    //0x4F R/W - ADC8 control
    //0x50 to 0x5F - RESERVED
    //0x58 R/W - Changed when setting tx data rate
    rfm22_write(0x58, 0xED);       // 200kbps
    //rfm22_write(0x58, 0xC0);   // 150kbps & 100kbps
    //0x60 R/W - Channel filter coefficient address        
    //0x61 - RESERVED
    //0x62 R/W - Crystal oscillator / Control test
    //0x63 to 0x68 - RESERVED
    //0x69 R/W - AGC Override 1
    rfm22_write(0x69, 0x60);       // 200kbps & 150kbps & 100kbps
    //0x6A to 0x6C - RESERVED
    //0x6D R/W - TX power
    rfm22_write(0x6D, 0x00);       // TX power to min
    //0x6E R/W - TX data rate 1
    rfm22_write(0x6E, 0x33);   // 200kbps
    //rfm22_write(0x6E, 0x26);   // 150kbps
    //rfm22_write(0x6E, 0x19);   // 100kbps              
    //0x6F R/W - TX data rate 0
    rfm22_write(0x6F, 0x33);   // 200kbps
    //rfm22_write(0x6F, 0x66);   // 150kbps
    //rfm22_write(0x6F, 0x9A);   // 100kbps          
    //0x70 R/W - Modulation mode control 1        
    rfm22_write(0x70, 0x0C);       // No manchester code, no data whiting
    //0x71 R/W - Modulation mode control 2
    //rfm22_write(0x71, 0x02);     // Direct mode FSK
    //rfm22_write(0x71, 0x22);       // FSK, fd[8]=0, no invert for TX/RX data, FIFO mode
    //0x72 R/W - Frequency deviation        
    rfm22_write(0x72, 0x50);       // 200kbps: Frequency deviation setting to 45K=72*625
    //0x73 R/W - Frequency offset 1        
    rfm22_write(0x73, 0x00);       // No frequency offset
    //0x74 R/W - Frequency offset 2        
    rfm22_write(0x74, 0x00);       // No frequency offset
    //0x75 R/W - Frequency band select
    rfm22_write(0x75, 0x53);       // frequency set to 434MHz
    //0x76 R/W - Nominal carrier frequency 1
    rfm22_write(0x76, 0x64);       // frequency set to 434MHz
    //0x77 R/W - Nominal carrier frequency 0  
    rfm22_write(0x77, 0x00);       // frequency set to 434Mhz
    //0x78 - RESERVED
    //0x79 R/W - Frequency hopping channel select
    rfm22_write(0x79, 0x00);       // no frequency hopping
    //0x7A R/W - Frequency hopping step size
    rfm22_write(0x7A, 0x00);       // no frequency hopping
    //0x7B - RESERVED
    //0x7C R/W - TX FIFO control 1
    //0x7D R/W - TX FIFO control 2
    //0x7E R/W - RX FIFO control
    //0x7F R/W - FIFO access    
    
    PORTC &= 0xFD;      // Clears TXANT
    PORTC &= 0xFB;      // Clears RXANT
}

//---------------------------------------------------------//
//Initiallise registers for direct mode on the RFM22 module//
//---------------------------------------------------------//
void DirectMode_Setup(void)
{
    //0x30 R/W - Data access control              
    rfm22_write(0x30, 0x01);	//Disable packet handling        
    //0x33 R/W - Header control 2        
    rfm22_write(0x33, 0x0A);	//No header, packet length not included in TX header
				//Sync words 3 & 2
    //0x34 R/W - Preamble length        
    rfm22_write(0x34, 8);       //8 nibbles = 4 byte preamble
    //0x35 R/W - Preamble detection control
    rfm22_write(0x35, 0x20);	//4 nibbles = 2 byte detection
    //0x36 R/W - Sync word 3
    rfm22_write(0x36, 0x2D);     
    //0x37 R/W - Sync word 2
    rfm22_write(0x37, 0xD4); 
    //0x71 R/W - Modulation mode control 2
    rfm22_write(0x71, 0xD3);	//GFSK modulation; fd[8]=0; no invert for TX/RX data
				//Direct mode using SDI pin (nSEL high); TX data CLK on nIRQ pin

    // Turn TX mode on
    rfm22_write(0x07, 0x09);
}

//==================//
//Interrupt handlers//
//==================//
//nIRQ interrupt
ISR(INT6_vect)
{
    //~~PD1 toggle~~
    PORTD ^= 0x02;

    ISRcount++;
    //Ignore first 2 toggles
    if(ISRcount > 2)
    {
	if(!(TX_BYTE_SENT))
	{
	    // Move bit position to the right
	    bitPos--;

	    // Separate individual bits in the test byte character
	    txBit = (TestByte & (1<<bitPos)) != 0;

	    // Place data onto the MOSI-SDI line (PB2)
	    if (txBit == 1) { PORTB |= 0x04; }
	    else { PORTB &= 0xFB; }

	    // Reached end of byte
	    if (bitPos == 0)
	    {
		// Reset variables
		bitPos = 8;
		txBit = 0;
		    
		// Show byte has been sent
		TX_BYTE_SENT = true;
		PORTD ^= 0x08; //PD3

		// Enable SPI again
		SPCR = (1<<SPE)|(1<<MSTR);
		SPSR = (1<<SPI2X);

		//Turn TX mode off
		rfm22_write(0x07, 0x01);
	    }
	}
	ISRcount = 2;
    }

    /*********************************/

    /* Receive a byte from the RFM22 */

    /*********************************/

}
