// Bare SPR project

#include <util/delay.h>
#include <avr/power.h>
#include <avr/wdt.h>

static const char rev_id[] ="$Revision: 317 $";
static const char by[] ="$LastChangedBy: sekerci.ahmet@gmail.com $";

int main(void)
{
    /* Disable watchdog if enabled by bootloader/fuses */
    MCUSR &= ~(1 << WDRF);
    wdt_disable();

    /* Disable clock division */
    clock_prescale_set(clock_div_1);

    /* Port D is output port (for testing) */
    DDRD = 0xFF;

    /* RGB LED output pins LED1(PE0 - R, PE1 - G, PC0 - B) */
    DDRE |= 0x03;
    PORTE |= 0x03;
    DDRC |= 0x01;
    PORTC |= 0x01;

    for(;;)
    {
	PORTE ^= 0x01;
	PORTD ^= 0x01;
	_delay_ms(1000);
    }
}
