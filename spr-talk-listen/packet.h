#ifndef _PACKET_H
#define _PACKET_H

typedef struct Packet {
    unsigned char dest;
    unsigned char src;
    unsigned char control;
    unsigned char payload_length;
    unsigned char tx_node;
    unsigned char rx_node;
    unsigned char *payload;
} Packet;

int send_packet(Packet *packet_buf, int spr_port);


#endif //_PACKET_H

