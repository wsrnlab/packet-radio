We have main thread and two threads created by main. 
Thread 1: "listen": listen packet radio and place 
       received packets in a queue (rcv_queue)
Thread 2: "talk": generate a message and place the message 
       in a queue (xmt_queue)

main thread, in round robin fashion:
1. check rcv_queue, if not empty, remove packet and process it 
(print the message?)
2. check xmt_queue, if not empty, remove packet and send it to 
the radio. 

Queue routines
--------------
Downloaded from the Internet, they are linked list implementation of queues. 
