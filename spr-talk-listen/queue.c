#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

/**
 * Push an item into queue, if this is the first item,
 * both queue->head and queue->tail will point to it,
 * otherwise the oldtail->next and tail will point to it.
 */
void push(Queue *queue, Packet *item) {
    // Create a new node
    Node *n = (Node *) malloc (sizeof(Node));
    n->item = item;
    n->next = NULL;

    if (queue->head == NULL) { // no head
        queue->head = n;
    } else {
        queue->tail->next = n;
    }
    queue->tail = n;
    queue->size++;
}
/**
 * Return and remove the first item.
 */
Packet *pop(Queue *queue) {
    if (queue->size > 0) {
	// get the first item
	Node *head = queue->head;
	Packet *item = head->item;
	// move head pointer to next node, decrease size
	queue->head = head->next;
	queue->size--;
	// free the memory of original head
	free(head);
	return item;
    } else {
	printf("Queue is empty!\n");
	exit(EXIT_FAILURE);
    }
}
/**
 * Return but not remove the first item.
 */
Packet *peek(Queue *queue) {
    Node *head = queue->head;
    return head->item;
}

// Return the number of items in the queue
int size(Queue *queue) {
    return queue->size;
}
/**
 * Create and initiate a Queue
 */
Queue createQueue(void) {
    Queue queue;
    queue.size = 0;
    queue.head = NULL;
    queue.tail = NULL;
    return queue;
}
