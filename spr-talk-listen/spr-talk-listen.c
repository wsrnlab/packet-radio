#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <pthread.h>
#include <ctype.h>
#include <string.h>
#include "../firmware/SmartPacketRadio/USB/USB_packet_defs.h" 
#include "queue.h" 
#include "packet.h"
#include "debug.h" 

// File scope functions 
static void listen(void *arg);
static void talk(void *arg);
static void process_command_line(int, char **);
static void print_help(int, char **);
static void initialize_board(void);

// File scope variables
static int spr_port = 0; 
static int board_addr = 0;
static int dest_addr = 0;
static Queue listen_queue;
static Queue talk_queue;
static pthread_mutex_t listen_queue_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t talk_queue_mutex =   PTHREAD_MUTEX_INITIALIZER;

int main(int argc, char *argv[])
{
    pthread_t listen_thread;
    pthread_t talk_thread;
    Packet *packet;

    process_command_line(argc, argv);
    initialize_board();
    listen_queue = createQueue(); // listen thread places the received packets
                                  // in this queue
    talk_queue = createQueue();   // talk thread places the packets to be
                                  // transmitted in this queue
    
    printf("Activating the listener and talker threads.\n");
    pthread_create(&listen_thread, NULL, (void *) listen, (void *) "listener");
    pthread_create(&talk_thread,   NULL, (void *) talk,   (void *) "talker"); 
    
    for (;;) { 
	// main() thread periodically checks the queues and acts
	printf("queues = %d %d\n", size(&listen_queue), size(&talk_queue));
	if (size(&listen_queue) > 0) {
	    printf("Pop a packet from the listen_queue.\n");
	    pthread_mutex_lock(&listen_queue_mutex); 
	    packet = pop(&listen_queue);
	    pthread_mutex_unlock(&listen_queue_mutex); 
	    // Display information extracted from the packet
	    free(packet);
	}
	if (size(&talk_queue) > 0) {
	    printf("Pop a packet from the talk_queue.\n");
	    pthread_mutex_lock(&talk_queue_mutex); 
	    packet = pop(&talk_queue);
	    pthread_mutex_unlock(&talk_queue_mutex); 
	    printf("Sending to %d (msg: %s)\n", packet->dest, packet->payload);
	    // send_packet(packet_buf, spr_port);
	    free(packet);
	}
	sleep(1);
    } // for

    pthread_join(listen_thread, NULL);
    pthread_join(talk_thread,   NULL);  
    return(EXIT_SUCCESS);
} // main()

static void listen(void *arg)
{
    printf("%s thread started.\n", (char *) arg);
    for (;;) {
	// here we listen the packet radio port
	// for incoming packets
	// 1. Allocate memory to store the packet.
	// 2. wait to receive the packet
	pthread_mutex_lock(&listen_queue_mutex); 
	// push(&listen_queue, 2);    
	pthread_mutex_unlock(&listen_queue_mutex); 
	sleep(1);
    } 
} // listen()

static void talk(void *arg)
{
    printf("%s thread started.\n", (char *) arg);
    unsigned char msg[] = "Quick brown fox";
    Packet *my_packet = NULL;

    for (;;) {
	// Allocate memory for the packet
	my_packet = malloc(sizeof(Packet));
	if(my_packet == NULL) {
	    perror("malloc()");
	    exit(EXIT_FAILURE);
	}
	// Fill the packet information
	my_packet->dest = dest_addr;
	my_packet->src = board_addr;
	my_packet->control = 0x00;
	my_packet->payload_length = strlen(msg);
	my_packet->tx_node = board_addr;
	my_packet->rx_node = dest_addr;
	my_packet->payload = msg;

	pthread_mutex_lock(&talk_queue_mutex); 
	push(&talk_queue, my_packet);    
	pthread_mutex_unlock(&talk_queue_mutex); 
	sleep(1);
    }
} // talk()

void process_command_line(int argc, char **argv)
{
    // Dealing with options provided through the command line
    int c;
    while ((c = getopt(argc, argv, "a:d:h")) != -1) {
	switch (c) {
	case 'a': // -a option for setting the device number
	    board_addr = strtol(optarg, NULL, 10);	
	    break;
	case 'd': // -d option for setting the destination
	    dest_addr = strtol(optarg, NULL, 10);	
	    break;
	case 'h': 
	case '?': 
	default:
	    if (optopt == 'a' || optopt == 'd')	{
		fprintf(stderr, "Option -%c requires an argument.\n", optopt);
		print_help(argc, argv);
	    } else if (isprint(optopt)) {
		fprintf(stderr, "Unknown option `-%c'.\n", optopt);
		print_help(argc, argv);
	    } else {
		fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
		print_help(argc, argv);
	    }
	    exit(EXIT_FAILURE);
	    break;
	} // switch
    } // while
    if ((board_addr == 0) || (dest_addr == 0)) {
	fprintf(stderr, "You need to provide the device and \
                                                destination addresses.\n");
	print_help(argc, argv);
	exit(EXIT_FAILURE);
    }
} // process_command_line()

void print_help(int argc, char **argv) {
    printf("Usage: %s -a <board_addr> -d <dest_addr> where \n", argv[0]);
    printf("       board_addr is the address of the packet radio (0 < board_addr < 255)\n");
    printf("       dest_addr is the destination to talk to (0 < dest_addr < 255)\n");
}

void initialize_board(void) {
    // Initialize:
    // 1. file descriptor
    // 2. the terminal device to talk to the radio
    // 3. smart packet radio board

    // Do not modify the code in this function. 
    // There are no user serviceable parts below. 

    char port_name[80] = "\0";
    sprintf(port_name, "/dev/radio%d", board_addr);
    spr_port = open(port_name, O_RDWR);
    if(spr_port == -1) {
	printf("Did not find device\n");
	exit(EXIT_FAILURE);
    }
    PRINTFDB(("Packet radio board is connected: %s\n", port_name));
    struct termios options;

    tcgetattr(spr_port, &options);
    cfmakeraw(&options); 

    options.c_cc[VTIME] = 0; // How long to wait for input before returning
                             // (units of deciseconds). 
    options.c_cc[VMIN] =  2; // Minimum number of characters for read() to
                             // return. 
    tcsetattr(spr_port, TCSANOW, &options);
    tcflush(spr_port, TCIFLUSH);

   // Reset the SPR board
    char reset_spr[] = {USB_PACKET_RESET};
    write(spr_port, reset_spr, sizeof(reset_spr));

    // Initialize SPR board address
    char set_address[] = {USB_PACKET_SET_ADDRESS, board_addr};
    write(spr_port, set_address, sizeof(set_address));
    
    // Enable direct mode (no MAC) 
    char set_mac[] = {USB_PACKET_SET_TX_MODE, SET_DIRECT_MODE};
    write(spr_port, set_mac, sizeof(set_mac));

    // Inquire about the firmware revision number
    char inquire_fw_version[] = {USB_PACKET_QUERY_VERSION};
    write(spr_port, inquire_fw_version, sizeof(inquire_fw_version));

    // Read the response from the packet radio
    char *revision_id = 
	(char *) calloc(USB_PACKET_QUERY_VER_SIZE + 1, sizeof(char));
    read(spr_port, revision_id, USB_PACKET_QUERY_VER_SIZE);
    printf("Monash WSRNLab Packet Radio -- Firmware %s\n", ++revision_id);

    // char set_led_color[] = {USB_PACKET_LED, LED1, MAGENTA};
    // write(spr_port, set_led_color, sizeof(set_led_color));
} // initialize_board()

