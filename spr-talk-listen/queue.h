#ifndef _QUEUE_H
#define _QUEUE_H

#include "packet.h"

// Classic linked-list implementation of queues.

typedef struct Node {
    Packet *item;  // each item contains a packet pointer
    struct Node *next;
} Node;

// The Queue struct, contains the pointers that
// point to first node and last node, and
// the size of the Queue
typedef struct Queue {
    Node *head;
    Node *tail;
    int size;     // size of this queue
} Queue;

void push (Queue *queue, Packet *item);
Packet *pop (Queue *queue);
Packet *peek (Queue *queue);
int size(Queue *queue);
Queue createQueue(void);

#endif //_QUEUE_H

