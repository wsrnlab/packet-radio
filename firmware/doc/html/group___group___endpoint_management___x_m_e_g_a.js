var group___group___endpoint_management___x_m_e_g_a =
[
    [ "Endpoint_FIFO_t", "struct_endpoint___f_i_f_o__t.html", [
      [ "Data", "struct_endpoint___f_i_f_o__t.html#af7af1b88696b844492dec5c4a48e9567", null ],
      [ "Length", "struct_endpoint___f_i_f_o__t.html#a0dffd72169d95352554b2bb346605c00", null ],
      [ "Position", "struct_endpoint___f_i_f_o__t.html#aa01bdff327b9282fbf03b614dfb7dc0b", null ]
    ] ],
    [ "Endpoint_FIFOPair_t", "struct_endpoint___f_i_f_o_pair__t.html", [
      [ "IN", "struct_endpoint___f_i_f_o_pair__t.html#af410d582b880007dd4d4f5589593c242", null ],
      [ "OUT", "struct_endpoint___f_i_f_o_pair__t.html#a99a65040d6c742d79ae11100bb6e8bfe", null ]
    ] ],
    [ "ENDPOINT_CONTROLEP_DEFAULT_SIZE", "group___group___endpoint_management___x_m_e_g_a.html#gaebe9cfe97e9292e5e8cfba9885bbd901", null ],
    [ "ENDPOINT_TOTAL_ENDPOINTS", "group___group___endpoint_management___x_m_e_g_a.html#ga0aaeafaa974fb7095750a771e2adfc73", null ],
    [ "Endpoint_ClearStatusStage", "group___group___endpoint_management___x_m_e_g_a.html#ga9e00020d1fca630c351e3b8139ba67df", null ],
    [ "Endpoint_ConfigureEndpointTable", "group___group___endpoint_management___x_m_e_g_a.html#ga6b39a9542d970d8c7aff7347783137ae", null ],
    [ "USB_Device_ControlEndpointSize", "group___group___endpoint_management___x_m_e_g_a.html#gac1805540ce24cf3cbd621c2e0cbc1c31", null ]
];