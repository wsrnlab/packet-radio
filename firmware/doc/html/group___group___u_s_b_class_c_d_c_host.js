var group___group___u_s_b_class_c_d_c_host =
[
    [ "USB_ClassInfo_CDC_Host_t", "struct_u_s_b___class_info___c_d_c___host__t.html", [
      [ "Config", "struct_u_s_b___class_info___c_d_c___host__t.html#adb09b7a008050acd3a1d8f5f725c8a31", null ],
      [ "ControlInterfaceNumber", "struct_u_s_b___class_info___c_d_c___host__t.html#a76d44141297c2465510c80226a78f309", null ],
      [ "ControlLineStates", "struct_u_s_b___class_info___c_d_c___host__t.html#aba2cd98d7ef60cc9be7b94e71aca91c5", null ],
      [ "DataINPipe", "struct_u_s_b___class_info___c_d_c___host__t.html#a02768260798e0d2ca8bb98e77833f328", null ],
      [ "DataOUTPipe", "struct_u_s_b___class_info___c_d_c___host__t.html#a129fd765c370bf9565d5469bbc59b95b", null ],
      [ "DeviceToHost", "struct_u_s_b___class_info___c_d_c___host__t.html#a29f20c2c7a77c9b44d61109d38ad43e2", null ],
      [ "HostToDevice", "struct_u_s_b___class_info___c_d_c___host__t.html#a74d6deaeff592d861016cd80efab8779", null ],
      [ "IsActive", "struct_u_s_b___class_info___c_d_c___host__t.html#a91198440d8a3cf889f40065c03d5ae05", null ],
      [ "LineEncoding", "struct_u_s_b___class_info___c_d_c___host__t.html#a0799e738058b0dd830ae4ae5dfa4fd07", null ],
      [ "NotificationPipe", "struct_u_s_b___class_info___c_d_c___host__t.html#a0158af917dca22c182eba77b23ec7e81", null ],
      [ "State", "struct_u_s_b___class_info___c_d_c___host__t.html#aa9f0e16e7319cd89ceb524d24ccb1085", null ]
    ] ],
    [ "CDC_Host_EnumerationFailure_ErrorCodes_t", "group___group___u_s_b_class_c_d_c_host.html#ga7f19327b672b465d863740866ae2c2c1", [
      [ "CDC_ENUMERROR_NoError", "group___group___u_s_b_class_c_d_c_host.html#gga7f19327b672b465d863740866ae2c2c1ab48e01039ec1629611d6da10ccc7a663", null ],
      [ "CDC_ENUMERROR_InvalidConfigDescriptor", "group___group___u_s_b_class_c_d_c_host.html#gga7f19327b672b465d863740866ae2c2c1a5a6b3de59a087e356fb3a8888133d09e", null ],
      [ "CDC_ENUMERROR_NoCompatibleInterfaceFound", "group___group___u_s_b_class_c_d_c_host.html#gga7f19327b672b465d863740866ae2c2c1a8b984e0611de79e65c1ab49e6325cc4e", null ],
      [ "CDC_ENUMERROR_PipeConfigurationFailed", "group___group___u_s_b_class_c_d_c_host.html#gga7f19327b672b465d863740866ae2c2c1afffdd1b5330604ebf61c298d54530a44", null ]
    ] ],
    [ "CDC_Host_BytesReceived", "group___group___u_s_b_class_c_d_c_host.html#ga60e22ed372a52178d59b251e0dc07f66", null ],
    [ "CDC_Host_ConfigurePipes", "group___group___u_s_b_class_c_d_c_host.html#ga26bf4a84f4a0daa07758dc1b18e63315", null ],
    [ "CDC_Host_Flush", "group___group___u_s_b_class_c_d_c_host.html#gaa416cb5aebb3f8a2afa8956127c240f5", null ],
    [ "CDC_Host_ReceiveByte", "group___group___u_s_b_class_c_d_c_host.html#gac01990d80adec5a3004cf4b4d3bd6511", null ],
    [ "CDC_Host_SendBreak", "group___group___u_s_b_class_c_d_c_host.html#ga4b6f600fe7d0c14f7e574b542c64893a", null ],
    [ "CDC_Host_SendByte", "group___group___u_s_b_class_c_d_c_host.html#gabdd1e6dc984d3fe716c4c00362ee18a0", null ],
    [ "CDC_Host_SendControlLineStateChange", "group___group___u_s_b_class_c_d_c_host.html#ga1c049ab0129e344215f7361f692c5f04", null ],
    [ "CDC_Host_SendData", "group___group___u_s_b_class_c_d_c_host.html#ga3ff7d361f2c48ee52e63c1a47f6c4b0e", null ],
    [ "CDC_Host_SendString", "group___group___u_s_b_class_c_d_c_host.html#ga0745f73c42f9bbb56dbde645e15e23e1", null ],
    [ "CDC_Host_SetLineEncoding", "group___group___u_s_b_class_c_d_c_host.html#ga87422f19d4f1050a6b0d59afd85dd56d", null ],
    [ "CDC_Host_USBTask", "group___group___u_s_b_class_c_d_c_host.html#gac1d8f6a65e1e5baa835bc705b2cd5714", null ],
    [ "EVENT_CDC_Host_ControLineStateChanged", "group___group___u_s_b_class_c_d_c_host.html#ga33a4e8ea9bb822ac7dd6369d86aa9f16", null ]
];