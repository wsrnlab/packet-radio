var _descriptors_8h =
[
    [ "USB_Descriptor_Configuration_t", "struct_u_s_b___descriptor___configuration__t.html", "struct_u_s_b___descriptor___configuration__t" ],
    [ "CDC_NOTIFICATION_EPADDR", "_descriptors_8h.html#a375d8befdd497fa6548ed0f72cb0d85d", null ],
    [ "CDC_NOTIFICATION_EPSIZE", "_descriptors_8h.html#a1cfffa25431375dde9c6c880212f43b6", null ],
    [ "CDC_RX_EPADDR", "_descriptors_8h.html#a5051b28e57390b0072358e2dc602083e", null ],
    [ "CDC_TX_EPADDR", "_descriptors_8h.html#a2114400269fd643e0e3b597df6a8930d", null ],
    [ "CDC_TXRX_EPSIZE", "_descriptors_8h.html#aef7fa1919196a302fd0f8b1079e008be", null ],
    [ "CALLBACK_USB_GetDescriptor", "_descriptors_8h.html#a205665735698917df77439b51d372e64", null ]
];