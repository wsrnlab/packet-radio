var _config_2_l_u_f_a_config_8h =
[
    [ "DEVICE_STATE_AS_GPIOR", "_config_2_l_u_f_a_config_8h.html#a69e8daf873adef1003356168e1a78725", null ],
    [ "FIXED_CONTROL_ENDPOINT_SIZE", "_config_2_l_u_f_a_config_8h.html#a94173e25c382d0261cb5be6312f9c6b7", null ],
    [ "FIXED_NUM_CONFIGURATIONS", "_config_2_l_u_f_a_config_8h.html#ac67524e986b10e9a1eb5c190863c5c7d", null ],
    [ "INTERRUPT_CONTROL_ENDPOINT", "_config_2_l_u_f_a_config_8h.html#a8322aa284ea72089e41750fb5f666ccf", null ],
    [ "USB_DEVICE_ONLY", "_config_2_l_u_f_a_config_8h.html#a0ea40b95bddb7eae247d3d91f5517d79", null ],
    [ "USE_FLASH_DESCRIPTORS", "_config_2_l_u_f_a_config_8h.html#a62b9d6f45fd422c7ba2957a67752a31b", null ],
    [ "USE_STATIC_OPTIONS", "_config_2_l_u_f_a_config_8h.html#a7b4f2c7fd2749f6c278416edefd7de3d", null ]
];