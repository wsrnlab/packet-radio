var group___group___u_s_b =
[
    [ "Device Management", "group___group___device.html", "group___group___device" ],
    [ "Endpoint Management", "group___group___endpoint_management.html", "group___group___endpoint_management" ],
    [ "USB Events", "group___group___events.html", "group___group___events" ],
    [ "Host Management", "group___group___host.html", "group___group___host" ],
    [ "USB On The Go (OTG) Management", "group___group___o_t_g.html", "group___group___o_t_g" ],
    [ "Pipe Management", "group___group___pipe_management.html", "group___group___pipe_management" ],
    [ "USB Descriptors", "group___group___std_descriptors.html", "group___group___std_descriptors" ],
    [ "Standard USB Requests", "group___group___std_request.html", "group___group___std_request" ],
    [ "USB Interface Management", "group___group___u_s_b_management.html", "group___group___u_s_b_management" ],
    [ "USB Mode Tokens", "group___group___u_s_b_mode.html", null ]
];