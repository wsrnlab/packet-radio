var group___group___pipe_management___a_v_r8 =
[
    [ "PIPE_CONTROLPIPE_DEFAULT_SIZE", "group___group___pipe_management___a_v_r8.html#ga0dc275b75a1bccc1b51dd78ef1485e2e", null ],
    [ "PIPE_ERRORFLAG_CRC16", "group___group___pipe_management___a_v_r8.html#ga523291f171ce8bbcad293302c702fff1", null ],
    [ "PIPE_ERRORFLAG_DATAPID", "group___group___pipe_management___a_v_r8.html#ga69a45a7966eea73ea92d663b68748abd", null ],
    [ "PIPE_ERRORFLAG_DATATGL", "group___group___pipe_management___a_v_r8.html#ga5b587ec7e9f0fc14d601bc6f9472bfdc", null ],
    [ "PIPE_ERRORFLAG_OVERFLOW", "group___group___pipe_management___a_v_r8.html#ga17a1e2b7563c2bb2b9ffd6f1800de2db", null ],
    [ "PIPE_ERRORFLAG_PID", "group___group___pipe_management___a_v_r8.html#ga6758ca70b0917fb4076a8d4d2f6dd3f4", null ],
    [ "PIPE_ERRORFLAG_TIMEOUT", "group___group___pipe_management___a_v_r8.html#ga5ba4ba74769293d65b32109e50a9e2d4", null ],
    [ "PIPE_ERRORFLAG_UNDERFLOW", "group___group___pipe_management___a_v_r8.html#ga955a374fff365dc1c3771f24651a1c7b", null ],
    [ "PIPE_MAX_SIZE", "group___group___pipe_management___a_v_r8.html#gaab4f0d06a298c058d074511c4d22dede", null ],
    [ "PIPE_TOKEN_IN", "group___group___pipe_management___a_v_r8.html#gaaad74ddaafc14770bfd3902e202658ee", null ],
    [ "PIPE_TOKEN_OUT", "group___group___pipe_management___a_v_r8.html#ga9d95c915c1f2fa07ecdcd1d08027ec40", null ],
    [ "PIPE_TOKEN_SETUP", "group___group___pipe_management___a_v_r8.html#ga23184c2580f8241fcc6c27c0a580706d", null ],
    [ "PIPE_TOTAL_PIPES", "group___group___pipe_management___a_v_r8.html#ga4b88cfa023da9b8e1ade82238b2603e2", null ],
    [ "Pipe_ConfigurePipe", "group___group___pipe_management___a_v_r8.html#gafdf581f3a8edb048740ded05ed30b646", null ],
    [ "Pipe_ConfigurePipeTable", "group___group___pipe_management___a_v_r8.html#gaceb01306be7379cb8f427305f6b84738", null ],
    [ "Pipe_IsEndpointBound", "group___group___pipe_management___a_v_r8.html#ga346269f8ae8cbe8270e5b99c2338c17e", null ],
    [ "USB_Host_ControlPipeSize", "group___group___pipe_management___a_v_r8.html#ga030459b0c0d934f1683c4656c0606a2a", null ]
];