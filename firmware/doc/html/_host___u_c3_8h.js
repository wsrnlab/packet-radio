var _host___u_c3_8h =
[
    [ "HOST_DEVICE_SETTLE_DELAY_MS", "_host___u_c3_8h.html#ga4591576cd26d635aa5dfdf992ed99a3f", null ],
    [ "USB_HOST_DEVICEADDRESS", "_host___u_c3_8h.html#ga8233ed35f4abbb180ec8304ce483d406", null ],
    [ "USB_Host_EnumerationErrorCodes_t", "_host___u_c3_8h.html#ga647b09a2205a9beda86049b6b8f004a0", [
      [ "HOST_ENUMERROR_NoError", "_host___u_c3_8h.html#gga647b09a2205a9beda86049b6b8f004a0af109c8e2f136312daf33c3f24813075f", null ],
      [ "HOST_ENUMERROR_WaitStage", "_host___u_c3_8h.html#gga647b09a2205a9beda86049b6b8f004a0a1dae3bd36f1c5a9f5e106fff9a42cc66", null ],
      [ "HOST_ENUMERROR_NoDeviceDetected", "_host___u_c3_8h.html#gga647b09a2205a9beda86049b6b8f004a0a10683800ce0b14ee049c9a50fdc101b9", null ],
      [ "HOST_ENUMERROR_ControlError", "_host___u_c3_8h.html#gga647b09a2205a9beda86049b6b8f004a0ad4e5a60748230db93193081b06e61a06", null ],
      [ "HOST_ENUMERROR_PipeConfigError", "_host___u_c3_8h.html#gga647b09a2205a9beda86049b6b8f004a0aa54e1f3548f38b80968903c585710de0", null ],
      [ "HOST_ENUMERROR_NoError", "_host___u_c3_8h.html#gga647b09a2205a9beda86049b6b8f004a0af109c8e2f136312daf33c3f24813075f", null ],
      [ "HOST_ENUMERROR_WaitStage", "_host___u_c3_8h.html#gga647b09a2205a9beda86049b6b8f004a0a1dae3bd36f1c5a9f5e106fff9a42cc66", null ],
      [ "HOST_ENUMERROR_NoDeviceDetected", "_host___u_c3_8h.html#gga647b09a2205a9beda86049b6b8f004a0a10683800ce0b14ee049c9a50fdc101b9", null ],
      [ "HOST_ENUMERROR_ControlError", "_host___u_c3_8h.html#gga647b09a2205a9beda86049b6b8f004a0ad4e5a60748230db93193081b06e61a06", null ],
      [ "HOST_ENUMERROR_PipeConfigError", "_host___u_c3_8h.html#gga647b09a2205a9beda86049b6b8f004a0aa54e1f3548f38b80968903c585710de0", null ]
    ] ],
    [ "USB_Host_ErrorCodes_t", "_host___u_c3_8h.html#ga3e4d1d4775626c3d29543f4a3154d74d", [
      [ "HOST_ERROR_VBusVoltageDip", "_host___u_c3_8h.html#gga3e4d1d4775626c3d29543f4a3154d74dac4b5e8b0a3bfb8abf6c5a4f62b1901ce", null ],
      [ "HOST_ERROR_VBusVoltageDip", "_host___u_c3_8h.html#gga3e4d1d4775626c3d29543f4a3154d74dac4b5e8b0a3bfb8abf6c5a4f62b1901ce", null ]
    ] ],
    [ "USB_Host_WaitMSErrorCodes_t", "_host___u_c3_8h.html#ga0d7b4622e2efdef4af38f69479639d2d", [
      [ "HOST_WAITERROR_Successful", "_host___u_c3_8h.html#gga0d7b4622e2efdef4af38f69479639d2da6a877298693b9202d801a9ec4d437dbe", null ],
      [ "HOST_WAITERROR_DeviceDisconnect", "_host___u_c3_8h.html#gga0d7b4622e2efdef4af38f69479639d2dae56e1bc9a32b44bb9753df49417ff9c3", null ],
      [ "HOST_WAITERROR_PipeError", "_host___u_c3_8h.html#gga0d7b4622e2efdef4af38f69479639d2da23075100f2433bd6947a31b483e9051a", null ],
      [ "HOST_WAITERROR_SetupStalled", "_host___u_c3_8h.html#gga0d7b4622e2efdef4af38f69479639d2daa065f527f48ad4ee073fa0ebe9c68ae1", null ],
      [ "HOST_WAITERROR_Successful", "_host___u_c3_8h.html#gga0d7b4622e2efdef4af38f69479639d2da6a877298693b9202d801a9ec4d437dbe", null ],
      [ "HOST_WAITERROR_DeviceDisconnect", "_host___u_c3_8h.html#gga0d7b4622e2efdef4af38f69479639d2dae56e1bc9a32b44bb9753df49417ff9c3", null ],
      [ "HOST_WAITERROR_PipeError", "_host___u_c3_8h.html#gga0d7b4622e2efdef4af38f69479639d2da23075100f2433bd6947a31b483e9051a", null ],
      [ "HOST_WAITERROR_SetupStalled", "_host___u_c3_8h.html#gga0d7b4622e2efdef4af38f69479639d2daa065f527f48ad4ee073fa0ebe9c68ae1", null ]
    ] ],
    [ "USB_Host_ProcessNextHostState", "_host___u_c3_8h.html#ga476cfe39d1f841f89380ac0457781270", null ],
    [ "USB_Host_WaitMS", "_host___u_c3_8h.html#ga5aa8c172b9e47e48f597b90852b4d300", null ]
];