var struct_u_s_b___class_info___c_d_c___device__t =
[
    [ "Config", "struct_u_s_b___class_info___c_d_c___device__t.html#a9240662937bced074aeefd562f9dcaf9", null ],
    [ "ControlInterfaceNumber", "struct_u_s_b___class_info___c_d_c___device__t.html#a6def55a42400ab7d4042d9a7c8ef3741", null ],
    [ "ControlLineStates", "struct_u_s_b___class_info___c_d_c___device__t.html#a965a0cc4ce2ca84929acfab62f53b416", null ],
    [ "DataINEndpoint", "struct_u_s_b___class_info___c_d_c___device__t.html#a6c265f4fc2dc96ec39642d7716344330", null ],
    [ "DataOUTEndpoint", "struct_u_s_b___class_info___c_d_c___device__t.html#a6068857309ff63302f4c6d6d51d32416", null ],
    [ "DeviceToHost", "struct_u_s_b___class_info___c_d_c___device__t.html#a507cd88f5576432895cb9751417604f8", null ],
    [ "HostToDevice", "struct_u_s_b___class_info___c_d_c___device__t.html#aed544533dad34ccf57714c2265446f7e", null ],
    [ "LineEncoding", "struct_u_s_b___class_info___c_d_c___device__t.html#a9d50fbe12872ea25ff8b62e9ab1df623", null ],
    [ "NotificationEndpoint", "struct_u_s_b___class_info___c_d_c___device__t.html#a170a03055ef588a94ed772432e284e1a", null ],
    [ "State", "struct_u_s_b___class_info___c_d_c___device__t.html#a88e1c85bdc7902735de69fb3db541104", null ]
];