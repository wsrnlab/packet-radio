var searchData=
[
  ['read_2fwrite_20of_20primitive_20data_20types',['Read/Write of Primitive Data Types',['../group___group___endpoint_primitive_r_w.html',1,'']]],
  ['read_2fwrite_20of_20primitive_20data_20types_20_28avr8_29',['Read/Write of Primitive Data Types (AVR8)',['../group___group___endpoint_primitive_r_w___a_v_r8.html',1,'']]],
  ['read_2fwrite_20of_20primitive_20data_20types_20_28uc3_29',['Read/Write of Primitive Data Types (UC3)',['../group___group___endpoint_primitive_r_w___u_c3.html',1,'']]],
  ['read_2fwrite_20of_20primitive_20data_20types_20_28xmega_29',['Read/Write of Primitive Data Types (XMEGA)',['../group___group___endpoint_primitive_r_w___x_m_e_g_a.html',1,'']]],
  ['read_2fwrite_20of_20multi_2dbyte_20streams',['Read/Write of Multi-Byte Streams',['../group___group___endpoint_stream_r_w.html',1,'']]],
  ['read_2fwrite_20of_20multi_2dbyte_20streams_20_28avr8_29',['Read/Write of Multi-Byte Streams (AVR8)',['../group___group___endpoint_stream_r_w___a_v_r8.html',1,'']]],
  ['read_2fwrite_20of_20multi_2dbyte_20streams_20_28uc3_29',['Read/Write of Multi-Byte Streams (UC3)',['../group___group___endpoint_stream_r_w___u_c3.html',1,'']]],
  ['read_2fwrite_20of_20multi_2dbyte_20streams_20_28xmega_29',['Read/Write of Multi-Byte Streams (XMEGA)',['../group___group___endpoint_stream_r_w___x_m_e_g_a.html',1,'']]],
  ['read_2fwrite_20of_20primitive_20data_20types',['Read/Write of Primitive Data Types',['../group___group___pipe_primitive_r_w.html',1,'']]],
  ['read_2fwrite_20of_20primitive_20data_20types_20_28avr8_29',['Read/Write of Primitive Data Types (AVR8)',['../group___group___pipe_primitive_r_w___a_v_r8.html',1,'']]],
  ['read_2fwrite_20of_20primitive_20data_20types_20_28uc3_29',['Read/Write of Primitive Data Types (UC3)',['../group___group___pipe_primitive_r_w___u_c3.html',1,'']]],
  ['read_2fwrite_20of_20multi_2dbyte_20streams',['Read/Write of Multi-Byte Streams',['../group___group___pipe_stream_r_w.html',1,'']]],
  ['read_2fwrite_20of_20multi_2dbyte_20streams_20_28avr8_29',['Read/Write of Multi-Byte Streams (AVR8)',['../group___group___pipe_stream_r_w___a_v_r8.html',1,'']]],
  ['read_2fwrite_20of_20multi_2dbyte_20streams_20_28uc3_29',['Read/Write of Multi-Byte Streams (UC3)',['../group___group___pipe_stream_r_w___u_c3.html',1,'']]]
];
