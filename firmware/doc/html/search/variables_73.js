var searchData=
[
  ['serialnumstrindex',['SerialNumStrIndex',['../struct_a_t_t_r___p_a_c_k_e_d.html#a6ae0ef89fe4c4624e976d47df5fb5d94',1,'ATTR_PACKED']]],
  ['size',['Size',['../struct_u_s_b___endpoint___table__t.html#a582900316f3b1b4650601e24bd60a39b',1,'USB_Endpoint_Table_t::Size()'],['../struct_u_s_b___pipe___table__t.html#a70ec242a1707897a8614d5919feb2b8e',1,'USB_Pipe_Table_t::Size()'],['../struct_a_t_t_r___p_a_c_k_e_d.html#a64f1411baa9e93a2261c6be20d4aef23',1,'ATTR_PACKED::Size()']]],
  ['slaveinterfacenumber',['SlaveInterfaceNumber',['../struct_a_t_t_r___p_a_c_k_e_d.html#af820648a2db1dc6ec0b5c87f598e9d24',1,'ATTR_PACKED']]],
  ['state',['State',['../struct_u_s_b___class_info___c_d_c___device__t.html#a88e1c85bdc7902735de69fb3db541104',1,'USB_ClassInfo_CDC_Device_t::State()'],['../struct_u_s_b___class_info___c_d_c___host__t.html#aa9f0e16e7319cd89ceb524d24ccb1085',1,'USB_ClassInfo_CDC_Host_t::State()']]],
  ['subclass',['SubClass',['../struct_a_t_t_r___p_a_c_k_e_d.html#a44fc92774c032658d90f17464c73226b',1,'ATTR_PACKED']]],
  ['subtype',['Subtype',['../struct_a_t_t_r___p_a_c_k_e_d.html#a0abf90cd8582644532b1f6f0221128e7',1,'ATTR_PACKED']]]
];
