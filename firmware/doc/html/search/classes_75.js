var searchData=
[
  ['usb_5fclassinfo_5fcdc_5fdevice_5ft',['USB_ClassInfo_CDC_Device_t',['../struct_u_s_b___class_info___c_d_c___device__t.html',1,'']]],
  ['usb_5fclassinfo_5fcdc_5fhost_5ft',['USB_ClassInfo_CDC_Host_t',['../struct_u_s_b___class_info___c_d_c___host__t.html',1,'']]],
  ['usb_5fdescriptor_5fconfiguration_5ft',['USB_Descriptor_Configuration_t',['../struct_u_s_b___descriptor___configuration__t.html',1,'']]],
  ['usb_5fendpoint_5ftable_5ft',['USB_Endpoint_Table_t',['../struct_u_s_b___endpoint___table__t.html',1,'']]],
  ['usb_5fpipe_5ftable_5ft',['USB_Pipe_Table_t',['../struct_u_s_b___pipe___table__t.html',1,'']]]
];
