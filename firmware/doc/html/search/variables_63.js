var searchData=
[
  ['capabilities',['Capabilities',['../struct_a_t_t_r___p_a_c_k_e_d.html#a822da0f61ae719f92d4f0eea126eb24b',1,'ATTR_PACKED']]],
  ['cdcspecification',['CDCSpecification',['../struct_a_t_t_r___p_a_c_k_e_d.html#a71e626aadeb608431dc2a77ba595575d',1,'ATTR_PACKED']]],
  ['charformat',['CharFormat',['../struct_a_t_t_r___p_a_c_k_e_d.html#a7eace8ea513fcfd46952782904e05639',1,'ATTR_PACKED']]],
  ['class',['Class',['../struct_a_t_t_r___p_a_c_k_e_d.html#a1df2483b92d1aa1246ad1895d298015a',1,'ATTR_PACKED']]],
  ['config',['Config',['../struct_u_s_b___class_info___c_d_c___device__t.html#a9240662937bced074aeefd562f9dcaf9',1,'USB_ClassInfo_CDC_Device_t::Config()'],['../struct_u_s_b___class_info___c_d_c___host__t.html#adb09b7a008050acd3a1d8f5f725c8a31',1,'USB_ClassInfo_CDC_Host_t::Config()']]],
  ['configattributes',['ConfigAttributes',['../struct_a_t_t_r___p_a_c_k_e_d.html#ae9d1345b90cc933f59d41f89d5e5a5c9',1,'ATTR_PACKED']]],
  ['configurationdescriptor',['ConfigurationDescriptor',['../_descriptors_8c.html#a59d882a5961a04a054fab63be98c3b80',1,'Descriptors.c']]],
  ['configurationnumber',['ConfigurationNumber',['../struct_a_t_t_r___p_a_c_k_e_d.html#a2719a17dbf90b4e0e940c7b03529fd1e',1,'ATTR_PACKED']]],
  ['configurationstrindex',['ConfigurationStrIndex',['../struct_a_t_t_r___p_a_c_k_e_d.html#a8724766e0d63fb9b6b014509907b3da0',1,'ATTR_PACKED']]],
  ['controlinterfacenumber',['ControlInterfaceNumber',['../struct_u_s_b___class_info___c_d_c___device__t.html#a6def55a42400ab7d4042d9a7c8ef3741',1,'USB_ClassInfo_CDC_Device_t::ControlInterfaceNumber()'],['../struct_u_s_b___class_info___c_d_c___host__t.html#a76d44141297c2465510c80226a78f309',1,'USB_ClassInfo_CDC_Host_t::ControlInterfaceNumber()']]],
  ['controllinestates',['ControlLineStates',['../struct_u_s_b___class_info___c_d_c___device__t.html#a965a0cc4ce2ca84929acfab62f53b416',1,'USB_ClassInfo_CDC_Device_t::ControlLineStates()'],['../struct_u_s_b___class_info___c_d_c___host__t.html#aba2cd98d7ef60cc9be7b94e71aca91c5',1,'USB_ClassInfo_CDC_Host_t::ControlLineStates()']]]
];
