var searchData=
[
  ['jtag_5fassert',['JTAG_ASSERT',['../group___group___architecture_specific.html#ga6707f2ae6083d3561a2688781f92c889',1,'ArchitectureSpecific.h']]],
  ['jtag_5fdebug_5fbreak',['JTAG_DEBUG_BREAK',['../group___group___architecture_specific.html#ga85846657c9d154fbafc89af57731f6a7',1,'ArchitectureSpecific.h']]],
  ['jtag_5fdebug_5fpoint',['JTAG_DEBUG_POINT',['../group___group___architecture_specific.html#ga17adf9c743cced05c37c49fe2e09d4d5',1,'ArchitectureSpecific.h']]],
  ['jtag_5fdisable',['JTAG_DISABLE',['../group___group___architecture_specific.html#gaf40f6b28050728617f95a45389000fd9',1,'ArchitectureSpecific.h']]],
  ['jtag_5fenable',['JTAG_ENABLE',['../group___group___architecture_specific.html#ga4900cf2828678570ead9f5f1109d0048',1,'ArchitectureSpecific.h']]]
];
