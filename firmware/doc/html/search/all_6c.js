var searchData=
[
  ['language_5fid_5feng',['LANGUAGE_ID_ENG',['../group___group___std_descriptors.html#ga5ec24e5402e906ab96c4d653b4ab2cec',1,'StdDescriptors.h']]],
  ['languagestring',['LanguageString',['../_descriptors_8c.html#a2706320165dd3831bf525233371d7af9',1,'Descriptors.c']]],
  ['le16_5fto_5fcpu',['le16_to_cpu',['../group___group___endian_conversion.html#ga65e9510f535c1ee2f826d447471289fa',1,'le16_to_cpu():&#160;Endianness.h'],['../group___group___endian_conversion.html#gaa0da5576d76569d3a7452897a91ea0e6',1,'LE16_TO_CPU():&#160;Endianness.h']]],
  ['le32_5fto_5fcpu',['le32_to_cpu',['../group___group___endian_conversion.html#ga48f527b00bc1d5e46366d720280a1039',1,'le32_to_cpu():&#160;Endianness.h'],['../group___group___endian_conversion.html#ga488a93b151ee4920c0a117fe66cc2efb',1,'LE32_TO_CPU():&#160;Endianness.h']]],
  ['lineencoding',['LineEncoding',['../struct_u_s_b___class_info___c_d_c___host__t.html#a0799e738058b0dd830ae4ae5dfa4fd07',1,'USB_ClassInfo_CDC_Host_t']]],
  ['lufa_5fversion_5finteger',['LUFA_VERSION_INTEGER',['../_version_8h.html#a466b679fe64e498eb8f545874138cd23',1,'Version.h']]],
  ['lufa_5fversion_5fstring',['LUFA_VERSION_STRING',['../_version_8h.html#a792e00b21c88af9a8d9b72ae41260afd',1,'Version.h']]],
  ['lufaconfig_2eh',['LUFAConfig.h',['../_l_u_f_a_2_code_templates_2_l_u_f_a_config_8h.html',1,'']]],
  ['lufaconfig_2eh',['LUFAConfig.h',['../_config_2_l_u_f_a_config_8h.html',1,'']]]
];
