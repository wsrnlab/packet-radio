var searchData=
[
  ['databits',['DataBits',['../struct_a_t_t_r___p_a_c_k_e_d.html#af2f13ba55710136f89b3cd4c3d6e1023',1,'ATTR_PACKED']]],
  ['datainendpoint',['DataINEndpoint',['../struct_u_s_b___class_info___c_d_c___device__t.html#a6c265f4fc2dc96ec39642d7716344330',1,'USB_ClassInfo_CDC_Device_t']]],
  ['datainpipe',['DataINPipe',['../struct_u_s_b___class_info___c_d_c___host__t.html#a02768260798e0d2ca8bb98e77833f328',1,'USB_ClassInfo_CDC_Host_t']]],
  ['dataoutendpoint',['DataOUTEndpoint',['../struct_u_s_b___class_info___c_d_c___device__t.html#a6068857309ff63302f4c6d6d51d32416',1,'USB_ClassInfo_CDC_Device_t']]],
  ['dataoutpipe',['DataOUTPipe',['../struct_u_s_b___class_info___c_d_c___host__t.html#a129fd765c370bf9565d5469bbc59b95b',1,'USB_ClassInfo_CDC_Host_t']]],
  ['devicedescriptor',['DeviceDescriptor',['../_descriptors_8c.html#addd196cc2b517282c4dc2a694313b6ac',1,'Descriptors.c']]],
  ['devicetohost',['DeviceToHost',['../struct_u_s_b___class_info___c_d_c___device__t.html#a507cd88f5576432895cb9751417604f8',1,'USB_ClassInfo_CDC_Device_t::DeviceToHost()'],['../struct_u_s_b___class_info___c_d_c___host__t.html#a29f20c2c7a77c9b44d61109d38ad43e2',1,'USB_ClassInfo_CDC_Host_t::DeviceToHost()']]]
];
