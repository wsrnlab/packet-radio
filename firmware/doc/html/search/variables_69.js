var searchData=
[
  ['iadstrindex',['IADStrIndex',['../struct_a_t_t_r___p_a_c_k_e_d.html#a54d6f08b084cd9a163385c721912ab33',1,'ATTR_PACKED']]],
  ['iconfiguration',['iConfiguration',['../struct_a_t_t_r___p_a_c_k_e_d.html#ad3c64a123595b4018ff8c2148f02fb9a',1,'ATTR_PACKED']]],
  ['idproduct',['idProduct',['../struct_a_t_t_r___p_a_c_k_e_d.html#a2e444f741b9e7cd231bf587373fa65ed',1,'ATTR_PACKED']]],
  ['idvendor',['idVendor',['../struct_a_t_t_r___p_a_c_k_e_d.html#aef387ab552a30e86ef9cc0ed3bcf51ae',1,'ATTR_PACKED']]],
  ['ifunction',['iFunction',['../struct_a_t_t_r___p_a_c_k_e_d.html#a84e55ee4d4e5bf14d015b804fe4f7d39',1,'ATTR_PACKED']]],
  ['iinterface',['iInterface',['../struct_a_t_t_r___p_a_c_k_e_d.html#a97f4e97083f13174c5047aadf6523d47',1,'ATTR_PACKED']]],
  ['imanufacturer',['iManufacturer',['../struct_a_t_t_r___p_a_c_k_e_d.html#a2d8897804bc3c9860cf3504de924028e',1,'ATTR_PACKED']]],
  ['interfacenumber',['InterfaceNumber',['../struct_a_t_t_r___p_a_c_k_e_d.html#a012dddf5190a12bd17a6edbb1473fa18',1,'ATTR_PACKED']]],
  ['interfacestrindex',['InterfaceStrIndex',['../struct_a_t_t_r___p_a_c_k_e_d.html#a2a29fe5d657c105da43993fd22bb33bf',1,'ATTR_PACKED']]],
  ['iproduct',['iProduct',['../struct_a_t_t_r___p_a_c_k_e_d.html#a986f0cf39ec3367e84b3ad7891f4ac7e',1,'ATTR_PACKED']]],
  ['isactive',['IsActive',['../struct_u_s_b___class_info___c_d_c___host__t.html#a91198440d8a3cf889f40065c03d5ae05',1,'USB_ClassInfo_CDC_Host_t']]],
  ['iserialnumber',['iSerialNumber',['../struct_a_t_t_r___p_a_c_k_e_d.html#ab840ce433b0639717043bf3f828854ee',1,'ATTR_PACKED']]]
];
