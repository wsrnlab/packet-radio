var searchData=
[
  ['totalconfigurationsize',['TotalConfigurationSize',['../struct_a_t_t_r___p_a_c_k_e_d.html#a9ae5a5d0b52657e2fcf17e365d2499a4',1,'ATTR_PACKED']]],
  ['totalendpoints',['TotalEndpoints',['../struct_a_t_t_r___p_a_c_k_e_d.html#a017fba35ff17e024e877bab8f3e6aaf0',1,'ATTR_PACKED']]],
  ['totalinterfaces',['TotalInterfaces',['../struct_a_t_t_r___p_a_c_k_e_d.html#a2a72ca7b2a4dfaa5e2f903a2e7628502',1,'ATTR_PACKED']]],
  ['tx_5fmode_5ft',['tx_mode_t',['../_u_s_b__packet_8h.html#a50b692f77fc055b7569a380cfddb97be',1,'USB_packet.h']]],
  ['type',['Type',['../struct_u_s_b___endpoint___table__t.html#a4f8f6b50da5c787f0194c10aa7d17c73',1,'USB_Endpoint_Table_t::Type()'],['../struct_u_s_b___pipe___table__t.html#a2fe69c9ce0164329d0f00bfcb3d93720',1,'USB_Pipe_Table_t::Type()'],['../struct_a_t_t_r___p_a_c_k_e_d.html#ab5f7e781e8a6f15b13afb8eaf5f388df',1,'ATTR_PACKED::Type()']]]
];
