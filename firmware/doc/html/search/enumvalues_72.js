var searchData=
[
  ['req_5fclearfeature',['REQ_ClearFeature',['../group___group___std_request.html#gga677969861705f7148f96c464e76c79d2a0b43219969607951b15ea3e49fae39b3',1,'StdRequestType.h']]],
  ['req_5fgetconfiguration',['REQ_GetConfiguration',['../group___group___std_request.html#gga677969861705f7148f96c464e76c79d2a571f74edd218f30fec062011b908899a',1,'StdRequestType.h']]],
  ['req_5fgetdescriptor',['REQ_GetDescriptor',['../group___group___std_request.html#gga677969861705f7148f96c464e76c79d2a88c7bffa0d345dd0b3d9598ca6ba31cd',1,'StdRequestType.h']]],
  ['req_5fgetinterface',['REQ_GetInterface',['../group___group___std_request.html#gga677969861705f7148f96c464e76c79d2a104428fd5a45d154fda7332979ef3013',1,'StdRequestType.h']]],
  ['req_5fgetstatus',['REQ_GetStatus',['../group___group___std_request.html#gga677969861705f7148f96c464e76c79d2a5892786a0451b9d8f5d0384271b4fe44',1,'StdRequestType.h']]],
  ['req_5fsetaddress',['REQ_SetAddress',['../group___group___std_request.html#gga677969861705f7148f96c464e76c79d2a770e84561a6039de36e24a3acf14e035',1,'StdRequestType.h']]],
  ['req_5fsetconfiguration',['REQ_SetConfiguration',['../group___group___std_request.html#gga677969861705f7148f96c464e76c79d2a84e0eb0b9ced98ce7004e124af8a857f',1,'StdRequestType.h']]],
  ['req_5fsetdescriptor',['REQ_SetDescriptor',['../group___group___std_request.html#gga677969861705f7148f96c464e76c79d2a15817dac0d27f15ca826b3e1ee663cdd',1,'StdRequestType.h']]],
  ['req_5fsetfeature',['REQ_SetFeature',['../group___group___std_request.html#gga677969861705f7148f96c464e76c79d2a872ff93c50d35b60282c6f27e0fcb57a',1,'StdRequestType.h']]],
  ['req_5fsetinterface',['REQ_SetInterface',['../group___group___std_request.html#gga677969861705f7148f96c464e76c79d2a5219c390455a2c028c735df18a16e4e1',1,'StdRequestType.h']]],
  ['req_5fsynchframe',['REQ_SynchFrame',['../group___group___std_request.html#gga677969861705f7148f96c464e76c79d2a8f9e3be78471708c4d9dea781d86d933',1,'StdRequestType.h']]]
];
