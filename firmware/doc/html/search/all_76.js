var searchData=
[
  ['vibration_20sensor_20_28adxl345_29',['Vibration Sensor (ADXL345)',['../_a_d_x_l345.html',1,'']]],
  ['vibration_20sensor_20driver_20_28adxl345_29',['Vibration Sensor Driver (ADXL345)',['../group___a_d_x_l345___drivers.html',1,'']]],
  ['vendorid',['VendorID',['../struct_a_t_t_r___p_a_c_k_e_d.html#abca22bb558b4f7f328063da8436461f7',1,'ATTR_PACKED']]],
  ['version_2eh',['Version.h',['../_version_8h.html',1,'']]],
  ['version_5fbcd',['VERSION_BCD',['../group___group___std_descriptors.html#ga40a667815d7089266f5e2ea33c2cc157',1,'StdDescriptors.h']]],
  ['virtualserial_5fcdc_5finterface',['VirtualSerial_CDC_Interface',['../_smart_packet_radio_8c.html#a6abff48bf476b7a0b5d81012d90058a6',1,'VirtualSerial_CDC_Interface():&#160;SmartPacketRadio.c'],['../_smart_packet_radio_8h.html#a6abff48bf476b7a0b5d81012d90058a6',1,'VirtualSerial_CDC_Interface():&#160;SmartPacketRadio.c'],['../_u_s_b__packet_8c.html#a6abff48bf476b7a0b5d81012d90058a6',1,'VirtualSerial_CDC_Interface():&#160;SmartPacketRadio.c']]]
];
