var _s_p_i___x_m_e_g_a_8h =
[
    [ "SPI_MODE_MASTER", "_s_p_i___x_m_e_g_a_8h.html#gaa335c2abdfad9e6f6c2677719d93b64e", null ],
    [ "SPI_MODE_SLAVE", "_s_p_i___x_m_e_g_a_8h.html#ga75f094fee5a9dc10b88401ccd17925d3", null ],
    [ "SPI_ORDER_LSB_FIRST", "_s_p_i___x_m_e_g_a_8h.html#gaf9992517b45b9bc290d1f0a6a30702e1", null ],
    [ "SPI_ORDER_MSB_FIRST", "_s_p_i___x_m_e_g_a_8h.html#ga279bd3261b219549f4f88dea8e1655bc", null ],
    [ "SPI_SAMPLE_LEADING", "_s_p_i___x_m_e_g_a_8h.html#gaf922244c6d3b36dcc62c9f7d6bc39124", null ],
    [ "SPI_SAMPLE_TRAILING", "_s_p_i___x_m_e_g_a_8h.html#ga8fec7470b67acb5527caf2b6ed514128", null ],
    [ "SPI_SCK_LEAD_FALLING", "_s_p_i___x_m_e_g_a_8h.html#gabf2cebda7e2fa76b4989bcb0df98659c", null ],
    [ "SPI_SCK_LEAD_RISING", "_s_p_i___x_m_e_g_a_8h.html#gadfb25a268f39349e5432712199121399", null ],
    [ "SPI_SPEED_FCPU_DIV_128", "_s_p_i___x_m_e_g_a_8h.html#ga02baddf33900cc258ad837fa659d6b8a", null ],
    [ "SPI_SPEED_FCPU_DIV_16", "_s_p_i___x_m_e_g_a_8h.html#ga502fed67d053187481f6963d9c7a5d2e", null ],
    [ "SPI_SPEED_FCPU_DIV_2", "_s_p_i___x_m_e_g_a_8h.html#gabf1030f0fd85fa359e11cd7db29833c3", null ],
    [ "SPI_SPEED_FCPU_DIV_32", "_s_p_i___x_m_e_g_a_8h.html#ga84ce6e65d75cf34bd744b1bab61f63e0", null ],
    [ "SPI_SPEED_FCPU_DIV_4", "_s_p_i___x_m_e_g_a_8h.html#ga0ca3a6d14c56f5a303f22b3e0b2c4079", null ],
    [ "SPI_SPEED_FCPU_DIV_64", "_s_p_i___x_m_e_g_a_8h.html#gaacd339fe87d27ec4a48fa7119059e823", null ],
    [ "SPI_SPEED_FCPU_DIV_8", "_s_p_i___x_m_e_g_a_8h.html#ga61e01416a7bb8467f847c358140c6cb3", null ],
    [ "SPI_USE_DOUBLESPEED", "_s_p_i___x_m_e_g_a_8h.html#gad8be19dddb897f8078074bd9753b61ce", null ]
];