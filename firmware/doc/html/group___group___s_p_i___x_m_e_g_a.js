var group___group___s_p_i___x_m_e_g_a =
[
    [ "SPI_MODE_MASTER", "group___group___s_p_i___x_m_e_g_a.html#gaa335c2abdfad9e6f6c2677719d93b64e", null ],
    [ "SPI_MODE_SLAVE", "group___group___s_p_i___x_m_e_g_a.html#ga75f094fee5a9dc10b88401ccd17925d3", null ],
    [ "SPI_ORDER_LSB_FIRST", "group___group___s_p_i___x_m_e_g_a.html#gaf9992517b45b9bc290d1f0a6a30702e1", null ],
    [ "SPI_ORDER_MSB_FIRST", "group___group___s_p_i___x_m_e_g_a.html#ga279bd3261b219549f4f88dea8e1655bc", null ],
    [ "SPI_SAMPLE_LEADING", "group___group___s_p_i___x_m_e_g_a.html#gaf922244c6d3b36dcc62c9f7d6bc39124", null ],
    [ "SPI_SAMPLE_TRAILING", "group___group___s_p_i___x_m_e_g_a.html#ga8fec7470b67acb5527caf2b6ed514128", null ],
    [ "SPI_SCK_LEAD_FALLING", "group___group___s_p_i___x_m_e_g_a.html#gabf2cebda7e2fa76b4989bcb0df98659c", null ],
    [ "SPI_SCK_LEAD_RISING", "group___group___s_p_i___x_m_e_g_a.html#gadfb25a268f39349e5432712199121399", null ],
    [ "SPI_SPEED_FCPU_DIV_128", "group___group___s_p_i___x_m_e_g_a.html#ga02baddf33900cc258ad837fa659d6b8a", null ],
    [ "SPI_SPEED_FCPU_DIV_16", "group___group___s_p_i___x_m_e_g_a.html#ga502fed67d053187481f6963d9c7a5d2e", null ],
    [ "SPI_SPEED_FCPU_DIV_2", "group___group___s_p_i___x_m_e_g_a.html#gabf1030f0fd85fa359e11cd7db29833c3", null ],
    [ "SPI_SPEED_FCPU_DIV_32", "group___group___s_p_i___x_m_e_g_a.html#ga84ce6e65d75cf34bd744b1bab61f63e0", null ],
    [ "SPI_SPEED_FCPU_DIV_4", "group___group___s_p_i___x_m_e_g_a.html#ga0ca3a6d14c56f5a303f22b3e0b2c4079", null ],
    [ "SPI_SPEED_FCPU_DIV_64", "group___group___s_p_i___x_m_e_g_a.html#gaacd339fe87d27ec4a48fa7119059e823", null ],
    [ "SPI_SPEED_FCPU_DIV_8", "group___group___s_p_i___x_m_e_g_a.html#ga61e01416a7bb8467f847c358140c6cb3", null ]
];