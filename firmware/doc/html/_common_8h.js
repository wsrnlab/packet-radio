var _common_8h =
[
    [ "__INCLUDE_FROM_COMMON_H", "_common_8h.html#ga98cfacf635430fb1da5596d8782bb9b0", null ],
    [ "ARCH_HAS_EEPROM_ADDRESS_SPACE", "_common_8h.html#gad93b856217ef28cfbbcde64027b2234f", null ],
    [ "ARCH_HAS_FLASH_ADDRESS_SPACE", "_common_8h.html#ga9d99da330d33073be40af3e7604cfc5f", null ],
    [ "ARCH_HAS_MULTI_ADDRESS_SPACE", "_common_8h.html#gaeda0e6513e22ae78fa359d22faed9cfc", null ],
    [ "ARCH_LITTLE_ENDIAN", "_common_8h.html#ga1c8ed4b72dac6929d882e2aeb0261081", null ],
    [ "ISR", "_common_8h.html#ga4de331f060de68e2368825e1634edf3f", null ],
    [ "MACROE", "_common_8h.html#ga0b4041b3602c0924a8c61c7d8231c801", null ],
    [ "MACROS", "_common_8h.html#gab669385ecb2cb74e6d17a0bc671ec1aa", null ],
    [ "MAX", "_common_8h.html#gaacc3ee1a7f283f8ef65cea31f4436a95", null ],
    [ "MIN", "_common_8h.html#ga74e75242132eaabbc1c512488a135926", null ],
    [ "STRINGIFY", "_common_8h.html#ga6df1d22fb5f09eccc23b9f399670cfd7", null ],
    [ "STRINGIFY_EXPANDED", "_common_8h.html#gad32fe059c6b285c6f8d5bd58185d7ea3", null ],
    [ "uint_reg_t", "_common_8h.html#ga8d21664b9e487d7568bf29d012b3bde1", null ]
];