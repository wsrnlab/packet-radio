var _u_s_b_controller___x_m_e_g_a_8h =
[
    [ "USB_OPT_BUSEVENT_PRIHIGH", "_u_s_b_controller___x_m_e_g_a_8h.html#ga59cfabc50ed61d6fc601a91873e1a523", null ],
    [ "USB_OPT_BUSEVENT_PRILOW", "_u_s_b_controller___x_m_e_g_a_8h.html#gaa2113b15f631e4b5c9eaf6474b088795", null ],
    [ "USB_OPT_BUSEVENT_PRIMED", "_u_s_b_controller___x_m_e_g_a_8h.html#gae3170aafaf92953a91ac3887c01a72f0", null ],
    [ "USB_OPT_PLLCLKSRC", "_u_s_b_controller___x_m_e_g_a_8h.html#gad5fafaea0854b5f936a00396704b2f16", null ],
    [ "USB_OPT_RC32MCLKSRC", "_u_s_b_controller___x_m_e_g_a_8h.html#gac0b7c86e6add9dd80e1920de1c5f05ae", null ],
    [ "USB_Modes_t", "_u_s_b_controller___x_m_e_g_a_8h.html#ga750c4e36c6792e03b80ab04d2d0b6ddb", [
      [ "USB_MODE_None", "_u_s_b_controller___x_m_e_g_a_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba9941f94cd940d822c81bf88405bccfb3", null ],
      [ "USB_MODE_Device", "_u_s_b_controller___x_m_e_g_a_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaba0991704fbb65de7d2ed168036cd656", null ],
      [ "USB_MODE_Host", "_u_s_b_controller___x_m_e_g_a_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba4c57ef994d66a3b4d95fb6cc13b8babb", null ],
      [ "USB_MODE_UID", "_u_s_b_controller___x_m_e_g_a_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaddfec606070e7f77eb4e71ba325aa7e4", null ],
      [ "USB_MODE_None", "_u_s_b_controller___x_m_e_g_a_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba9941f94cd940d822c81bf88405bccfb3", null ],
      [ "USB_MODE_Device", "_u_s_b_controller___x_m_e_g_a_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaba0991704fbb65de7d2ed168036cd656", null ],
      [ "USB_MODE_Host", "_u_s_b_controller___x_m_e_g_a_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba4c57ef994d66a3b4d95fb6cc13b8babb", null ],
      [ "USB_MODE_UID", "_u_s_b_controller___x_m_e_g_a_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaddfec606070e7f77eb4e71ba325aa7e4", null ],
      [ "USB_MODE_None", "_u_s_b_controller___x_m_e_g_a_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba9941f94cd940d822c81bf88405bccfb3", null ],
      [ "USB_MODE_Device", "_u_s_b_controller___x_m_e_g_a_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaba0991704fbb65de7d2ed168036cd656", null ]
    ] ],
    [ "USB_Disable", "_u_s_b_controller___x_m_e_g_a_8h.html#ga60cf524c4acc0ccae14186b4a572316b", null ],
    [ "USB_Init", "_u_s_b_controller___x_m_e_g_a_8h.html#ga22b285b5403cf48fc8fc587921278563", null ],
    [ "USB_ResetInterface", "_u_s_b_controller___x_m_e_g_a_8h.html#ga12464051e3763a6ac6ccf8eaa9f40324", null ],
    [ "USB_CurrentMode", "_u_s_b_controller___x_m_e_g_a_8h.html#gaf61a61231449336fa28b9d6dc57e19b5", null ],
    [ "USB_EndpointTable", "_u_s_b_controller___x_m_e_g_a_8h.html#gab0038864d695a7ee458eeb2afc3eda82", null ],
    [ "USB_Options", "_u_s_b_controller___x_m_e_g_a_8h.html#ga3b2989fdb6af51351e8fc62a4a82a399", null ]
];