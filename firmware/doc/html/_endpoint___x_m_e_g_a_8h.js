var _endpoint___x_m_e_g_a_8h =
[
    [ "ENDPOINT_CONTROLEP_DEFAULT_SIZE", "_endpoint___x_m_e_g_a_8h.html#gaebe9cfe97e9292e5e8cfba9885bbd901", null ],
    [ "ENDPOINT_TOTAL_ENDPOINTS", "_endpoint___x_m_e_g_a_8h.html#ga0aaeafaa974fb7095750a771e2adfc73", null ],
    [ "Endpoint_WaitUntilReady_ErrorCodes_t", "_endpoint___x_m_e_g_a_8h.html#gaa42b7eb8d1be3afadb97097bf2605740", [
      [ "ENDPOINT_READYWAIT_NoError", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740a5bd129a3bb182ecdcfe16549e175308a", null ],
      [ "ENDPOINT_READYWAIT_EndpointStalled", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740ac395cfec6ba73cd76fd016e746b7dc74", null ],
      [ "ENDPOINT_READYWAIT_DeviceDisconnected", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740a39bcc8c146680a1666f2eba6dccc0152", null ],
      [ "ENDPOINT_READYWAIT_BusSuspended", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740a46004ca0aeabab26bf7fa723ab27999f", null ],
      [ "ENDPOINT_READYWAIT_Timeout", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740aa6da793dddcd7512d2e92599052d04b5", null ],
      [ "ENDPOINT_READYWAIT_NoError", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740a5bd129a3bb182ecdcfe16549e175308a", null ],
      [ "ENDPOINT_READYWAIT_EndpointStalled", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740ac395cfec6ba73cd76fd016e746b7dc74", null ],
      [ "ENDPOINT_READYWAIT_DeviceDisconnected", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740a39bcc8c146680a1666f2eba6dccc0152", null ],
      [ "ENDPOINT_READYWAIT_BusSuspended", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740a46004ca0aeabab26bf7fa723ab27999f", null ],
      [ "ENDPOINT_READYWAIT_Timeout", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740aa6da793dddcd7512d2e92599052d04b5", null ],
      [ "ENDPOINT_READYWAIT_NoError", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740a5bd129a3bb182ecdcfe16549e175308a", null ],
      [ "ENDPOINT_READYWAIT_EndpointStalled", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740ac395cfec6ba73cd76fd016e746b7dc74", null ],
      [ "ENDPOINT_READYWAIT_DeviceDisconnected", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740a39bcc8c146680a1666f2eba6dccc0152", null ],
      [ "ENDPOINT_READYWAIT_BusSuspended", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740a46004ca0aeabab26bf7fa723ab27999f", null ],
      [ "ENDPOINT_READYWAIT_Timeout", "_endpoint___x_m_e_g_a_8h.html#ggaa42b7eb8d1be3afadb97097bf2605740aa6da793dddcd7512d2e92599052d04b5", null ]
    ] ],
    [ "Endpoint_ClearEndpoints", "_endpoint___x_m_e_g_a_8h.html#ga3f5d191754d25b22c1eab73456bc9758", null ],
    [ "Endpoint_ClearStatusStage", "_endpoint___x_m_e_g_a_8h.html#ga9e00020d1fca630c351e3b8139ba67df", null ],
    [ "Endpoint_ConfigureEndpoint_PRV", "_endpoint___x_m_e_g_a_8h.html#ga0f28d9a1997c697df94c666547eb179f", null ],
    [ "Endpoint_ConfigureEndpointTable", "_endpoint___x_m_e_g_a_8h.html#ga6b39a9542d970d8c7aff7347783137ae", null ],
    [ "Endpoint_WaitUntilReady", "_endpoint___x_m_e_g_a_8h.html#ga2ee0f0710d9f319a5d5d13f1b6019488", null ],
    [ "USB_Device_ControlEndpointSize", "_endpoint___x_m_e_g_a_8h.html#gac1805540ce24cf3cbd621c2e0cbc1c31", null ],
    [ "USB_Endpoint_FIFOs", "_endpoint___x_m_e_g_a_8h.html#ga8bf00e23ba614158da38a93486e4d1a3", null ],
    [ "USB_Endpoint_SelectedEndpoint", "_endpoint___x_m_e_g_a_8h.html#gac7d6d8d3595000b609adfa7382183252", null ],
    [ "USB_Endpoint_SelectedFIFO", "_endpoint___x_m_e_g_a_8h.html#gad184d0d2d2b3f0d1cfb2b21e4294103a", null ],
    [ "USB_Endpoint_SelectedHandle", "_endpoint___x_m_e_g_a_8h.html#ga3a92f69fa10c95e3e645dd51f9bb0f3c", null ]
];