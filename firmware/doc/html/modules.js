var modules =
[
    [ "Peripheral Drivers", "group___peripheral___drivers.html", "group___peripheral___drivers" ],
    [ "Common Utility Headers - LUFA/Drivers/Common/Common.h", "group___group___common.html", "group___group___common" ],
    [ "SPI Driver - LUFA/Drivers/Peripheral/SPI.h", "group___group___s_p_i.html", "group___group___s_p_i" ],
    [ "USB Core - LUFA/Drivers/USB/USB.h", "group___group___u_s_b.html", "group___group___u_s_b" ],
    [ "USB Class Drivers", "group___group___u_s_b_class_drivers.html", "group___group___u_s_b_class_drivers" ]
];