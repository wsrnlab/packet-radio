var group___group___host___a_v_r8 =
[
    [ "HOST_DEVICE_SETTLE_DELAY_MS", "group___group___host___a_v_r8.html#ga4591576cd26d635aa5dfdf992ed99a3f", null ],
    [ "USB_HOST_DEVICEADDRESS", "group___group___host___a_v_r8.html#ga8233ed35f4abbb180ec8304ce483d406", null ],
    [ "USB_Host_EnumerationErrorCodes_t", "group___group___host___a_v_r8.html#ga647b09a2205a9beda86049b6b8f004a0", [
      [ "HOST_ENUMERROR_NoError", "group___group___host___u_c3_b.html#gga647b09a2205a9beda86049b6b8f004a0af109c8e2f136312daf33c3f24813075f", null ],
      [ "HOST_ENUMERROR_WaitStage", "group___group___host___u_c3_b.html#gga647b09a2205a9beda86049b6b8f004a0a1dae3bd36f1c5a9f5e106fff9a42cc66", null ],
      [ "HOST_ENUMERROR_NoDeviceDetected", "group___group___host___u_c3_b.html#gga647b09a2205a9beda86049b6b8f004a0a10683800ce0b14ee049c9a50fdc101b9", null ],
      [ "HOST_ENUMERROR_ControlError", "group___group___host___u_c3_b.html#gga647b09a2205a9beda86049b6b8f004a0ad4e5a60748230db93193081b06e61a06", null ],
      [ "HOST_ENUMERROR_PipeConfigError", "group___group___host___u_c3_b.html#gga647b09a2205a9beda86049b6b8f004a0aa54e1f3548f38b80968903c585710de0", null ],
      [ "HOST_ENUMERROR_NoError", "group___group___host___u_c3_b.html#gga647b09a2205a9beda86049b6b8f004a0af109c8e2f136312daf33c3f24813075f", null ],
      [ "HOST_ENUMERROR_WaitStage", "group___group___host___u_c3_b.html#gga647b09a2205a9beda86049b6b8f004a0a1dae3bd36f1c5a9f5e106fff9a42cc66", null ],
      [ "HOST_ENUMERROR_NoDeviceDetected", "group___group___host___u_c3_b.html#gga647b09a2205a9beda86049b6b8f004a0a10683800ce0b14ee049c9a50fdc101b9", null ],
      [ "HOST_ENUMERROR_ControlError", "group___group___host___u_c3_b.html#gga647b09a2205a9beda86049b6b8f004a0ad4e5a60748230db93193081b06e61a06", null ],
      [ "HOST_ENUMERROR_PipeConfigError", "group___group___host___u_c3_b.html#gga647b09a2205a9beda86049b6b8f004a0aa54e1f3548f38b80968903c585710de0", null ]
    ] ],
    [ "USB_Host_ErrorCodes_t", "group___group___host___a_v_r8.html#ga3e4d1d4775626c3d29543f4a3154d74d", [
      [ "HOST_ERROR_VBusVoltageDip", "group___group___host___u_c3_b.html#gga3e4d1d4775626c3d29543f4a3154d74dac4b5e8b0a3bfb8abf6c5a4f62b1901ce", null ],
      [ "HOST_ERROR_VBusVoltageDip", "group___group___host___u_c3_b.html#gga3e4d1d4775626c3d29543f4a3154d74dac4b5e8b0a3bfb8abf6c5a4f62b1901ce", null ]
    ] ]
];