var _u_s_b_controller___u_c3_8h =
[
    [ "USB_CLOCK_REQUIRED_FREQ", "_u_s_b_controller___u_c3_8h.html#ga52823066e0454a395d7d047a12435260", null ],
    [ "USB_OPT_GCLK_CHANNEL_0", "_u_s_b_controller___u_c3_8h.html#ga23cdfbc038b4841977505997d7aa9ce9", null ],
    [ "USB_OPT_GCLK_CHANNEL_1", "_u_s_b_controller___u_c3_8h.html#gaab84f7914d17013e35a153a7bbc9bfc6", null ],
    [ "USB_OPT_GCLK_SRC_OSC", "_u_s_b_controller___u_c3_8h.html#gafb356f3d50cffc0e71044f8e209db27f", null ],
    [ "USB_OPT_GCLK_SRC_PLL", "_u_s_b_controller___u_c3_8h.html#ga624506bc3bf97779e2bdfca608fb2ae9", null ],
    [ "USB_Modes_t", "_u_s_b_controller___u_c3_8h.html#ga750c4e36c6792e03b80ab04d2d0b6ddb", [
      [ "USB_MODE_None", "_u_s_b_controller___u_c3_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba9941f94cd940d822c81bf88405bccfb3", null ],
      [ "USB_MODE_Device", "_u_s_b_controller___u_c3_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaba0991704fbb65de7d2ed168036cd656", null ],
      [ "USB_MODE_Host", "_u_s_b_controller___u_c3_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba4c57ef994d66a3b4d95fb6cc13b8babb", null ],
      [ "USB_MODE_UID", "_u_s_b_controller___u_c3_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaddfec606070e7f77eb4e71ba325aa7e4", null ],
      [ "USB_MODE_None", "_u_s_b_controller___u_c3_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba9941f94cd940d822c81bf88405bccfb3", null ],
      [ "USB_MODE_Device", "_u_s_b_controller___u_c3_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaba0991704fbb65de7d2ed168036cd656", null ],
      [ "USB_MODE_Host", "_u_s_b_controller___u_c3_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba4c57ef994d66a3b4d95fb6cc13b8babb", null ],
      [ "USB_MODE_UID", "_u_s_b_controller___u_c3_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaddfec606070e7f77eb4e71ba325aa7e4", null ],
      [ "USB_MODE_None", "_u_s_b_controller___u_c3_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba9941f94cd940d822c81bf88405bccfb3", null ],
      [ "USB_MODE_Device", "_u_s_b_controller___u_c3_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaba0991704fbb65de7d2ed168036cd656", null ]
    ] ],
    [ "USB_Disable", "_u_s_b_controller___u_c3_8h.html#ga60cf524c4acc0ccae14186b4a572316b", null ],
    [ "USB_Init", "_u_s_b_controller___u_c3_8h.html#ga22b285b5403cf48fc8fc587921278563", null ],
    [ "USB_ResetInterface", "_u_s_b_controller___u_c3_8h.html#ga12464051e3763a6ac6ccf8eaa9f40324", null ],
    [ "USB_CurrentMode", "_u_s_b_controller___u_c3_8h.html#gaf61a61231449336fa28b9d6dc57e19b5", null ],
    [ "USB_Options", "_u_s_b_controller___u_c3_8h.html#ga3b2989fdb6af51351e8fc62a4a82a399", null ]
];