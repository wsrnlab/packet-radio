var annotated =
[
    [ "ATTR_PACKED", "struct_a_t_t_r___p_a_c_k_e_d.html", "struct_a_t_t_r___p_a_c_k_e_d" ],
    [ "Endpoint_FIFO_t", "struct_endpoint___f_i_f_o__t.html", "struct_endpoint___f_i_f_o__t" ],
    [ "Endpoint_FIFOPair_t", "struct_endpoint___f_i_f_o_pair__t.html", "struct_endpoint___f_i_f_o_pair__t" ],
    [ "USB_ClassInfo_CDC_Device_t", "struct_u_s_b___class_info___c_d_c___device__t.html", "struct_u_s_b___class_info___c_d_c___device__t" ],
    [ "USB_ClassInfo_CDC_Host_t", "struct_u_s_b___class_info___c_d_c___host__t.html", "struct_u_s_b___class_info___c_d_c___host__t" ],
    [ "USB_Descriptor_Configuration_t", "struct_u_s_b___descriptor___configuration__t.html", "struct_u_s_b___descriptor___configuration__t" ],
    [ "USB_Endpoint_Table_t", "struct_u_s_b___endpoint___table__t.html", "struct_u_s_b___endpoint___table__t" ],
    [ "USB_Pipe_Table_t", "struct_u_s_b___pipe___table__t.html", "struct_u_s_b___pipe___table__t" ]
];