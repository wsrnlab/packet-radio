var _u_s_b_controller_8h =
[
    [ "ENDPOINT_DIR_IN", "_u_s_b_controller_8h.html#gabe19393ecfcac9ea1ee8c6b3dc87830a", null ],
    [ "ENDPOINT_DIR_MASK", "_u_s_b_controller_8h.html#gac9f42369e91e331606a3c031ab10bc7b", null ],
    [ "ENDPOINT_DIR_OUT", "_u_s_b_controller_8h.html#gab011e2ee1e9f55b60cda3dbc79b95961", null ],
    [ "EP_TYPE_BULK", "_u_s_b_controller_8h.html#ga07d9641305bdb74348ed3f7d063d2eb5", null ],
    [ "EP_TYPE_CONTROL", "_u_s_b_controller_8h.html#ga36e2febaffc004d9a16ffbb2e19702ff", null ],
    [ "EP_TYPE_INTERRUPT", "_u_s_b_controller_8h.html#gaca8e10a5688781c86e0317b63d06ecb8", null ],
    [ "EP_TYPE_ISOCHRONOUS", "_u_s_b_controller_8h.html#ga76b5473683ee6153560cc800ef615ce1", null ],
    [ "EP_TYPE_MASK", "_u_s_b_controller_8h.html#ga98ea7b67ef1cacbedac5283a033eee27", null ],
    [ "PIPE_DIR_IN", "_u_s_b_controller_8h.html#ga697a43d20bcb36ccbb3ec0a1210b7dfa", null ],
    [ "PIPE_DIR_MASK", "_u_s_b_controller_8h.html#ga37a86265af9fc64b69222aaa754580ed", null ],
    [ "PIPE_DIR_OUT", "_u_s_b_controller_8h.html#gad3d4d84bf9b8092917bec0ae1b4511af", null ]
];