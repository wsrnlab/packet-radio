var _u_s_b_controller___a_v_r8_8h =
[
    [ "USB_OPT_AUTO_PLL", "_u_s_b_controller___a_v_r8_8h.html#ga47443eb243a07f418206ee6ec23949e7", null ],
    [ "USB_OPT_MANUAL_PLL", "_u_s_b_controller___a_v_r8_8h.html#ga360c15bbd5a5f90ee4e0e01ab0f12242", null ],
    [ "USB_OPT_REG_DISABLED", "_u_s_b_controller___a_v_r8_8h.html#gaad47d5101b932d235b970225a661336c", null ],
    [ "USB_OPT_REG_ENABLED", "_u_s_b_controller___a_v_r8_8h.html#ga3437fe2544e58716d50234997a2c96d1", null ],
    [ "USB_Modes_t", "_u_s_b_controller___a_v_r8_8h.html#ga750c4e36c6792e03b80ab04d2d0b6ddb", [
      [ "USB_MODE_None", "_u_s_b_controller___a_v_r8_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba9941f94cd940d822c81bf88405bccfb3", null ],
      [ "USB_MODE_Device", "_u_s_b_controller___a_v_r8_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaba0991704fbb65de7d2ed168036cd656", null ],
      [ "USB_MODE_Host", "_u_s_b_controller___a_v_r8_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba4c57ef994d66a3b4d95fb6cc13b8babb", null ],
      [ "USB_MODE_UID", "_u_s_b_controller___a_v_r8_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaddfec606070e7f77eb4e71ba325aa7e4", null ],
      [ "USB_MODE_None", "_u_s_b_controller___a_v_r8_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba9941f94cd940d822c81bf88405bccfb3", null ],
      [ "USB_MODE_Device", "_u_s_b_controller___a_v_r8_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaba0991704fbb65de7d2ed168036cd656", null ],
      [ "USB_MODE_Host", "_u_s_b_controller___a_v_r8_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba4c57ef994d66a3b4d95fb6cc13b8babb", null ],
      [ "USB_MODE_UID", "_u_s_b_controller___a_v_r8_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaddfec606070e7f77eb4e71ba325aa7e4", null ],
      [ "USB_MODE_None", "_u_s_b_controller___a_v_r8_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddba9941f94cd940d822c81bf88405bccfb3", null ],
      [ "USB_MODE_Device", "_u_s_b_controller___a_v_r8_8h.html#gga750c4e36c6792e03b80ab04d2d0b6ddbaba0991704fbb65de7d2ed168036cd656", null ]
    ] ],
    [ "USB_Disable", "_u_s_b_controller___a_v_r8_8h.html#ga60cf524c4acc0ccae14186b4a572316b", null ],
    [ "USB_Init", "_u_s_b_controller___a_v_r8_8h.html#ga22b285b5403cf48fc8fc587921278563", null ],
    [ "USB_ResetInterface", "_u_s_b_controller___a_v_r8_8h.html#ga12464051e3763a6ac6ccf8eaa9f40324", null ],
    [ "USB_CurrentMode", "_u_s_b_controller___a_v_r8_8h.html#gaf61a61231449336fa28b9d6dc57e19b5", null ],
    [ "USB_Options", "_u_s_b_controller___a_v_r8_8h.html#ga3b2989fdb6af51351e8fc62a4a82a399", null ]
];