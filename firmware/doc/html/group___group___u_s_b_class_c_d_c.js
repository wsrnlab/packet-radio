var group___group___u_s_b_class_c_d_c =
[
    [ "Common Class Definitions", "group___group___u_s_b_class_c_d_c_common.html", "group___group___u_s_b_class_c_d_c_common" ],
    [ "CDC Class Device Mode Driver", "group___group___u_s_b_class_c_d_c_device.html", "group___group___u_s_b_class_c_d_c_device" ],
    [ "CDC Class Host Mode Driver", "group___group___u_s_b_class_c_d_c_host.html", "group___group___u_s_b_class_c_d_c_host" ]
];