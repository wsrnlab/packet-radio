var _u_s_b_interrupt___u_c3_8h =
[
    [ "USB_Interrupts_t", "_u_s_b_interrupt___u_c3_8h.html#aa3c91c0637607c108960a91967076ded", [
      [ "USB_INT_VBUSTI", "_u_s_b_interrupt___u_c3_8h.html#aa3c91c0637607c108960a91967076deda56fe9fd3acb4826e3f9289ae9ad5befc", null ],
      [ "USB_INT_BUSEVENTI", "_u_s_b_interrupt___u_c3_8h.html#aa3c91c0637607c108960a91967076deda4bc8418c1da13ca5d3bf23c8caa583ee", null ],
      [ "USB_INT_BUSEVENTI_Suspend", "_u_s_b_interrupt___u_c3_8h.html#aa3c91c0637607c108960a91967076deda17848b454ca770fb1f814a7ccbeb29c2", null ],
      [ "USB_INT_BUSEVENTI_Resume", "_u_s_b_interrupt___u_c3_8h.html#aa3c91c0637607c108960a91967076dedabbbb4a0621f4474c3ddd46f8014aaee1", null ],
      [ "USB_INT_BUSEVENTI_Reset", "_u_s_b_interrupt___u_c3_8h.html#aa3c91c0637607c108960a91967076dedae8d5904e19fb2541aae3ba4dd302891b", null ],
      [ "USB_INT_SOFI", "_u_s_b_interrupt___u_c3_8h.html#aa3c91c0637607c108960a91967076deda215f32397d5482dcd43bc0058906267c", null ]
    ] ],
    [ "ISR", "_u_s_b_interrupt___u_c3_8h.html#a28bf4c54d9527b4a20eb142b6cf3d66a", null ],
    [ "USB_INT_ClearAllInterrupts", "_u_s_b_interrupt___u_c3_8h.html#a534870c0b71c45fcd698bc8401e9a553", null ],
    [ "USB_INT_DisableAllInterrupts", "_u_s_b_interrupt___u_c3_8h.html#a6c0fc0fad08c06c28813c253a0de814b", null ],
    [ "USB_Endpoint_SelectedEndpoint", "_u_s_b_interrupt___u_c3_8h.html#aec656f0f710144246889dbb8e8344c51", null ]
];