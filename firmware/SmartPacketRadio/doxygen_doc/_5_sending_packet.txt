/*	\file
 *
 *	This file contains the contents of the "Sending a Packet" page in the Doxygen documentation
 *	This is not a source file of the Smart Packet Radio project.
 */
	
/*!	\page sending_packet Sending a Packet
The packet is stored in a buffer on the microcontroller and once the full packet has been received the tx_mode function is called:

		void tx_mode(void)
		{
			write(0x07, 0x01);              // To ready mode
			cbi(PORTC, RXANT);        // Clears RXANT
			sbi(PORTC, TXANT);        // Sets TXANT
			write(0x08, 0x03);              // FIFO reset
			write(0x08, 0x00);              // Clear FIFO
		}
		
This function sets the antenna to be used for transmission and clears the FIFO on the transceiver. This is done to ensure that the correct data is sent. Once this has been done, the transmit function is called:

		void transmit(unsigned char *tx_packet)
		{
			write(0x3A, tx_packet[0]);
			write(0x3E, (tx_packet[2]+3));
			for (int i=0; i&lt;(tx_packet[2]+3); i++) {
				write(0x7F, tx_packet[i]);     // Send payload to the FIFO
			}
			write(0x05, 0x04);                    // Enable packet sent interrupt
			write(0x06, 0x00);
			tx_mode_enabled = true;      // Flag used to identify source of interrupt
			write(0x07, 0x09);                    // Start TX
		}
		
The transmit function takes the buffer of characters sent from the host as an input. It writes the destination address to be used as Header Byte 3 which will be checked when other radios receive the packet and also sets the packet length. The packet is then written to the transceiver modules FIFO. An interrupt is enabled on the transceiver which is used to notify the microcontroller when the packet has been completely sent and a flag is set which prevents another packet from being read on the USB input before the current pack is sent. Another flag is set and checked in the ISR to determine what the interrupt was for. The microcontroller then sends the command for the transceiver to begin transmission.
Once the transmission is complete, an interrupt is sent to the microcontroller. When this happens, the ISR sets a flag high and reads the interrupt status registers located on the transceiver. The actual response to the interrupt takes place in the main loop of code. The interrupt status registers and relevant flags are checked and once it is determined that the interrupt was due to a valid packet being sent the following events occur:
- The transceiver is taken out of transmission mode and placed into ready mode
- The TX LED is turned on and a timer is started which will turn it off after an arbitrary amount of time.
- The relevant transmission flags are cleared.
- The rx_mode function is called, placing the transceiver into a state awaiting a packet.


*/