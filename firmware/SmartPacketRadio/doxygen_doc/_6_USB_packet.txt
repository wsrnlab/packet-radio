/**	\file USB packet page
 *
 *	This file only the content of the "USB Packet" page for the Doxygen document.
 *	This is a source file for the firmware.
 */
 
 /*! \page Page_USBpacket USB Packet Structure
 
		\tableofcontents
		
		\section Main_Struct Packet Structure
		\subsection Host2SPR Host to Smart Packet Radio
		\subsubsection Reset Reset (0x01)
			
		BYTE0   | BYTE1
		:------:|:-------:
		0x01    | -
			
		Instruction to reset the board
			
		\subsubsection Set_TX Set Transmission Mode (0x02)
		
		BYTE0   | BYTE1
		:------:|:-------:
		0x02    | MODE_SELECT
			
		Instruction to set the transmission mode. MODE_SELECT can be one of the following, depending on the desired
		mode:-
		<table>
			<tr>
				<td>0x00</td>
				<td>Direct Mode</td>
			</tr>
			<tr>
				<td>0x01</td>
				<td>Aloha Mode</td>
			</tr>
			<tr>
				<td>0x02</td>
				<td>CSMACA Mode</td>
			</tr>
		</table>
		
		\subsubsection Set_ADDRESS Set Address (0x08)
		
		BYTE0    | BYTE1
		:-------:|:-------:
		0x08     | ADDRESS
		
		Instruction to tell the Smart Packet Radio to set its own address to ADDRESS.
		
		ADDRESS can only assume a value 0-255.
		
		\subsubsection TX Transmit (0x10)
		
		BYTE0   | BYTE1  | BYTE2      | BYTE3|BYTE4|BYTE5| BYTE6       | BYTE7   | BYTE8   | BYTE9~  
		:------:|:------:|:----------:|:----:|:---:|:---:|:-----------:|:-------:|:-------:|:-------
		0x10    | DEST   | PACKET_LEN | DEST | SRC | CTL | PAYLOAD_LEN | RX_NODE | TX_NODE | PAYLOAD
		
		Instruction to transmit a packet from the connected Smart Packet Radio to another Smart Packet Radio.
		
		\note
		The packet will transmit in whatever mode it has been previous set to by the \ref Set_TX . Please set 
		the transmit mode via \ref Set_TX before you use this instruction if you are unsure. 
		
		\note
		Please use the \ref Set_ADDRESS commands to send the address of each Smart Packet Radio involved separately
		before the use of this instruction
		
		<table>
			<tr>
				<td>DEST</td>
				<td>Destination address of the packet (Value between 0-255)</td>
			</tr>
			<tr>
				<td>PACKET_LEN</td>
				<td>Length of the packet in bytes</td>
			</tr>
			<tr>
				<td>SRC</td>
				<td>Source address of the packet (Value between 0-255)</td>
			</tr>
			<tr>
				<td>CTL</td>
				<td>Control byte (Needs Documentation)</td>
			</tr>
			<tr>
				<td>PAYLOAD_LEN</td>
				<td>Length of the payload packet</td>
			</tr>
			<tr>
				<td>RX_NODE</td>
				<td>Not implemented, leave as 0xFF</td>
			</tr>
			<tr>
				<td>TX_NODE</td>
				<td>Not implemented, leave as 0xFF</td>
			</tr>
			<tr>
				<td>PAYLOAD</td>
				<td>The actual payload of the packet. The maximum size for the payload is 255 bytes</td>
			</tr>
		</table>
		
		\subsubsection TEST Testing Mode (0x15)
		
		BYTE0    | BYTE1
		:-------:|:-------:
		0x15     | -
		
		Instruction to test the throughput of the Smart Packet Radio. (Need Documentation)
		
		\subsubsection Set_Backoff Set Backoff Limit (0x20)
		
		BYTE0    | BYTE1
		:-------:|:-------:
		0x20     | BACKOFF
		
		Instruction to set the Backoff Limit (Needs more Documentation)
		
		\subsubsection Set_SS Set Slot Time (0x40)
		
		BYTE0    | BYTE1
		:-------:|:-------:
		0x40     | TIME_SELECT
		
		Instruction to set the slot time for the packet. TIME_SELECT will be able to assume 
		one of these values:(Needs more Documentation)
		
		<table>
			<tr>
				<td>Byte value</td>
				<td>0x00</td>
				<td>0x01</td>
				<td>0x02</td>
				<td>0x03</td>
				<td>0x04</td>
				<td>0x05</td>
				<td>0x06</td>
				<td>0x07</td>
				<td>0x08</td>
				<td>0x09</td>
			</tr>
			<tr>
				<td>Slot time</td>
				<td>0</td>
				<td>519</td>
				<td>1010</td>
				<td>1502</td>
				<td>1993</td>
				<td>2485</td>
				<td>2976</td>
				<td>3304</td>
				<td>4942</td>
				<td>6582</td>
			</tr>
		</table>
		
		\subsubsection Set_MAX_TX Set Maximum Transmissions (0x80)
		
		BYTE0    | BYTE1
		:-------:|:-------:
		0x80     | MAX_LIMIT
		
		(Needs more Documentation)
		
		\subsubsection Query_ADXL345 Query the ADXL345 Chip (0x03)

		BYTE0    | BYTE1
		:-------:|:-------:
		0x03     | -		
		
		Instruction to poll the ADXL345 vibration sensor for acceleration data. The response will in the format
		in the structure documentation in \ref RX_ADXL345.
		
		\subsubsection Query_DS18B20 Query the DS18B20 Temperature Sensor (0x04)
		
		BYTE0    | BYTE1
		:-------:|:-------:
		0x04     | -	

		Instruction to poll the DS18B20 vibration sensor for temperature data. The response will in the format
		in the structure documentation in \ref RX_DS18B20.
		
		\subsubsection Set_ADXL345Mode Set the ADXL345 Interrupt Mode (0x05)
		
		BYTE0    | BYTE1
		:-------:|:-------:
		0x05     | MODE	
		
		Instruction to set the ADXL345 vibration sensor to trigger interrupts in the particular mode specified by
		MODE. The following table specifies the modes in which the firmware will read:-
		
		<table>
			<tr>
				<td>0x00</td>
				<td>Disable Interrupts</td>
			</tr>
			<tr>
				<td>0x01</td>
				<td>Set Activity Interrupt (trigger on activity)</td>
			</tr>
		</table>
		
		\subsubsection Query_Version Query the Firmware Version (0x06)
		
		BYTE0    | BYTE1
		:-------:|:-------:
		0x06     | -	

		Instruction to query for the firmware version of the Smart Packet Radio. The response will in the format
		in the structure documentation in \ref RX_Version.
		
		\note Firmware version below revision 433 does not have this function built-in, as such, will not respond
		to this query instruction
		
		\subsubsection Set_LED Set the colour of an LED (0x07)
		
		BYTE0    | BYTE1   | BYTE2    |
		:-------:|:-------:|:---------|
		0x07     | LED_ID  | COLOUR   |

		Instruction to set the colour of an LED on Smart Packet Radio. LED_ID will identify which LED, as labeled on the 
		Smart Packet Radio itself, to change the colour of whilst COLOUR specifies which colour to change it to.
		
		Below are the colour codes in which COLOUR can assume:-
		
		<table>
			<tr>
				<td>0x00</td>
				<td>OFF</td>
			</tr>
			<tr>
				<td>0x01</td>
				<td>BLUE</td>
			</tr>
			<tr>
				<td>0x02</td>
				<td>RED</td>
			</tr>
			<tr>
				<td>0x03</td>
				<td>MAGENTA</td>
			</tr>
			<tr>
				<td>0x04</td>
				<td>GREEN</td>
			</tr>
			<tr>
				<td>0x05</td>
				<td>CYAN</td>
			</tr>
			<tr>
				<td>0x06</td>
				<td>YELLOW</td>
			</tr>
			<tr>
				<td>0x07</td>
				<td>WHITE</td>
			</tr>
		</table>		
		

		\subsection SPR2Host Smart Packet Radio to Host
			
		\subsubsection RX_Valid Valid Packet (0x81)
		
		BYTE0    | BYTE2|BYTE3|BYTE4| BYTE5      | BYTE6   | BYTE7   | BYTE8~ 
		:-------:|:----:|:---:|:---:|:-----------:|:-------:|:-------:|:-------
		0x81     | DEST | SRC | CTL | PAYLOAD_LEN | RX_NODE | TX_NODE | PAYLOAD
		
		Contains the valid packet that was received by the radio module on the Smart Packet Radio.
		
		<table>
			<tr>
				<td>DEST</td>
				<td>Destination address of the packet (Value between 0-255)</td>
			</tr>
			<tr>
				<td>SRC</td>
				<td>Source address of the packet (Value between 0-255)</td>
			</tr>
			<tr>
				<td>CTL</td>
				<td>Control byte (Needs Documentation)</td>
			</tr>
			<tr>
				<td>PAYLOAD_LEN</td>
				<td>Length of the payload packet</td>
			</tr>
			<tr>
				<td>RX_NODE</td>
				<td>Not implemented, leave as 0xFF</td>
			</tr>
			<tr>
				<td>TX_NODE</td>
				<td>Not implemented, leave as 0xFF</td>
			</tr>
			<tr>
				<td>PAYLOAD</td>
				<td>The actual payload of the packet. The maximum size for the payload is 255 bytes</td>
			</tr>
		</table>
		
		\subsubsection RX_Stat Statistics Packet (0x7E)
		
		BYTE0    | BYTE2|BYTE3|BYTE4| BYTE5       | BYTE6   | BYTE7   | BYTE8-11 
		:-------:|:----:|:---:|:---:|:-----------:|:-------:|:-------:|:---------:
		0x7E     | SRC  | SRC | 0x00| 0x06        | SRC     | SRC     | DELAY_INTRS
		
		Contains a statistical packet from the radio module on the Smart Packet Radio.
		
		<table>
			<tr>
				<td>SRC</td>
				<td>Source address of the packet (Value between 0-255)</td>
			</tr>
			<tr>
				<td>DELAY_INTRS</td>
				<td>A 32-bit word that contains the number of timer increments between transmission and acknowledgement</td>
			</tr>
		</table>
		
		\subsubsection RX_ADXL345 Query Reply for ADXL345 (0xF1)
		
		BYTE0    | BYTE1   | BYTE2  | BYTE3  | BYTE4  | BYTE5  | BYTE6   |
		:-------:|:-------:|:------:|:------:|:------:|:------:|:-------:|
		0xF1     | DATAX0  | DATAX1 | DATAY0 | DATAY1 | DATAZ0 | DATAZ1  |
		
		The reply packet to the query for the Vibration Sensor. The ADXL345 accelerometer chip outputs data
		in 2 registers per axis. DATAx0 is the lower byte and DATAx1 is the upper byte of the halfword that 
		contains the full value of the reading. 
		
		\subsubsection RX_DS18B20 Query Reply for DS18B20 (0xF2)
		
		BYTE0    | BYTE1   | BYTE2    | BYTE3    |
		:-------:|:-------:|:--------:|:---------|
		0xF2     | DIGIT   | DECIMAL1 | DECIMAL0 |

		The reply packet to the query for the Temperature Sensor. The Smart Packet Radio will translate the
		data outputted by the DS18B20 chip and split it into 2 numbers. The DIGIT byte, that contains the whole numbers
		of the temperature, and DECIMAL halfword, that contains the decimal component of the temperature. An increment of 1 in
		the DECIMAL halfword is 0.0001 of a degree.
		
		\subsubsection RX_Version Query Reply for Firmware Version (0xF3)
		
		BYTE0    | BYTE1-13      |
		:-------:|:--------------|
		0xF3     | "$Rev:: ###$" |

		The reply packet to the query for the Firmware Version. The firmware version is directly linked to the revision
		number in the SVN repository. The reply packet will be 14 bytes long and the payload will be a string as seen in
		the table above with ### replaced with the revision number.
		
		
 
 */