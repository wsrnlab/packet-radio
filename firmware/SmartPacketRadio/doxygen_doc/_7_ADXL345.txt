/**	\file Vibration Sensor Page
 *
 *	This file only the content of the "Vibration Sensor (ADXL345)" page for the Doxygen document.
 *	This is a source file for the firmware.
 */
 
 /*! \page ADXL345 Vibration Sensor (ADXL345)
 
		The Smart Packet Radio has limited compatibility with the Smart Packet Radio at its current stage. However
		the foundation has been laid in the firmware to be able to expand its capabilities with internal functions.
		
		The firmware features the following:
		- Writing/Reading to ADXL345 registers
		- Custom op-code from the USB packet to poll the sensor for data
		
		The vibration sensor uses the Analog Devices' ADXL345 accelerometer for the sensing. The chip features
		a 10-bit precision measurement of the acceleration the chip is experiencing in either a +- 2g/4g/8g/16g
		range. The sensor uses the SPI protocol to communicate through PORTD of the Smart Packet Radio.
		
		PORTD on the Smart Packet Radio has special function in which it has direct access to the USART, which can be 
		configured to use the SPI protocol, and 2 external interrupt lines that can be used for interrupts from the ADXL345
		chip.
		
		The following table features the pin layout for PORTD and which lines it will connect to on the vibration sensor
		breakout board.
		
		<table>
			<tr>
				<td>INT1</td>
				<td>MOSI (Tx)</td>
				<td>SCL</td>
				<td>-</td>
				<td>3.3V</td>
			</tr>
			<tr>
				<td>INT2</td>
				<td>MISO (Rx)</td>
				<td>~CS</td>
				<td>-</td>
				<td>GND</td>
			</tr>
		</table>
 */