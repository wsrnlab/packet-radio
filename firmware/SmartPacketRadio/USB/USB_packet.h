/**
 * USB_packet.h
 *
 * Created: 9/01/2014 12:17:56 PM
 *  Author: Moses Wan
 */ 

/**	\file
 *
 *	Header file that contains the control byte definitions of the API of the USB Packet
 */
#ifndef USB_PACKET_H_
#define USB_PACKET_H_

	#include <LUFA/Drivers/USB/USB.h>
	#include <stdbool.h>
	#include "../drivers/ADXL345/ADXL345.h"
	#include "../drivers/ds18b20/ds18b20.h"
	#include "../drivers/rfm22/rfm22.h"
	#include "../drivers/rfm22/rfm22_aloha.h"
	#include "../drivers/rfm22/rfm22_csmaca.h"
	#include "../tools/throughput_test.h"

        #include "USB_packet_defs.h"

	/**
	 * \brief Function Handler for handling incoming USB packets
	 * 
	 *	
	 * \param API_instruction The first byte of the USB packet that contains instruction codes
	 * \param tx_buffer The buffer which the transmission data should be placed
	 * \param rx_buffer The buffer which the received data will be placed in
	 * 
	 * \return void
	 */
	void API_Handler(int API_instruction, unsigned char* tx_buffer, unsigned char* rx_buffer);

#endif /* USB_PACKET_H_ */
