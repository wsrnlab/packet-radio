/*
 * USB_packet.c
 *
 * Created: 9/01/2014 12:17:30 PM
 *  Author: Moses
 */ 

/**
 * \file
 * The main program file that contains the handler for the USB communication between the host
 * and the Smart Packet Radio.
 */

#include "USB_packet.h"
#include <string.h>

int SLOT_TIME_ARRAY[] = {0, 519, 1010, 1502, 1993, 2485, 2976, 3304, 4942, 6582};   // Array of preset slot time values (15 ms increments). (Set via trial and error testing)
int SLOT_TIME = 2485;                                                               // The slot time in timer counts. Default is ~75ms.
int LEN_SLOT_TIME_ARRAY = sizeof(SLOT_TIME_ARRAY)/sizeof(*(SLOT_TIME_ARRAY));       // Finds the length of the slot time array

extern bool TRANSMITTING;
extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern char version[14];
int BACKOFF_LIMIT = 6;																 // 2^BACKOFF_LIMIT - 1 is the max value of the back-off timer (default value = 6)
unsigned char SOURCE_ADDRESS = 0xFF;  

//================================//
//Function to read in data packets//
//================================//
void ProcessPacket(unsigned char* tx_buffer)
{
	bool done = 0;
	int tx_count = 0;
	while(!done)
	{
		int16_t received_byte = CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
		if(received_byte >= 0)
		{
			switch(tx_count)
			{
				// Byte after initial control byte contains destination address
				// Next byte contains length of data to send
				case 0: case 1:
					tx_buffer[tx_count] = received_byte;
					tx_count++;
				break;
				default:
					if(tx_count < tx_buffer[1]+1)
					{
						tx_buffer[tx_count] = received_byte;
						tx_count++;
					}
					else
					{
						tx_buffer[tx_count] = received_byte;
						tx_count = 0;
						done = 1;
					}
				break;
			}
		}
	}
}

void API_Handler(int API_instruction, unsigned char* tx_buffer, unsigned char* rx_buffer)
{
	// TODO: check, if it is not a bit dangerous to switch a 16-bit int with byte values
	int slot_time_index;
	static unsigned char MAC_MODE = 0x00;
	unsigned char LEDid, colour;
	
	
	switch(API_instruction)
	{
		case USB_PACKET_RESET:  // Reset
			rfm22_reset();
			break;
			
		case USB_PACKET_SET_TX_MODE:  // Set transmission mode
			MAC_MODE = CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
			break;

		case USB_PACKET_SET_ADDRESS:  // Set unique source address
			SOURCE_ADDRESS = CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
			rfm22_set_address(SOURCE_ADDRESS);
			break;
			
		case USB_PACKET_TRANSMIT:  // Transmit
			ProcessPacket(tx_buffer);
			
			if(MAC_MODE == SET_ALOHA_MODE){		// Aloha Mode
				rfm22_transmit_aloha(tx_buffer, rx_buffer);
			}
			else if(MAC_MODE == SET_CSMACA_MODE)		// CSMACA Mode
			{
				rfm22_transmit_csmaca(tx_buffer, rx_buffer);
			}
				else
			{									// Direct Mode
				TRANSMITTING = true;
				rfm22_tx_mode();
				rfm22_raw_transmit(tx_buffer, true);
			}

			break;

		case USB_PACKET_TEST:  // Throughput testing
			throughput_test_start(tx_buffer);
			break;
		
		case USB_PACKET_SET_BACKOFF:  // Set back-off limit
			BACKOFF_LIMIT = CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
			break;
		
		case USB_PACKET_SET_SLOTTIME:  // Set slot time from an array of preset values
			slot_time_index = CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
			// If a valid address, change the slot time else toggle LED2 to blue
			if(slot_time_index < LEN_SLOT_TIME_ARRAY)
			{
				SLOT_TIME = SLOT_TIME_ARRAY[slot_time_index];
				PORTC |= (0x01 << 0x00);  // Clears the blue on the RGB LED
			}
			else
			{
				//Clears LEDs
				PORTC |= (0x01 << 0x00);
				PORTE |= (0x03 << 0x00);

				PORTC &= ~(0x01 << 0x00); // Causes RGB LED to go blue representing an failure to change slot time
			}
			break;
			
		case USB_PACKET_SET_MAX_TX:  //Set Maxmimum Transmissions
			// TODO: wisenet sets this to 1, which results in no retransmissions and is therefore considered a bug!
			//rfm22_set_max_transmissions(CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface));
			CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
			break;
			
		case USB_PACKET_QUERY_ADXL345:
		
			rx_buffer[0] = USB_PACKET_REPLY_ADXL345;							// Op-code
			ADXL345_readReg(&rx_buffer[1], 6, ADXL345_REG_DATAX0);		// Read the 6 bytes from the data registers, starting from DATAX0
			CDC_Device_SendData(&VirtualSerial_CDC_Interface, (char *) rx_buffer, 7);
			break;
			
		case USB_PACKET_SETMODE_ADXL345:
			
			ADXL345_setMode(CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface));
			break;
			
		case USB_PACKET_QUERY_DS18B20:
			
			if(bit_is_clear(PINA, PA6))
			{
				rx_buffer[0] = USB_PACKET_REPLY_DS18B20;
				therm_read_temperature((char *) &rx_buffer[1]);
				
				CDC_Device_SendData(&VirtualSerial_CDC_Interface, (char *) rx_buffer, 4);
			}
			break;
		
		case USB_PACKET_QUERY_VERSION:
			CDC_Device_SendData(&VirtualSerial_CDC_Interface, version, 14);		// Sends off the version string
			break;
			
		case USB_PACKET_LED:
			LEDid = CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
			colour = CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
			
			switch(LEDid)
			{
				case 0x01:
					PORTE |= 0x03;					//Pulls the LED lines high to turn them off
					PORTC |= 0x01;
					PORTE &= ~(colour >> 1);		// Sets G and R for LED1
					PORTC &= ~(colour & 0x01);		// Sets B for LED1
					break;
				default:
					break;
			}
			
		
		default:
			break;
		}
}

