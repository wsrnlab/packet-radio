#ifndef USB_PACKET_DEFS_H_
 #define USB_PACKET_DEFS_H_

 #define USB_PACKET_RESET           0x01	///< Reset Board
 #define USB_PACKET_SET_TX_MODE     0x02	///< Set Transmission mode
 #define USB_PACKET_SET_ADDRESS     0x08	///< Set unique source address
 #define USB_PACKET_TRANSMIT        0x10	///< Transmit
 #define USB_PACKET_TEST            0x15	///< Throughput testing
 #define USB_PACKET_SET_BACKOFF     0x20	///< Set back-off limit
 #define USB_PACKET_SET_SLOTTIME    0x40	///< Set Slot Time
 #define USB_PACKET_SET_MAX_TX      0x80	///< Set Maximum Transmissions
 #define USB_PACKET_QUERY_ADXL345   0x03	///< Query the vibration sensor
 #define USB_PACKET_QUERY_DS18B20   0x04	///< Query the temperature sensor
 #define USB_PACKET_SETMODE_ADXL345 0x05	///< Set interrupt mode for vibration sensor
 #define USB_PACKET_QUERY_VERSION   0x06	///< Query for the version number
 #define USB_PACKET_LED             0x07	///< Set an LED color

 #define USB_PACKET_STATS_RX        0x7E	///< Received Statistic Packet 
 #define USB_PACKET_VALID_RX        0x81	///< Received Valid Packet
 #define USB_PACKET_REPLY_ADXL345   0xF1	///< Reply to vibration sensor query
 #define USB_PACKET_REPLY_DS18B20   0xF2	///< Reply to temperature sensor query
 #define USB_PACKET_REPLY_VERSION   0xF3	///< Reply with Version Number

 #define USB_PACKET_QUERY_VER_SIZE     14 // Bytes
 #define USB_PACKET_REPLY_DS18B20_SIZE 7  // Bytes
 #define USB_PACKET_LED_SIZE           3  // Bytes

 /// Type that contains the transmission modes for outgoing packets
 enum {
    SET_DIRECT_MODE = 0x00,
    SET_ALOHA_MODE  = 0x01,
    SET_CSMACA_MODE = 0x02
 } tx_mode_t;

enum {
    LED1 = 0x01,
    LED2 = 0x02
} led_id_t;

enum {
    OFF     =    0x00,
    BLUE    =	 0x01,
    RED	    =    0x02,
    MAGENTA =	 0x03,
    GREEN   = 	 0x04,
    CYAN    =	 0x05,
    YELLOW  =	 0x06,
    WHITE   = 	 0x07
} led_color_t;

#endif /* USB_PACKET_DEFS_H_ */
