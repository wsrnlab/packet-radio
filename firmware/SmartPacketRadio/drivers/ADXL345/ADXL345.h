/*
 * ADXL345.h
 *	
 * Created: 3/01/2014 3:43:26 PM
 *  Author: Moses Wan
 */ 

/**	\file
 *	\brief Vibration Sensor Driver
 *	
 *	Interfaces the Smart Packet Radio with ADXL345 accelerometer that acts as a vibration sensor.
 *	Contains macros to the internal registers of the ADXL345 chip and the reset vectors for them.
 */

/**
 *	\defgroup Peripheral_Drivers Peripheral Drivers
 *	\brief Drivers for additional boards that can be attached to the Smart Packet Radio.
 */

/**	\ingroup Peripheral_Drivers
 *	\defgroup ADXL345_Drivers Vibration Sensor Driver (ADXL345)
 *	\brief Drivers for the ADXL345 based Vibration Sensor Board
 *	
 *	The drivers for the Vibration Sensor Board is designed with the fact that the board will be attached
 *	to the PORTD headers on the Smart Packet Radio. The board uses an ADXL345 accelerometer chip to detect
 *	vibrations.
 *	@{
 */
#ifndef ADXL345_H_
#define ADXL345_H_

	/* Necessary library for PORTD Serial SPI connection */
	#include <util/delay.h>
	#include <avr/io.h>

	
	/// Type that is used for referencing internal registers of the ADXL345 chip
	enum ADXL345_reg_t {
		ADXL345_REG_DEVID = 0x00,				///< Device ID
		ADXL345_REG_THRESH_TAP = 0x1D,			///< Tap threshold
		ADXL345_REG_OFSX = 0x1E,				///< X-axis offset
		ADXL345_REG_OFSY = 0x1F,				///< Y-axis offset
		ADXL345_REG_OFSZ = 0x20,				///< Z-axis offset
		ADXL345_REG_DUR = 0x21,					///< Tap duration
		ADXL345_REG_LATENT = 0x22,				///< Tap latency
		ADXL345_REG_WINDOW = 0x23,				///< Tap window
		ADXL345_REG_THRESH_ACT = 0x24,			///< Activity threshold
		ADXL345_REG_THRESH_INACT = 0x25,		///< Inactivity threshold
		ADXL345_REG_TIME_INACT = 0x26,			///< Inactivity time
		ADXL345_REG_ACT_INACT_CTL = 0x27,		///< Axis enable control for activity and inactivity detection
		ADXL345_REG_THRESH_FF = 0x28,			///< Free-fall threshold
		ADXL345_REG_TIME_FF = 0x29,				///< Free-fall time
		ADXL345_REG_TAP_AXES = 0x2A,			///< Axis control for single tap/double tap
		ADXL345_REG_ACT_TAP_STATUS = 0x2B,		///< Source of single tap/double tap
		ADXL345_REG_BW_RATE = 0x2C,				///< Data rate and power mode control
		ADXL345_REG_POWER_CTL = 0x2D,			///< Power-saving features control
		ADXL345_REG_INT_ENABLE = 0x2E,			///< Interrupt enable control
		ADXL345_REG_INT_MAP = 0x2F,				///< Interrupt mapping control
		ADXL345_REG_INT_SOURCE = 0x30,			///< Source of interrupts
		ADXL345_REG_DATA_FORMAT = 0x31,			///< Data format control
		ADXL345_REG_DATAX0 = 0x32,				///< X-Axis Data 0
		ADXL345_REG_DATAX1 = 0x33,				///< X-Axis Data 1
		ADXL345_REG_DATAY0 = 0x34,				///< Y-Axis Data 0
		ADXL345_REG_DATAY1 = 0x35,				///< Y-Axis Data 1
		ADXL345_REG_DATAZ0 = 0x36,				///< Z-Axis Data 0
		ADXL345_REG_DATAZ1 = 0x37,				///< Z-Axis Data 1
		ADXL345_REG_FIFO_CTL = 0x38,			///< FIFO control
		ADXL345_REG_FIFO_STATUS = 0x39			///< FIFO status
	};

	/** @name Reset vectors
	 *	Macros for the reset vectors for the registers
	 */
	///@{
	#define ADXL345_RESET_DEVID 0xE5			///< Reset vector for DEVID
	#define ADXL345_RESET_DEFAULT 0x00			///< Reset vector for general registers
	#define ADXL345_RESET_BW_RESET 0x0D			///< Reset vector for BW_RESET
	#define ADXL345_RESET_INT_SOURCE 0x02		///< Reset vector for INT_SOURCE
	/// @}
	
	/// Definitions for interrupt modes that can be used
	enum ADXL345_mode_t {
		ADXL345_MODE_ACT = 0x01,		///< Activity Interrupt
		ADXL345_MODE_NONE = 0x00		///< Disable all interrupts
	};
	
	/**
	 * \brief 
	 * Used to USART1 interface to communicate with the ADXL345 accelerometer.
	 * 
	 * \return void
	 */
	void ADXL345_Init();
	
	
	/**
	 * \brief Function to write to the internal registers of the ADXL345 chip.
	 * 
	 * \param	reg The address of the register that is being written to.
	 * \param	data The data which is to be written to the register.
	 * 
	 * \return void
	 */
	void ADXL345_writeReg(enum ADXL345_reg_t reg, uint8_t data);
	
	/**
	 * \brief Function to read values from the internal register of the ADXL345 chip.
	 * 
	 * \param	buffer The buffer in which the read value will be stored into.
	 * \param	len The number of byte to be read.
	 * \param	reg The address of the register that is being read from.
	 * 
	 * \return void
	 */
	void ADXL345_readReg(uint8_t *buffer, int len, enum ADXL345_reg_t reg);
	
	/**
	 * \brief Function used to set up interrupts
	 * 
	 * \param mode Mode in which to configure interrupt
	 * 
	 * \return void
	 */
	void ADXL345_setMode(enum ADXL345_mode_t mode);

#endif 
/** @} */
/* ADXL345_H_ */