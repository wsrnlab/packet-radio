/*
 * ADXL345.h
 *	
 * Created: 3/01/2014 3:43:26 PM
 *  Author: Moses Wan
 */

#include "ADXL345.h"
#include "../../USB/USB_packet.h"

#define USART1SPI_UBBRVAL(Baud)       ((Baud < (F_CPU / 2)) ? ((F_CPU / (2 * Baud)) - 1) : 0)	// Macro to set the Baud rate

void USART1SPI_Init();
uint8_t USART1SPI_TrasnferByte(const uint8_t DataByte);

void ADXL345_Init()
{
	
	/*	Initialize the SPI protocol for the PORTD USART		*/
	USART1SPI_Init();
	
	/*	Putting the ADXL345 device into Measurement Mode	*/
	ADXL345_writeReg(ADXL345_REG_DATA_FORMAT, 0x01);
	ADXL345_writeReg(ADXL345_REG_POWER_CTL, 0x08);
	
	
	return;
}



/** Public Function used to write to the ADXL345 registers */
void ADXL345_writeReg(enum ADXL345_reg_t reg, uint8_t data)
{
	
	/*	The ADXL345 requires the first bit to be a 0 followed by the address
		then the data to write something into the registers		*/
	
	PORTD &= ~(1 << PD4);				// Pull down the ~(CS) line to the ADXL345 chip to enable it
	USART1SPI_TrasnferByte(reg);		// Transfers the register and the data information
	USART1SPI_TrasnferByte(data);
	PORTD |= (1 << PD4);				// Pull up the ~(CS) line to the ADXL345 chip to disable it
	return;
}

void ADXL345_setMode(enum ADXL345_mode_t mode)
{
		static enum ADXL345_mode_t current_mode;		// Saves the current mode
		static uint8_t ADXL345_int_vect;				// Variable for future expansion if more interrupt features are going to be used
		
		// If the current mode is the same as the new mode, do nothing.
		if(mode == current_mode)
		{
			return;
		}
		
		// Check conditions for the new mode
		if(mode == ADXL345_MODE_ACT)
		{
			ADXL345_writeReg(ADXL345_REG_INT_MAP, 0xEF);			// Act interrupt to INT1, else, all to INT2
			ADXL345_writeReg(ADXL345_REG_THRESH_ACT, 0x38);			// 62.5e-3 g threshold
			ADXL345_writeReg(ADXL345_REG_ACT_INACT_CTL, 0xF0);		// Activity detection in all axis and set to relative
			ADXL345_readReg(NULL, 1, ADXL345_REG_INT_SOURCE);		// Clear the interrupt source register
			
			ADXL345_int_vect = 0x10;
			ADXL345_writeReg(ADXL345_REG_INT_ENABLE, ADXL345_int_vect);	// Enable activity interrupts
			current_mode = mode;
		}
		else
		{
			
			ADXL345_int_vect = 0x00;
			ADXL345_writeReg(ADXL345_REG_INT_ENABLE, ADXL345_int_vect); // Disable all interrupts
			current_mode = mode;
		}
};

/** Public Function used to read data from the ADXL345 registers.
 *	The function can make use of multi-byte reads and consumes let clock cycles to get
 *	more than one byte.
 */
void ADXL345_readReg(uint8_t *buffer, int len, enum ADXL345_reg_t reg)
{
	
	/*	The ADXL345 requires the first bit to be a 1 followed by the address.
		The response will be a byte long.		*/
	
	buffer[0] = reg | 0x80;					// Set bit 7 of the address to indicate a read instruction
	
	if (len > 1) buffer[0] |= 0x40;			// If it is a multi-byte read, set bit 6 in the instruction
	
	PORTD &= ~(1 << PD4);					// Pull down the ~(CS) line to the ADXL345 chip to enable it
	USART1SPI_TrasnferByte(buffer[0]);		// Send the instruction
	
	
	for(int i=0; i< len; i++)				// Loop a bunch of dummy values to get the values back (refer to data sheet)
	{
		buffer[i] = USART1SPI_TrasnferByte(0x00);	
	}
	PORTD |= (1 << PD4);					// Pull up the ~(CS) line to the ADXL345 chip to disable it
	return;
}

void USART1SPI_Init()
{	
	
	UBRR1 = 0x0000;	// Set baud rate to 0

	DDRD |= ((1 << DDD5) | (1 << DDD4)); 	// Set clock to output
	
	
	UCSR1C = ((1 << UMSEL10) | (1 << UMSEL11) | (1 << UCPOL1) | (1 << UCSZ10));		// Set both UMSEL10 and UMSEL11 into Master SPI mode and SPI Mode 3 as per required
	UCSR1B = (1 << TXEN1)  | (1 << RXEN1);			// Enabling transmission and reception
	
	
	UBRR1 = (uint16_t) USART1SPI_UBBRVAL(2000000);	// Set baud rate of 2MHz
	PORTD |= (1 << PD4);				// Pull high the ~(CS) to disable the ADXL345 chip till needed

}

uint8_t USART1SPI_TrasnferByte(const uint8_t DataByte)
{
	while (!(UCSR1A & (1<<UDRE1)));		// Wait till data buffer is empty
	UDR1 = DataByte;					// Place the data into the data buffer to send
	while (!(UCSR1A & (1<<RXC1)));		// Wait till the receive operation is complete
	return UDR1;						// Return the value that was pulled back during transmission
}

