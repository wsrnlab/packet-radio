/** \file 
 *
 *	\brief DS18B20 Temperature Sensor Header file
 *	
 *	This file contains the basic read/write functions for the DS18B20 Temperature sensor and
 *	contains important definitions for the commands to control the Temperature Sensor.
 *	
 */

/** \ingroup Peripheral_Drivers
 *	\defgroup DS18B20_Drivers Temperature Sensor Driver (DS18B20)
 *	\brief Drivers for the DS18B20 Temperature Sensor
 *
 *	The driver controls the Temperature Sensor which is on-board the Smart Packet Radio.The component that is used for
 *	temperature sensing is the DS18B20 chip.
 *	@{
 */

#ifndef DS18B20_H_
#define DS18B20_H_

	#include <stdio.h>
	#include <avr/io.h>
	#include <util/delay.h>
	
	/** @name Temperature Sensor Conditions
	 *	
	 *	Macros that contains definitions concerning pin locations to the temperature sensor
	 */
	///@{
	#define THERM_PORT PORTC		///< The port on the MCU that the temeperature sensor is connected to
	#define THERM_DDR DDRC			///< The direction register that controls the data direction of the temperature sensor
	#define THERM_PIN PINC			///< The power pin for the temperature sensor
	#define THERM_DQ PC7			///< The data bus line for the temperature sensor
	/// @}

	/** @name Utilities
	 *	
	 */
	///@{
	#define THERM_INPUT_MODE() THERM_DDR&=~(1<<THERM_DQ)		///< Set Temperature sensor to Input Mode
	#define THERM_OUTPUT_MODE() THERM_DDR|=(1<<THERM_DQ)		///< Set Temperature sensor to Output Mode
	#define THERM_LOW() THERM_PORT&=~(1<<THERM_DQ)				///< Pulls down the bus line
	#define THERM_HIGH() THERM_PORT|=(1<<THERM_DQ)				///< Pulls up the bus line
	/// @}

	
	/** @name Commands Definitions
	 *	Macros for the call commands for the operation of the temperature sensor
	 */
	///@{
	#define THERM_CMD_CONVERTTEMP 0x44			///< Command to initiate a single temperature conversion
	#define THERM_CMD_RSCRATCHPAD 0xbe			///< Command to read the scratchpad
	#define THERM_CMD_WSCRATCHPAD 0x4e			///< Command to write bytes into the scratchpad
	#define THERM_CMD_CPYSCRATCHPAD 0x48		///< Command to copy registers from the scratchpad to EEPROM
	#define THERM_CMD_RECEEPROM 0xb8			///< Command to recall the alarm trigger values and configuration data from EEPROM to scratchpad
	#define THERM_CMD_RPWRSUPPLY 0xb4			///< Command to signal the DS18B20 to the Smart Packet Radio
	#define THERM_CMD_SEARCHROM 0xf0			///< Command to search for the devices on the bus
	#define THERM_CMD_READROM 0x33				///< Command to read the ROM of the temperature sensor
	#define THERM_CMD_MATCHROM 0x55				///< Command to address a specific slave on the bus
	#define THERM_CMD_SKIPROM 0xcc				///< Command to address all the slaves simultaneously
	#define THERM_CMD_ALARMSEARCH 0xe			///< Command to address slaves which has its alarm condition triggered
	#define THERM_DECIMAL_STEPS_12BIT 625		
	/// @}

	//==========//
	//Prototypes//
	//==========//
	/**
	 * \brief Reset Temperature Sensor
	 * 
	 * \return uint8_t Returns a 0 for success and 1 for failure
	 */
	uint8_t therm_reset(void);

	/**
	 * \brief Reads data from the Temperature Sensor
	 * 
	 * \param buffer The byte in which to store the data in. The function will output data in a ["+XXX.XXXX C"] format
	 *	\note The buffer must be at least 12 bytes long
	 * 
	 * \return void
	 */
	void therm_read_temperature(char *buffer);
	
#endif
/// @}
/* DS18B20_H_ */