/** \file
 *
 *  This file contains the CSMA/CA implementation for the SPR using the RFM22B transceiver.
 * 
 *  Author(s):
 *      Jordan Guest [jordannguest(at)gmail.com]
 *      Nick D'Ademo [nickdademo(at)gmail.com]
 *      Benjamin Foo [bsmf91(at)gmail(dot)com]
 *      Felix Shzu-Juraschek [fjuraschek(at)gmail(dot)com]
 */

#include <math.h>
#include <stdlib.h>
#include "rfm22_csmaca.h"
#include "rfm22.h"
#include "SmartPacketRadio.h"

//============================================//
//Function to transmit packets in CSMA/CA mode//
//============================================//
void rfm22_transmit_csmaca(unsigned char* tx_buffer, unsigned char * rx_buffer)
{
    uint8_t delay_count;
    uint8_t transmission_count;                 // number of transmission attempts
    uint8_t transmission_count_real;            // number of transmission attempts
    long long int timeout_count = 0;            // save the current timer value in ticks
    long long int timeout_limit = 0;            // timeout limit for waiting operations
    long long int total_wait_time = 0;          // total backoff time in ticks
    char *stats_packet;							// statistics packet to send the result to the host via serial comm
    long int window_size = MIN_WINDOW_SIZE;     // initialize window size with the minimum
    long unsigned int backoff = 0;              // holds the number of slots to backoff in case of unsuccessful transmit attempt

    VALID_ACK = false;          // resets the flag
    transmission_count = 0;     // reset transmission attempts
    transmission_count_real = 0;     // reset transmission attempts
    
    DELAY_INTRS = 0;            // reset overflow counter
    TCNT2 = 0;                  // reset counter
    TCCR2B = 0x01;              // start timer2
    
    // try to transmit as long as no valid ACK has been received or the max number of transmission attempts is reached
    while(!VALID_ACK && (transmission_count < MAX_TRANSMISSIONS))
    {
        transmission_count +=1;                                         // increment transmission attempts
        
        //====== check with carrier sensing if the air is free
        rfm22_cs_enable();                                              // enable carrier sensing
        
        timeout_count = (256 * DELAY_INTRS) + TCNT2;                    // Current value of delay timer
        timeout_limit = timeout_count + DIFS;                           // set carrier sensing duration to DIFS (~4.5ms) 
        
        while ((timeout_count < timeout_limit) && !rfm22_cs_medium_busy()) // Wait until timeout is finished
        {
            rfm22_irq_handler(rx_buffer);
            timeout_count = (256 * DELAY_INTRS) + TCNT2;                // update counter   
        }
        rfm22_cs_disable();                                             // disable carrier sensing

        //====== start transmission if the medium has been sensed free
        if(!rfm22_cs_medium_busy())
        {
            // Transmitting
            TRANSMITTING = true;
            rfm22_tx_mode();
            rfm22_raw_transmit(tx_buffer, true);

            transmission_count_real++;
            // wait until the transmission is finished (packet sent ir fires)
            while(TRANSMITTING)
            {
                rfm22_irq_handler(rx_buffer);
            }
            
            timeout_count = (256 * DELAY_INTRS) + TCNT2;                // update value of delay timer
        
            // If this is a broadcast message, continue without waiting for ACK
            if(tx_buffer[2] == 0xFF)
            {
                VALID_ACK = true;
				break;
            }

            timeout_limit = timeout_count + (3200);     // wait here for the ACK for double SLOT_TIME (100ms)
            while((timeout_count < timeout_limit) && !VALID_ACK)
            {
                rfm22_irq_handler(rx_buffer);                                           // check, if a received packet ir was fired
                if(MAC_PACKET_FLAG){
                    rfm22_receive(rx_buffer, &tx_buffer[2]);       // process the received packet (ACK)
                    MAC_PACKET_FLAG = false;
                }
                timeout_count = (256 * DELAY_INTRS) + TCNT2; // update counter
            }
        }
        
        //====== calculate new backoff and wait, in case of failed transmission
        if(!VALID_ACK && (transmission_count < MAX_TRANSMISSIONS))
        {
            
            timeout_count = (256 * DELAY_INTRS) + TCNT2;        // get current time stamp
            
            window_size *= 2;                                   // double the window size
            if(window_size > MAX_WINDOW_SIZE)                   // check if max window size is exceeded
                window_size = MAX_WINDOW_SIZE;                  // if so, use max window size instead
            
            //Random number generator (from StackOverflow)
            do
            {
                backoff = rand()/(RAND_MAX/(window_size + 1) );
            }
            while(backoff > window_size);
            total_wait_time = (backoff * CSMACA_SLOT_TIME);     // calculate total wait time
            timeout_limit = timeout_count + total_wait_time;    // calculate timer limit to wait until
            
            while (timeout_count < timeout_limit)               // wait until timeout is finished
            {
                timeout_count = (256 * DELAY_INTRS) + TCNT2;    // update timer counter   
            }
        }
    }

    //====== clean up and sent status over serial comm
    TCCR2B = 0x00;          // stops delay timer
    delay_count = TCNT2;    // reads the timer

    // if no ACK received, set transmissions = 0 to indicate failure to send
    if(!VALID_ACK)
    {
        transmission_count = 0;  // indicates error
        transmission_count_real = 5;
    }
    
    // Creates and 'sends' statistics 'packet' to the serial comm
	stats_packet = (char *) rx_buffer;
    stats_packet[0] = 0x7E; // Identifies packet as statistics packet
    stats_packet[1] = SOURCE_ADDRESS;
    stats_packet[2] = SOURCE_ADDRESS;
    stats_packet[3] = 0;
    stats_packet[4] = 6;
    stats_packet[5] = SOURCE_ADDRESS;
    stats_packet[6] = SOURCE_ADDRESS;
    stats_packet[7] = transmission_count_real;
    stats_packet[8] = delay_count;
	*(&stats_packet[9]) = DELAY_INTRS;

    // send statistics packet over the serial comm
	CDC_Device_SendData(&VirtualSerial_CDC_Interface, stats_packet, 13);
}