/** \file
 *
 *  Header file for the CSMA/CA implementation for the SPR using the RFM22B transceiver.
 * 
 *  Author(s):
 *      Jordan Guest [jordannguest(at)gmail.com]
 *      Nick D'Ademo [nickdademo(at)gmail.com]
 *      Benjamin Foo [bsmf91(at)gmail(dot)com]
 *      Felix Shzu-Jursachek [fjuraschek(at)gmail(dot)com]
 */

#ifndef _RFM22_CSMACA_H_
#define _RFM22_CSMACA_H_

        /* Includes: */
        #include <avr/io.h>
        #include <avr/wdt.h>
        #include <avr/interrupt.h>
        #include <avr/power.h>
        
        #include <stdbool.h>
        
        #define CSMACA_SLOT_TIME 2400		///< Used for backoff - the value is much higher than the actual slot time
		#define DIFS 50						///< DIFS for normal data packets, the default wait time
		#define MIN_WINDOW_SIZE 4			///< Minimum window size of backoff algorithm
		#define MAX_WINDOW_SIZE 16			///< Maximum window size of backoff algorithm
		       
        
        /* Function Prototypes: */
        void rfm22_transmit_csmaca(unsigned char*, unsigned char*);
        
#endif
