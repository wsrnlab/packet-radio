
/** \file
 *
 *  Driver for the RFM22B transceiver. This file contains the basic functions and 
 *  initialisation of the transceiver.
 * 
 *  Author(s):
 *      Jordan Guest [jordannguest(at)gmail.com]
 *      Nick D'Ademo [nickdademo(at)gmail.com]
 *      Benjamin Foo [bsmf91(at)gmail(dot)com]
 *      Felix Shzu-Jursachek [fjuraschek(at)gmail(dot)com]
 */

#include "rfm22.h"

#include "SmartPacketRadio.h"
#include "../../tools/throughput_test.h"

unsigned char int1;                             // NOTE: must be lower case
unsigned char int2;                             // NOTE: must be lower case
volatile bool IRQ_TRIGGERED = false;            // Flag indicating when an IRQ has occurred
bool TX_MODE_ENABLED;                           // Flag used to show the board has just sent a packet
bool RX_MODE_ENABLED;                           // Flag used to show the board is waiting to receive a packet
uint8_t MAX_TRANSMISSIONS = 4;                  // The maximum number of times the radio will attempt to transmit a packet
bool CS_BUSY = false;                           // Flag indicating that the air is busy

// Initialize the RFM22 for transmitting
void rfm22_init(void)
{
    /*
    //=======================//
    //Register initialisation//
    //=======================//
      Address Read/Write - Function
    */

    _delay_ms(50);
    rfm22_write(0x07, 0x81);  // Reset register values on RFM22B
    _delay_ms(50);

    //0x00 R - Device type
    //0x01 R - Device version
    //0x02 R - Device status
    //0x03 R - Interrupt status 1
    //0x04 R - Interrupt status 2
    //0x05 R/W - Interrupt Enable 1   
    rfm22_write(0x05, 0x00);          // Disable all interrupts
    //0x06 R/W - Interrupt Enable 2
    rfm22_write(0x06, 0x00);          // Disable all interrupts but RSSI/CCA
    //0x07 R/W - Operating function and control 2
    rfm22_write(0x07, 0x01);          // Set READY mode
    //0x08 R/W - Operating function and control 2
    //0x09 R/W - Crystal oscillator load capacitance        
    rfm22_write(0x09, 0x7F);          // Cap = 12.5pF
    //0x0A R/W - Microcontroller output clock
    rfm22_write(0x0A, 0x05);          // Clk output is 2MHz
    //0x0B R/W - GPIO_0 configuration       
    rfm22_write(0x0B, 0xF4);          // GPIO0 is for RX data output
    //0x0C R/W - GPIO_1 configuration
    rfm22_write(0x0C, 0xEF);          // GPIO1 is TX/RX data CLK output
    //0x0D R/W - GPIO_2 configuration        
    rfm22_write(0x0D, 0x00);              
    //0x0E R/W - I/O port configuration
    rfm22_write(0x0E, 0x00);              
    //0x0F R/W - ADC configuration        
    rfm22_write(0x0F, 0x70);             
    //0x10 R/W - ADC sensor amplifier offset
    rfm22_write(0x10, 0x00);              
    //0x11 R - ADC value
    //0x12 R/W - Temperature sensor control
    rfm22_write(0x12, 0x00);              
    //0x13 R/W - Temperature offset value
    rfm22_write(0x13, 0x00);              
    //0x14 R/W - Wake-up timer period 1        
    //0x15 R/W - Wake-up timer period 2
    //0x16 R/W - Wake-up timer period 3
    //0x17 R - Wake-up timer value 1
    //0x18 R - Wake-up timer value 2
    //0x19 R/W - Low-duty cycle mode duration
    //0x1A R/W - Low battery detector threshold
    //0x1B R - Battery voltage level
    //0x1C R/W - IF filter bandwidth
    rfm22_write(0x1C, 0x88);        // 200kbps 
    //  rfm22_write(0x1C, 0x83);    // 150kbps
    //  rfm22_write(0x1C, 0x9A);    // 100kbps              
    //0x1D R/W - AFC loop gearshift override
    rfm22_write(0x1D, 0x3C);        // 200kbps & 150kbps & 100kbps       
    //0x1E R/W - AFC timing control
    rfm22_write(0x1E, 0x02);        // 200kbps & 150kbps & 100kbps 
    //0x1F R/W - Clock recovery gearshift override
    rfm22_write(0x1F, 0x03);        // 200kbps & 150kbps & 100kbps
    //0x20 R/W - Clock recovery oversampling ratio        
    rfm22_write(0x20, 0x3C);        // 200kbps  
    //  rfm22_write(0x20, 0x50);    // 150kbps
    //rfm22_write(0x20, 0x3C);    // 100kbps           
    //0x21 R/W Clock recovery offset 2 
    rfm22_write(0x21, 0x02);        // 200kbps 
    //  rfm22_write(0x21, 0x01);    // 150kbps
    //rfm22_write(0x21, 0x02);    // 100kbps       
    //0x22 R/W - Clock recovery offset 1
    rfm22_write(0x22, 0x22);        // 200kbps
    //  rfm22_write(0x22, 0x99);    // 150kbps
    //rfm22_write(0x22, 0x22);    // 100kbps
    //0x23 R/W - Clock recovery offset 0
    rfm22_write(0x23, 0x22);        // 200kbps
    //  rfm22_write(0x23, 0x9A);    // 150kbps 
    //rfm22_write(0x23, 0x22);    // 100kbps        
    //0x24 R/W - Clock recovery timing loop gain 1
    rfm22_write(0x24, 0x07);        // 200kbps & 150 kbps & 100kbps       
    //0x25 R/W - Clock recovery timing loop gain 0
    rfm22_write(0x25, 0xFF);        // 200kbps & 150 kbps & 100kbps              
    //0x26 R - Received signal strength indicator
    //0x27 R/W - RSSI threshold for clear channel indicator
    rfm22_write(0x27, 0x45);
    //0x28 R - Antenna dicersity register 1       
    //0x29 R - Antenna dicersity register 2
    //0x2A R/W - AFC limiter
    rfm22_write(0x2A, 0xFF);        // 200kbps & 150 kbps & 100kbps
    //0x2B R - AFC correction read
    //0x2C R/W - OOK counter value 1
    rfm22_write(0x2C, 0x00);
    //0x2D R/W - OOK counter value 2
    rfm22_write(0x2D, 0x00);
    //0x2E R/W Slicer peak hold
    rfm22_write(0x2E, 0x00);
    //0x2F - RESERVED
    //0x30 R/W - Data access control              
    rfm22_write(0x30, 0x8C);              
    //0x31 R - EzMAC Status
    //0x32 R/W - Header control 1        
    rfm22_write(0x32, 0x88);         // Check header byte 3 and enable broadcast byte (0xFF)          
    //0x33 R/W - Header control 2        
    rfm22_write(0x33, 0x10);         // Sets length of header to 1 byte       
    //0x34 R/W - Preamble length        
    rfm22_write(0x34, 4);            // 4 nibble = 2 byte preamble
    //0x35 R/W - Preamble detection control
    rfm22_write(0x35, 0x20);              
    //0x36 R/W - Sync word 3
    rfm22_write(0x36, 0x2D);              
    //0x37 R/W - Sync word 2
    //0x38 R/W - Sync word 1
    //0x39 R/W - Sync word 0
    //0x3A R/W - Transmit header 3  //Transmit header set dynamically in transmit function           
    //0x3B R/W - Transmit header 2
    //0x3C R/W - Transmit header 1
    //0x3D R/W - Transmit header 0
    //0x3E R/W - Packet length      //Packet length is set dynamically in transmit function
    //0x3F R/W - Check header 3
    rfm22_write(0x3F, SOURCE_ADDRESS);
    //0x40 R/W - Check header 2
    //0x41 R/W - Check header 1
    //0x42 R/W - Check header 0
    //0x43 R/W - Header enable 3
    rfm22_write(0x43, 0xFF);          // Check all bits
    //0x44 R/W - Header enable 2
    //0x45 R/W - Header enable 1
    //0x46 R/W - Header enable 0
    //0x47 R - Received header 3
    //0x48 R - Received header 2
    //0x49 R - Received header 1
    //0x4A R - Received header 0        
    //0x4B R - Received packet length
    //0x4C to 0x4E - RESERVED
    //0x4F R/W - ADC8 control
    //0x50 to 0x5F - RESERVED
    //0x58 R/W - Changed whsen setting tx data rate
    rfm22_write(0x58, 0xED);       // 200kbps
    //rfm22_write(0x58, 0xC0);   // 150kbps & 100kbps
    //0x60 R/W - Channel filter coefficient address        
    //0x61 - RESERVED
    //0x62 R/W - Crystal oscillator / Control test
    //0x63 to 0x68 - RESERVED
    //0x69 R/W - AGC Override 1
    rfm22_write(0x69, 0x60);       // 200kbps & 150kbps & 100kbps
    //0x6A to 0x6C - RESERVED
    //0x6D R/W - TX power
    rfm22_write(0x6D, 0x00);       // TX power to min
    //0x6E R/W - TX data rate 1
    rfm22_write(0x6E, 0x33);       // 200kbps
    //  rfm22_write(0x6E, 0x26);   // 150kbps
    //rfm22_write(0x6E, 0x19);   // 100kbps              
    //0x6F R/W - TX data rate 0
    rfm22_write(0x6F, 0x33);       // 200kbps
    //  rfm22_write(0x6F, 0x66);   // 150kbps
    //rfm22_write(0x6F, 0x9A);   // 100kbps          
    //0x70 R/W - Modulcation mode control 1        
    rfm22_write(0x70, 0x0C);       // No manchester code, no data whiting
    //0x71 R/W - Modulcation mode control 2
    //rfm22_write(0x71, 0x02);     // Direct mode FSK
    rfm22_write(0x71, 0x22);       // FSK, fd[8]=0, no invert for TX/RX data, FIFO mode
    //0x72 R/W - Frequency deviation        
    rfm22_write(0x72, 0x50);       // 200kbps: Frequency deviation setting to 45K=72*625
    //0x73 R/W - Frequency offset 1        
    rfm22_write(0x73, 0x00);       // No frequency offset
    //0x74 R/W - Frequency offset 2        
    rfm22_write(0x74, 0x00);       // No frequency offset
    //0x75 R/W - Frequency band select
    rfm22_write(0x75, 0x53);       // frequency set to 434MHz
    //0x76 R/W - Nominal carrier frequency 1
    rfm22_write(0x76, 0x64);       // frequency set to 434MHz
    //0x77 R/W - Nominal carrier frequency 0  
    rfm22_write(0x77, 0x00);       // frequency set to 434Mhz
    //0x78 - RESERVED
    //0x79 R/W - Frequency hopping channel select
    rfm22_write(0x79, 0x00);       // no frequency hopping
    //0x7A R/W - Frequency hopping step size
    rfm22_write(0x7A, 0x00);       // no frequency hopping
    //0x7B - RESERVED
    //0x7C R/W - TX FIFO control 1
    //0x7D R/W - TX FIFO control 2
    //0x7E R/W - RX FIFO control
    //0x7F R/W - FIFO access    
    
    cbi(PORTC, TXANT);      // Clears TXANT
    cbi(PORTC, RXANT);      // Clears RXANT
}

//==============================================//
//Reads from specified register on RFM22B module//
//==============================================//
char rfm22_read(uint8_t address)
{
    address &= 0x7F;             // Sets MSB to 0 for read
    
    cbi(PORTB, CS);              // Selects RFM22B module
    SPI_SendByte (address);      // Sends address to read from
    char rx = SPI_ReceiveByte(); // Reads char
    sbi(PORTB, CS);              // Deselects module
    
    return rx;
}

//=================================================//
//Writes specified data to address on RFM22B module//
//=================================================//
void rfm22_write(uint8_t address, char data)
{
    address |= 0x80;        // Sets MSB to 1 for write
    
    cbi(PORTB, CS);         // Selects RFM22B module
    SPI_SendByte (address); // Sends address to write to
    SPI_SendByte (data);    // Writes data to selected address
    sbi(PORTB, CS);         // Deselects module
}

//=============================================================//
//Puts RFM22B into an Idle state, neither sending nor receiving//
//=============================================================//
void rfm22_idle_mode(void)
{
    rfm22_write(0x07, 0x01);      // To ready mode

    rfm22_write(0x08, 0x03);      // Clears FIFO
    rfm22_write(0x08, 0x00);      // Reset FIFO

    cbi(PORTC, TXANT);      // Clears TXANT
    cbi(PORTC, RXANT);      // Clears RXANT
}
//===============================================//
//Function to place RFM22B module to receive mode//
//===============================================//
void rfm22_rx_mode(void)
{
    rfm22_write(0x07, 0x01);      // To ready mode
    
    cbi(PORTC, TXANT);      // Clears TXANT
    sbi(PORTC, RXANT);      // Sets RXANT

    rfm22_write(0x07, 0x01);      // To ready mode

    rfm22_write(0x08, 0x03);      // Clears FIFO
    rfm22_write(0x08, 0x00);      // Resets FIFO

    // TODO: check
    rfm22_write(0x05, 0x02);      // Enables packet received interrupt
    //rfm22_write(0x06, 0x00);
    RX_MODE_ENABLED = true;

    rfm22_write(0x07, 0x05);      // Places RFM22B in RX mode
}

//=================================================//
//Function reads data from RX FIFO on RFM22B module//
//=================================================//
int rfm22_raw_receive(unsigned char *rx_buffer)
{
    unsigned char i;
    
    int receive_packet_length = rfm22_read(0x4B);

    for(i=0; i<receive_packet_length; i++)
    {
        rx_buffer[i] = rfm22_read(0x7F);
    }   
    return receive_packet_length;
}

//=======================================================//
//Function to handle received packets that require an ACK//
//=======================================================//
void rfm22_receive(unsigned char * rx_buffer, unsigned char *tx_buffer)
{
    // No need to ACK broadcast packets
    if(rx_buffer[0] == 0xFF)
    {
        return;
    }

    unsigned char ACK[6];   // Creates ACK packet

    // Checks received packet for ACK and correct sequence number
    if(rx_buffer[2] & 0x80)
    {

        if((rx_buffer[2] & 0x70) == ((tx_buffer[2] + 0x10) & 0x70))
        {
            VALID_ACK = true;
            TCCR2B = 0x00; // Stops delay timer
        }
    }
    else
    {
        // Creates and sends an ACK packet
        ACK[0] = rx_buffer[1];
        ACK[1] = SOURCE_ADDRESS;
        ACK[2] = ((0x01 << 0x07) | (rx_buffer[2] + 0x10)); // Sets ACK bit in type field and increments sequence number
        ACK[3] = 0x00;
        ACK[4] = rx_buffer[1];
        ACK[5] = SOURCE_ADDRESS;

        // Testing code
        //SEQ_TEST = 0x01;//ACK[2];
        
        // Transmitting ACK
        TRANSMITTING = true;
        rfm22_tx_mode();
        rfm22_raw_transmit(ACK, false);
        while(TRANSMITTING)
        {
            rfm22_irq_handler(rx_buffer);
        }
    }
}

//======================================================================//
//The function which handles interrupts from the RFM22B transceiver chip//
//======================================================================//
void rfm22_irq_handler(unsigned char* rx_buffer)
{

    // ISR
    if (IRQ_TRIGGERED)
    {       
        IRQ_TRIGGERED = false; // Resets flag
        // Packet sent interrupt
        if((int1 & (1 << 2)) && TX_MODE_ENABLED)     
        {
            rfm22_write(0x07, 0x01);          // Set to ready mode
            PORTB |= (0x01 << 0x06);    // Toggle TX LED on
            TXRX_LED_ON = true;
            TX_MODE_ENABLED = false;    // Disable tx_mode flag
            TRANSMITTING = false;
            rfm22_rx_mode();                   // Sets chip back to rx_mode
            if(TP_TEST) {
                throughput_test_send_packet();
            }
        }
        
        // Valid packet received interrupt
        else if(int1 & (1 << 1) && RX_MODE_ENABLED) 
        {
            rfm22_write(0x07, 0x01);       // Set to ready mode
            PORTB |= (0x01 << 0x05); // Turn TX LED on
            TXRX_LED_ON = true;
			
			rx_buffer[0] = USB_PACKET_VALID_RX;											// Identifies packet as received over wireless link
			rx_buffer[1] = rfm22_raw_receive(&rx_buffer[2]);                // Reads received packet and places bits in rx_buffer
            CDC_Device_SendData(&VirtualSerial_CDC_Interface, (char *) rx_buffer, rx_buffer[1] + 1);
            
            if(MAC_MODE > 0x00)
            {
                MAC_PACKET_FLAG = true;
            }
            rfm22_rx_mode();   // Sets chip back to rx_mode                    
        }
        // Unexpected interrupt
        // Measured RSSI is higher than the CCA threshold => medium is busy
        else if(int2 & (1 << 4)) 
        {
            CS_BUSY = true;
            rfm22_write(0x06, 0x00); // cancel interrupts for CCA
            //CDC_Device_SendByte(&VirtualSerial_CDC_Interface, int2);
        }
        else
        {
            // Clears LED
            PORTC |= (0x01 << 0x00);
            PORTE |= (0x03 << 0x00);
//             PORTE &= ~(0x01 << 0x00); // Causes RGB LED to go red representing an unrecognised interrupt
        }
    }
}


//=============================================================//
//Function that reads all of the registers in the RFM22B module//
//=============================================================//
/*
void rfm22_read_all_registers(void)
{
    unsigned char value;
    unsigned char i;
    for(i = 0; i<0x80; i++)
    {
        value = rfm22_read(i);
        CDC_Device_SendByte(&VirtualSerial_CDC_Interface, i);
        CDC_Device_SendByte(&VirtualSerial_CDC_Interface, value);
    }

    PORTE ^= (1 << 0);
}
*/
//=================================================================//
//Function that readies the RFM22B module for wireless transmission//
//=================================================================//
void rfm22_tx_mode(void)
{       
    rfm22_write(0x07, 0x01);      // To ready mode
    cbi(PORTC, RXANT);      // Clears RXANT
    sbi(PORTC, TXANT);      // Sets TXANT

    rfm22_write(0x08, 0x03);      // FIFO reset
    rfm22_write(0x08, 0x00);      // Clear FIFO                  
}

//=============================================//
//Function to Transmit Packet via RFM22B Module//
//=============================================//
void rfm22_raw_transmit(unsigned char *tx_packet, bool API_instruction_flag)
{
    if (API_instruction_flag)
    {
        rfm22_write(0x3A, tx_packet[0]);
        rfm22_write(0x3E, tx_packet[1]);
        for (int i=0; i<(tx_packet[1]); i++)
        {
            //CDC_Device_SendByte(&VirtualSerial_CDC_Interface, tx_packet[i]);
            rfm22_write(0x7F, tx_packet[i+2]);      // Send payload to the FIFO
        }
    }
    // Sending ACKs
    else    
    {
        rfm22_write(0x3A, tx_packet[0]);
        rfm22_write(0x3E, tx_packet[3]+6);
        for (int i=0; i<(tx_packet[3]+6); i++)
        {
            //CDC_Device_SendByte(&VirtualSerial_CDC_Interface, tx_packet[i]);
            rfm22_write(0x7F, tx_packet[i]);      // Send payload to the FIFO
        }
    }
    // TODO check
    rfm22_write(0x05, 0x04);      // Enable packet sent interrupt
    //rfm22_write(0x06, 0x00);
    TX_MODE_ENABLED = true; // Sets tx_mode flag
    
    rfm22_write(0x07, 0x09);      // Start TX
    
}

void rfm22_reset(void)
{
    MAC_MODE = 0x00;
    SOURCE_ADDRESS = 0xff;
    rfm22_write(0x3F, SOURCE_ADDRESS);
    SLOT_TIME = SLOT_TIME_ARRAY[5];
    BACKOFF_LIMIT = 6;
    MAX_TRANSMISSIONS = 4;
    TRANSMITTING = false;
    VALID_ACK = false;
}

void rfm22_set_address(unsigned char source_address)
{
    rfm22_write(0x3F, source_address);
}

void rfm22_set_max_transmissions(uint8_t max_t)
{
    MAX_TRANSMISSIONS = max_t;
}

//======================================================//
//Raises flag when interrupt received from RFM22B module//
//======================================================//
void rfm22_cs_enable()
{
    CS_BUSY = false;            // reset CS_BUSY to false
    rfm22_write(0x06, 0x10);    // start capture CCA interrupts
}

//======================================================//
//Raises flag when interrupt received from RFM22B module//
//======================================================//
void rfm22_cs_disable()
{
    rfm22_write(0x06, 0x00);    // stop capture CCA interrupts
}

//======================================================//
//Function to check if the medium is busy               //
//======================================================//
bool rfm22_cs_medium_busy()
{
    return CS_BUSY;    // returns if the medium is busy
}

//======================================================//
//Raises flag when interrupt received from RFM22B module//
//======================================================//
ISR(INT6_vect)
{
    IRQ_TRIGGERED = true;
    // Read interrupt status registers
    int1 = rfm22_read(0x03);
    int2 = rfm22_read(0x04);
    
    
    if(int2 & (1 << 4)) 
    {
        rfm22_write(0x06, 0x00);
        CS_BUSY = true;
        //CDC_Device_SendByte(&VirtualSerial_CDC_Interface, int2);
    }
    
}