
#ifndef _RFM22_H_
#define _RFM22_H_

        /* Includes: */
        #include <avr/io.h>
        #include <avr/wdt.h>
        #include <avr/interrupt.h>
        #include <avr/power.h>
        
        #include <stdbool.h>
		#include "../../USB/USB_packet.h"
        
        void rfm22_init(void);
        char rfm22_read(uint8_t);                                       // SPI read function
        void rfm22_write(uint8_t, char);                                // SPI write function
        void rfm22_rx_mode(void);                                       // Readies RFM22 for receiving
        void rfm22_receive(unsigned char*, unsigned char*);			 // Receives and ACKs a packet
        int rfm22_raw_receive(unsigned char*);                          // Reads received data from RFM22B
        void rfm22_idle_mode(void);                                     // Places RFM22B in an idle state
        void rfm22_irq_handler(unsigned char*);                         // Handles Interrupts
        void rfm22_read_all_registers(void);                            // Reads all the registers on the RFM22B
        void rfm22_tx_mode(void);                                       // Readies RFM22B for transmission
        void rfm22_raw_transmit(unsigned char*, bool);                  // Writes data to FIFO on RFM22B and sends 
        void rfm22_reset(void);                                         // Resets the RFM22B to default configuration 
        void rfm22_set_address(unsigned char);                          // Sets the address of the RFM22B
        void rfm22_set_max_transmissions(uint8_t);
        void rfm22_cs_enable(void);                                     // Enable carrier sensing for RFM22B  
        void rfm22_cs_disable(void);                                    // Disable carrier sensing for RFM22B 
        bool rfm22_cs_medium_busy(void);                                // Returns if the air is busy or not 
        
        extern uint8_t MAX_TRANSMISSIONS;                               // The maximum number of times the radio will attempt to transmit a packet
        extern bool TX_MODE_ENABLED;
        extern bool CS_BUSY;
#endif