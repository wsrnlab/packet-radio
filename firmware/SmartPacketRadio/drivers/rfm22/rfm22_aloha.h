
#ifndef _RFM22_ALOHA_H_
#define _RFM22_ALOHA_H_

        /* Includes: */
        #include <avr/io.h>
        #include <avr/wdt.h>
        #include <avr/interrupt.h>
        #include <avr/power.h>
        
        #include <stdbool.h>

        /* Function Prototypes: */
        void rfm22_transmit_aloha(unsigned char*, unsigned char*);
        
//         extern bool VALID_ACK;                 // Flag to determine if a valid ACK message has been received
#endif
