
/** \file
 *
 *  This file contains the ALOHA implementation for the SPR using the RFM22B transceiver.
 * 
 *  Author(s):
 *      Jordan Guest [jordannguest(at)gmail.com]
 *      Nick D'Ademo [nickdademo(at)gmail.com]
 *      Benjamin Foo [bsmf91(at)gmail(dot)com]
 *      Felix Shzu-Jursachek [fjuraschek(at)gmail(dot)com]
 */

#include <math.h>
#include <stdlib.h>
#include "rfm22_aloha.h"
#include "rfm22.h"
#include "SmartPacketRadio.h"

//==========================================//
//Function to transmit packets in Aloha mode//
//==========================================//
void rfm22_transmit_aloha(unsigned char* tx_buffer, unsigned char * rx_buffer)
{
    uint8_t delay_count;
    // Aloha transmission code. NOTE: layer 2 frame starts from tx[4] (the first two bytes are useless, tx[3] is the API identifier)
    uint8_t transmissions;
    long long int timeout_count = 0;
    long long int timeout_limit = 0;
    long long int backoff_number;
    long int random_number_max;
    char *stats_packet; // Statistics packet returned to higher layers
    long unsigned int mult;
    
    PORTC ^= 0x01;

    VALID_ACK = false;  // Resets flag
    transmissions = 0;  // Resets counter
    
    // Resetting the timer here
    DELAY_INTRS = 0;
    TCNT2 = 0;
    TCCR2B = 0x01; //Enables delay timer
    
    // Loop runs while there is not a valid ACK and the maximum number of transmissions is not reached
    while(!VALID_ACK && (transmissions < MAX_TRANSMISSIONS))
    {
        // Transmitting
        TRANSMITTING = true;
        rfm22_tx_mode();
        rfm22_raw_transmit(tx_buffer, true);
        
        // Waits for confirmation of successful transmission
        while(TRANSMITTING)
        {
            rfm22_irq_handler(rx_buffer);
        }
        timeout_count = 256*DELAY_INTRS + TCNT2; // Current value of delay timer
        transmissions +=1;

        // If this is a broadcast message, continue without waiting for ACK
        if(tx_buffer[0] == 0xFF)
        {
            VALID_ACK = true;
			break;
        }

        // Waiting for ACK
        timeout_limit = timeout_count + 3220; // Waits here for ~100ms
        while((timeout_count < timeout_limit) && !VALID_ACK)
        {
            rfm22_irq_handler(rx_buffer);
            if(MAC_PACKET_FLAG){
                rfm22_receive(rx_buffer, &tx_buffer[2]);
                MAC_PACKET_FLAG = false;
            }
            timeout_count = 256*DELAY_INTRS + TCNT2; // Rechecks value of interrupts
        }

        // Random back-off timer for retransmissions
        if(!VALID_ACK && (transmissions < MAX_TRANSMISSIONS))
        {
            timeout_count = 256*DELAY_INTRS + TCNT2;
            // Sets the maximum number of slot times to back off for
            if(transmissions < (BACKOFF_LIMIT + 1))
            {
                //random_number_max = pow(2, transmissions) - 1;
                random_number_max = (1 << transmissions) - 1;
            }
            else
            {
                //random_number_max = pow(2, BACKOFF_LIMIT+1) - 1;
                random_number_max = (1 << (BACKOFF_LIMIT+1)) - 1;
            }
    
            //Random number generator (from StackOverflow)
            do
            {
                mult = rand()/(RAND_MAX/(random_number_max + 1) );
            }
            while(mult > random_number_max);
            
            backoff_number = mult * SLOT_TIME + timeout_count;
            while(timeout_count < backoff_number)
            {
                timeout_count = 256*DELAY_INTRS + TCNT2;
            }
        }
    }

    TCCR2B = 0x00;          // Stops delay timer
    delay_count = TCNT2;    // Reads the timer

    // If no ACK received, set transmissions = 0 to indicate failure to send
    if(!VALID_ACK)
    {
        transmissions = 0;  //Indicates error
    }
    
    //Creates and 'sends' statistics 'packet' to itself
	
	stats_packet = (char *) rx_buffer;
    stats_packet[0] = USB_PACKET_STATS_RX; // Identifies packet as statistics packet
    stats_packet[1] = SOURCE_ADDRESS;
    stats_packet[2] = SOURCE_ADDRESS;
    stats_packet[3] = 0;
    stats_packet[4] = 6;
    stats_packet[5] = SOURCE_ADDRESS;
    stats_packet[6] = SOURCE_ADDRESS;
    stats_packet[7] = transmissions;
    stats_packet[8] = delay_count;
	*(&stats_packet[9]) = DELAY_INTRS;

    // 'Sending' statistics 'packet'
	CDC_Device_SendData(&VirtualSerial_CDC_Interface, stats_packet, 13);
}
