/*
             LUFA Library
     Copyright (C) Dean Camera, 2011.

  dean [at] fourwalledcubicle [dot] com
           www.lufa-lib.org
*/

/*
  Copyright 2011  Dean Camera (dean [at] fourwalledcubicle [dot] com)

  Permission to use, copy, modify, distribute, and sell this
  software and its documentation for any purpose is hereby granted
  without fee, provided that the above copyright notice appear in
  all copies and that both that the copyright notice and this
  permission notice and warranty disclaimer appear in supporting
  documentation, and that the name of the author not be used in
  advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.

  The author disclaim all warranties with regard to this
  software, including all implied warranties of merchantability
  and fitness.  In no event shall the author be liable for any
  special, indirect or consequential damages or any damages
  whatsoever resulting from loss of use, data or profits, whether
  in an action of contract, negligence or other tortious action,
  arising out of or in connection with the use or performance of
  this software.
*/

/** \file
 *
 *  Header file for SmartPacketRadio.c.
 */

#ifndef _SMARTPACKETRADIO_H_
#define _SMARTPACKETRADIO_H_

        /* Includes: */
        #include <avr/io.h>
        #include <avr/wdt.h>
        #include <avr/interrupt.h>
        #include <avr/power.h>

        #include "Descriptors.h"

        #include <LUFA/Version.h>
        #include <LUFA/Drivers/USB/USB.h>
        #include <LUFA/Drivers/Peripheral/SPI.h>
        
        //===============================//
        //Set bit and clear bit functions//
        //===============================//
        #define sbi(var, mask) ((var) |= (uint8_t)(1 << mask))  // Set bit function
        #define cbi(var, mask) ((var) &= (uint8_t)~(1 << mask)) // Clear bit function

        //=================//
        //RFM22 Pin Defines//
        //=================//
        #define CS      0       // PB0 - Chip select pin for RFM22B
        #define TXANT   1       // PC1 - Sets the antenna to be used for transmission   
        #define RXANT   2       // PC2 - Sets the antenna to be used for receiving

        /* Function Prototypes: */
        void SetupHardware(void);
		void ProcessPacket(unsigned char*);  

        void EVENT_USB_Device_Connect(void);
        void EVENT_USB_Device_Disconnect(void);
        void EVENT_USB_Device_ConfigurationChanged(void);
        void EVENT_USB_Device_ControlRequest(void);
		

        void EVENT_CDC_Device_LineEncodingChanged(USB_ClassInfo_CDC_Device_t* const CDCInterfaceInfo);

        extern unsigned char SOURCE_ADDRESS;                            // Source address of board (defaults to 0xFF)
        extern bool TXRX_LED_ON;
        extern bool TRANSMITTING;
        extern bool MAC_PACKET_FLAG; 
        extern unsigned char MAC_MODE; 
        extern int SLOT_TIME_ARRAY[];                                   // Array of preset slot time values (15 ms increments). (Set via trial and error testing)
        extern int SLOT_TIME;                                           // The slot time in timer counts. Default is ~75ms.
        extern int BACKOFF_LIMIT;                                       // 2^BACKOFF_LIMIT - 1 is the max value of the back-off timer (default value = 6)
        extern bool VALID_ACK;                                          // Flag to determine if a valid ACK message has been received
        extern volatile long int  DELAY_INTRS;                          // Used in timer 2 for timeouts
        extern bool TP_TEST;
        extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
#endif