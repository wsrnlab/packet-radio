
/** \file
 *
 *  Main source file for the Smart Packet Radio project. This file contains the main tasks of
 *  the project and is responsible for the initial application hardware configuration.
 *
 *  Author(s):
 *      Jordan Guest [jordannguest(at)gmail.com]
 *      Nick D'Ademo [nickdademo(at)gmail.com]
 *      Benjamin Foo [bsmf91(at)gmail(dot)com]
 *      Felix Shzu-Jursachek [fjuraschek(at)gmail(dot)com]
 */

#include "SmartPacketRadio.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/boot.h>
#include <stdio.h>
#include <stdbool.h>
#include <util/delay.h>

#include <math.h>
#include <stdlib.h>

#include "drivers/ADXL345/ADXL345.h"
#include "drivers/ds18b20/ds18b20.h"
#include "drivers/rfm22/rfm22.h"
#include "drivers/rfm22/rfm22_aloha.h"
#include "drivers/rfm22/rfm22_csmaca.h"
#include "tools/throughput_test.h"
#include "USB/USB_packet.h"

//===================//
//Function Prototypes//
//===================//
void AVR_Init(void);                                                // Initialises pins on the microcontroller


//================//
//Global Variables//
//================//
volatile long int  DELAY_INTRS = 0;                                                 // Used in timer 2 for timeouts
uint8_t INTRS = 0;                                                                  // Used to flash blue MCU LED

unsigned char MAC_MODE = false;                                                       // Flag to indicate if in byte-by-byte mode or ALOHA mode (default is byte-by-byte)
bool TRANSMITTING = false;                                                          // Flag to indicate radio is currently transmitting
bool VALID_ACK = false;                                                             // Flag to determine if a valid ACK message has been received
bool MAC_PACKET_FLAG = false;  
uint8_t ADXL345_INT_FLAG = 0x00;                                                   // Flag to determine if a packet has been received in ALOHA mode

bool TXRX_LED_ON = false;

unsigned char MAC_PROTOCOL;
unsigned char rssi_max = 0; 
// volatile long int T1_OFLOW = 0;                                                 // Used in timer 2 for timeouts

char version[14];																	// Will contain version number of the firmware

/** LUFA CDC Class driver interface configuration and state information. This structure is
 *  passed to all CDC Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface =
    {
        .Config =
            {
                .ControlInterfaceNumber         = 0,
                .DataINEndpoint                 =
                    {
                        .Address                = CDC_TX_EPADDR,
                        .Size                   = CDC_TXRX_EPSIZE,
                        .Banks                  = 1,
                    },
                .DataOUTEndpoint                =
                    {
                        .Address                = CDC_RX_EPADDR,
                        .Size                   = CDC_TXRX_EPSIZE,
                        .Banks                  = 1,
                    },
                .NotificationEndpoint           =
                    {
                        .Address                = CDC_NOTIFICATION_EPADDR,
                        .Size                   = CDC_NOTIFICATION_EPSIZE,
                        .Banks                  = 1,
                    },
            },
    };

/** Main program entry point. This routine contains the overall program flow, including initial
 *  setup of all components and the main program loop.
 */
int main(void)
{ 
    // Local variables
    static unsigned char tx_buffer[255];    // Transmission buffer
    static unsigned char rx_buffer[255];    // Receiver buffer
    bool seed_flag = false;
    
    // IRQ_TRIGGERED = false;
    SetupHardware();
    
    sei();
    
    rfm22_rx_mode();
    
    version[0] = USB_PACKET_REPLY_VERSION;	// Fill up the version details
    strcpy(version+1, "$Rev:: 457 $");
    
    for (;;)  { // Event loop (never exits)
        rfm22_irq_handler(rx_buffer);

        if (MAC_PACKET_FLAG) {
            rfm22_receive(rx_buffer, rx_buffer);
            MAC_PACKET_FLAG = false;
        }

        if(!TRANSMITTING) {
            int16_t received_byte = CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
            //cbi(PORTC, 4);                      
            //int16_t received_byte = 0;         
            if (!(received_byte < 0)) {
                // Seed timer (implemented here to allow for a bit of 
		// randomness)
                if (seed_flag == false) {
                    srand(TCNT0);   // Seeds random number generator 
		                    // with value in timer 0
                    seed_flag = true;
                }
		// Send the received byte to the API handler to see 
		// what to do with the packet
                API_Handler(received_byte, tx_buffer, rx_buffer);
            }
        }
        
	if(ADXL345_INT_FLAG) {
	    API_Handler(0x03, tx_buffer,rx_buffer);
	    ADXL345_INT_FLAG = 0x00;
	}
		
        CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
        USB_USBTask();
    }
}

//==========================================//
//Function to handle instructions to the MCU//
//==========================================//

/** Configures the board hardware and chip peripherals for the functionality. */
void SetupHardware(void)
{
    /* Disable watchdog if enabled by bootloader/fuses */
    MCUSR &= ~(1 << WDRF);
    wdt_disable();

    /* Disable clock division */
    clock_prescale_set(clock_div_1);

    /* Hardware Initialization */
    USB_Init();
    AVR_Init();
    rfm22_init();
	ADXL345_Init();
    
    /* Start the flush timer so that overflows occur rapidly to push received bytes to the USB interface */
    TCCR0B = (1 << CS02);
}

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
    cbi(PORTC, 6); // Set RGB LED1 Blue - USB Enumerating
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
    cbi(PORTC, 4);  // Set RGB LED1 Red - USB Disconnected
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
    bool ConfigSuccess = true;

    ConfigSuccess &= CDC_Device_ConfigureEndpoints(&VirtualSerial_CDC_Interface);
    
    if (ConfigSuccess)
    {
            sbi(PORTC, 6);
            cbi(PORTC, 5);  // Set RGB LED1 Green - USB Ready
    }
    else
    {
            cbi(PORTC, 4);  // Set RGB LED1 Red - USB Error
    }
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
    CDC_Device_ProcessControlRequest(&VirtualSerial_CDC_Interface);
}

/** Event handler for the CDC Class driver Line Encoding Changed event.
 *
 *  \param[in] CDCInterfaceInfo  Pointer to the CDC class interface configuration structure being referenced
 */
void EVENT_CDC_Device_LineEncodingChanged(USB_ClassInfo_CDC_Device_t* const CDCInterfaceInfo)
{
	
}

void AVR_Init(void)
{
    // Initialises SPI for communication with RFM22B module
    SPI_Init(SPI_SPEED_FCPU_DIV_2 | SPI_ORDER_MSB_FIRST | SPI_SCK_LEAD_RISING |
         SPI_SAMPLE_LEADING | SPI_MODE_MASTER);

    DDRB |= 0xF1;       // Enable SS as an output
    PORTB |= 0x80;      // Pull SS high
    PORTB &= !(0x10);   // Enables RFM22 chip
    
    DDRE = 0x00;
    PORTE = 0x40;       // Enable pull-up on PE6 to be used for interrupt
    
	DDRD &= ~(0x03);	// Enable pull-up on the INT1 and INT2 lines from the ADXL345
	PORTD |= 0x03;
	
    DDRC = 0x77;        // RGB LED output pins LED1(PC4-R, PC5-G, PC6-B) LED2(PC0 - B) Used for USB Status
    PORTC = 0x71;       // Output High

    DDRA = 0x00;        // Port A used for input
    PORTA = 0x40;
    
    DDRE |= 0x03;       // RGB LED output pins LED2(PE0 - R, PE1 - G)
    PORTE |= 0x03;      // Output high
    
    // External Interrupt(s) initialization
    // INT6 Mode: Falling Edge
    // Sets pin PE6 to receive neg edge interrupt from RFM22B module
    EICRA=0x03;
    EICRB=0x20;
    EIMSK=0x41;
    EIFR=0x41;

    // Enable timers and timer overflow interrupts
    //TCCR0B = 0x00;  
    TCCR1B = 0x00;
    TCCR3B = 0x01;
    TCCR2B = 0x01;
    ASSR = 0x20;
    
    TIMSK0 |= _BV(TOIE0);
    TIMSK1 = 0x01;
    TIMSK3 = 0x01;
    TIMSK2 = 0x01;
    //TCNT0 = 0;  
    TCNT2 = 0;
}

//=============================================//
//Timer overflow interrupts used to control LEDs//
//=============================================//
ISR(TIMER3_OVF_vect)
{
    // Timer3 overflow used to toggle the MCU health LED

    //unsigned char value = rfm22_read(0x26);
    //if (value > rssi_max)
    //rssi_max = value;
        
    INTRS++;    
    if (INTRS >= 60)
    {
        //CDC_Device_SendByte(&VirtualSerial_CDC_Interface, rssi_max);  
        PORTB ^= (1 << 7);
        INTRS = 0;
        
    }
    // turn TX/RX LEDs off
    if(TXRX_LED_ON) {
        PORTB &= ~(0x01 << 0x05);
        PORTB &= ~(0x01 << 0x06);
        TXRX_LED_ON = false;
    }
	
}

ISR(TIMER0_OVF_vect)
{

}

ISR(TIMER1_OVF_vect)
{

}

ISR(TIMER2_OVF_vect)
{
    // Timer 2 used to measure time to complete transmission
    DELAY_INTRS++;
}

ISR(INT0_vect)
{
	ADXL345_readReg(&ADXL345_INT_FLAG, 1, ADXL345_REG_INT_SOURCE);
}
