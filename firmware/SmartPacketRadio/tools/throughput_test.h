
#ifndef _THROUGHPUT_TEST_H_
#define _THROUGHPUT_TEST_H_

	/* Includes: */
	#include <avr/io.h>
	#include <avr/wdt.h>
	#include <avr/interrupt.h>
	#include <avr/power.h>

	#include <stdbool.h>

	extern bool TP_TEST;

	/* Function Prototypes: */
	void throughput_test_start(unsigned char*);
	void throughput_test_send_packet(void);
	void throughput_test_send_stats(uint8_t);

#endif