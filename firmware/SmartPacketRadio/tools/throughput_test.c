/** \file
 *
 *  This file contains functions to measure the achievable throughput using the RFM22B transceiver.
 * 
 *  In a first, version 1000 packets are generated and send as fast as possible. The delay between
 *  the first and last packet is measured with timer2. The result in form of ticks and overflows
 *  of timer2 is sent over the serial interface.
 * 
 *  Author(s):
 *      Jordan Guest [jordannguest(at)gmail.com]
 *      Nick D'Ademo [nickdademo(at)gmail.com]
 *      Benjamin Foo [bsmf91(at)gmail(dot)com]
 *      Felix Shzu-Jursachek [fjuraschek(at)gmail(dot)com]
 */

#include "throughput_test.h"
#include <stdlib.h>

#include "../drivers/rfm22/rfm22.h"
#include "SmartPacketRadio.h"

bool TP_TEST = false;                   // Flag to indicate a throughput testing
int TP_TEST_PACKETS = 0;                // Number of remaining packets to sent.
unsigned char* TP_TEST_TX_BUFFER;       // Pointer to the tx_buffer of the main loop to send the same package multiple times.

//========================================================//
// Tests the achievable throughput by sending 1000 packets //
//========================================================//
void throughput_test_start(unsigned char* tx_buffer)
{
    TP_TEST = true;
    TP_TEST_PACKETS = 1000;
    TP_TEST_TX_BUFFER = tx_buffer;
    ProcessPacket(tx_buffer);
    
    // Resetting the timer here
    DELAY_INTRS = 0;
    TCNT2 = 0;
    TCCR2B = 0x01; //Enables delay timer
    
    throughput_test_send_packet();
}
void throughput_test_send_packet()
{
    TRANSMITTING = true;
    rfm22_tx_mode();
    rfm22_raw_transmit(TP_TEST_TX_BUFFER, true);
    TP_TEST_PACKETS--;
    /*
    if(TP_TEST_PACKETS % 5 == 0) {
        PORTC ^= 0x01;
        _delay_ms(100);
    }*/
    if(TP_TEST_PACKETS == 0) {
        TCCR2B = 0x00;          // Stops delay timer
        TP_TEST = false;
        throughput_test_send_stats(TCNT2);
    } 
}

void throughput_test_send_stats(uint8_t delay_ticks)
{
    unsigned char stats_packet[5];
    stats_packet[0] = delay_ticks;
    
    if(DELAY_INTRS < 100 && DELAY_INTRS > 90)
        PORTC ^= 0x01;		// Invert the blue channel of LED0;
    
    *(&stats_packet[1]) = DELAY_INTRS;
    
    // 'Sending' statistics 'packet'
    CDC_Device_SendData(&VirtualSerial_CDC_Interface, (char *) stats_packet, 5);
}