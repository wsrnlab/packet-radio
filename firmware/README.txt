Updated SPR firmware with Vibration Sensor Support
By Moses Wan (17 January 2014)

Things that are new:
- Doxygen documents
- Drivers for the vibration sensor
- Cut down code to reduce memory

=================================================
NOTES:

- Windows project is up to date
- Makefile in the project has not been tested for functionality


- To generate the documentation, you need to have doxygen installed
on your system:
-- In terminal: sudo apt-get install doxygen
- The configuration file for Doxygen is Doxyfile.dox
- Then to generate the documentation
-- cd into the SmartPacketRadio/firmware
-- doxygen Doxyfile.dox
- When creating doxygen documentation, to define the page order, make it 
       in alphabetical order.
- When accessing the Doxygen documentation, open up "index.html" first. 
	It is the main page for the documentation.

