#ifndef _PKLIB_H
#define _PKLIB_H

#include "../firmware/SmartPacketRadio/USB/USB_packet_defs.h" 

#define MAX_SEGMENT_SIZE   64
#define HEADER_SIZE        6		
#define PAYLOAD_SIZE       (MAX_SEGMENT_SIZE - HEADER_SIZE - 1)
#define OVERHEAD_SIZE      3
#define LENGTH_BYTE_OFFSET 5			// DEBUG (WAS LENGTH_BYTE_OFFSET 4)
#define ACK_LENGTH         6

/* Status Codes */
#define STATUS_NON_INIT    0x0
#define STATUS_IDLE        0x04
#define STATUS_RADIO_READY 0x01
#define STATUS_USER_READY  0x02
#define STATUS_WATING_ACK  0x10

/* Macros */
#define SET_STATUS_NON_INIT(x) (x|=STATUS_NON_INIT)
#define SET_STATUS_IDLE(x) (x|=STATUS_IDLE)
#define SET_STATUS_RADIO_READY(x) (x|=STATUS_RADIO_READY)
#define SET_STATUS_USER_READY(x) (x|=STATUS_USER_READY)
#define SET_STATUS_WATING_ACK(x) (x|=STATUS_WATING_ACK)

#define UNSET_STATUS_NON_INIT(x) (x&= ~STATUS_NON_INIT)
#define UNSET_STATUS_IDLE(x) (x&= ~STATUS_IDLE)
#define UNSET_STATUS_RADIO_READY(x) (x&= ~STATUS_RADIO_READY)
#define UNSET_STATUS_USER_READY(x) (x&= ~STATUS_USER_READY)
#define UNSET_STATUS_WATING_ACK(x) (x&= ~STATUS_WATING_ACK)
    
#define IS_STATUS_NON_INIT(x) (x&STATUS_NON_INIT)
#define IS_STATUS_IDLE(x) (x&STATUS_IDLE)
#define IS_STATUS_RADIO_READY(x) (x&STATUS_RADIO_READY)
#define IS_STATUS_USER_READY(x) (x&STATUS_USER_READY)
#define IS_STATUS_WATING_ACK(x) (x&STATUS_WATING_ACK)

#define BAUDRATE B115200

/*DEBUG MACROS*/

#define PACKET 1
#define RECEIVED_PACKET 2
#define SENT_PACKET 3


/* Typedef for the USB Packet Structure for sending tranmission packets */
typedef struct _packet {
    unsigned char src;
    unsigned char dest;
    unsigned char control;
    unsigned char length;
    unsigned char txNode;
    unsigned char rxNode;
    char payload[PAYLOAD_SIZE];
} Packet;

typedef Packet *PacketPtr;
typedef unsigned char Status;

void debugPackage(PacketPtr pk,char *buffer,int type);		// DEBUG FUNCTION TO CHECK PACKETS
void debugReceivedPacket(char *buffer,int type);		// DEBUG FUNCTION TO CHECK DIFFERENT TYPES OF PACKETS

int transmit(PacketPtr packet, int fd);
int chars2pk(PacketPtr pk, char *buffer);
int create_packet(int dest, int src, char control, const char *payload, PacketPtr);
int is_ack(PacketPtr);
void pk2chars(PacketPtr packet, char *buffer);
#endif

