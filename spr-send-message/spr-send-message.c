// $Id: packet_radio.c,v 1.3 2014/04/23 16:32:54 ahmet Exp ahmet $
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <pthread.h>
#include <strings.h>
#include <string.h>
#include <glib.h>
#include <unistd.h>
#include <ctype.h>
#include "debug.h"
#include "pklib.h"

// Function prototypes
static void process_command_line(int, char **);
static void print_help(int, char **);
static void initialize_board(void);
static void *receive(void *);
static void *user_input(void *);
static void check_recv_que(Status *);
static void check_user_que(Status *);

// File scope global variables
static int spr_port = 0; 
static int board_addr = 0;
static int dest_addr = 0;
static Status status = STATUS_NON_INIT;
static GQueue recv_que;
static GQueue user_que;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int main(int argc, char **argv) {

    process_command_line(argc, argv);
    
    // Initialize the file descriptor, terminal device and SPR
    initialize_board();
   
    // Create independent threads to execute functions concurrently:
    // Radio packet reception and user keyboard input threads
    pthread_t recv_th, user_th;
    char *message1 = "from radio receiver thread ";
    char *message2 = "from user keyboard input thread";
    int iret1, iret2;
    PRINTFDB(("Creating the threads.\n"));
    iret1 = pthread_create(&recv_th, NULL, receive, (void*) message1);
    iret2 = pthread_create(&user_th, NULL, user_input, (void*) message2);

    PRINTFDB(("Main thread: Entering the while loop.\n"));
    while (TRUE) {     

	// Main thread: checks the queues in round robin
	// fashion
	// Mutex is for preventing a queue accessed simultaneously
	// by two separate threads
	pthread_mutex_lock(&mutex);		
	if (IS_STATUS_RADIO_READY(status)) {
	    // (At least) one packet is in the receive queue
	    PacketPtr pk = (PacketPtr) malloc(sizeof(Packet));
	    char *buf = (char*) g_queue_pop_tail(&recv_que);				// DEBUG (char*)
	   
            printf(" -- RECEIVED PACKET (QUEUE) -- \n");
	    debugReceivedPacket(buf,(int)*(buf));
	    //debugPackage(NULL,buf,RECEIVED_PACKET);
	    
	    // If the second byte of the received packet is 0x7E, 
	    // it is a statistics packet


	    if (*(buf) == USB_PACKET_STATS_RX) { 					// DEBUG *(buf + 1)
		printf("Stats: ");
		// print the retransmission field.
		printf("transmission: %d\n", *(buf +10)); 
	    } else {
		// Change the buffer back to packet format and 
		// check if it is an ACK packet
		//chars2pk(pk, buf);							// DEBUG
		if (IS_STATUS_WATING_ACK(status) && is_ack(pk)) {
		    UNSET_STATUS_WATING_ACK(status);
		} else {
		    // PRINT_PK(pk); 							// DEBUG				    			
		   
		}
	    }
	    
	    free(buf);
	    free(pk);
	    // Check if receive queue is empty
	    check_recv_que(&status);		
	    // Free up access to the receive queue
	    pthread_mutex_unlock(&mutex);	
	    continue;
	} else if (IS_STATUS_USER_READY( status )) {
	    //prepare & transmit
	    PacketPtr pk = (PacketPtr) malloc(sizeof(Packet));
	    if (pk) {
		// Pop the message off the User Input Queue
		char *msg = (char*) g_queue_pop_tail(&user_que);

		create_packet(dest_addr, board_addr, 0, msg, pk);
		// Create the packet with all the relevant fields filled in
		free(msg);
	    }
	    // Send the packet
	    int ret = transmit(pk, spr_port);	    

	    //printf(" -- CORRECT PACKET -- \n");						// DEBUG
	    //debugPackage(pk,NULL,PACKET);						// DEBUG
	    
	    // Check if the user input queue is empty
	    check_user_que(&status);		
	    if (ret) {
		SET_STATUS_WATING_ACK(status);
	    } else {
		exit(ret);
	    }
	    free(pk);
	    pthread_mutex_unlock(&mutex);	
	    continue;
	}
	pthread_mutex_unlock(&mutex);
	usleep(50);		// Sleep for 50us
    }
    pthread_join(recv_th, NULL);
    pthread_join(user_th, NULL);
    printf("%d\t%d\n", iret1, iret2);
    exit(0);
} // main()


// Function for the receiver thread
// Runs concurrently with the user_input thread. 
void *receive(void *ptr) {

    printf("Receive...\n");	
    while (!STATUS_NON_INIT) {
	char *buf = (char *) calloc(MAX_SEGMENT_SIZE + 1, sizeof(char));
	if (read(spr_port, buf, HEADER_SIZE)) {
		
            printf("PACKET TYPE: %X\n",*(buf));						// DEBUG
	    if ((*(buf) & 0xFF) == USB_PACKET_STATS_RX) {				// DEBUG [WAS *(buf +1);]
		printf("STATISTICS PACKET \n");						// DEBUG 
		// This is a stats
		read(spr_port, (buf + HEADER_SIZE), 8); // Read another 8 bytes
	    } else if ((*(buf + LENGTH_BYTE_OFFSET) > 0) && ((*(buf) & 0xFF) == USB_PACKET_VALID_RX)) {				
		int len = *(buf + LENGTH_BYTE_OFFSET) + 1;				// DEBUG[WAS OFFSET - 1]
		int count = 0;
		
		printf(" -- VALID PACKET -- \n");					// DEBUG
		
		while(count < len) {
		    count += read(spr_port, buf + HEADER_SIZE + count, len);
		    printf("Value: %X\n",*(buf + HEADER_SIZE + count));			// DEBUG
		}

	    } else if((*(buf) & 0xFF) == USB_PACKET_REPLY_ADXL345) {
		printf(" -- QUERY ADXL345 PACKET -- \n");				// DEBUG
		read(spr_port, (buf + HEADER_SIZE), 1); 				// Read another 1 byte
	    
            } else if((*(buf) & 0xFF) == USB_PACKET_REPLY_DS18B20){
		printf(" -- QUERY DS18B20 PACKET -- \n");				// DEBUG
											// TODO
		
	    } else if((*(buf) & 0xFF) == USB_PACKET_REPLY_VERSION){
		printf(" -- QUERY FIRMWARE VERSION PACKET -- \n");			// DEBUG
		read(spr_port, (buf + HEADER_SIZE), 8); 				// Read another 7 bytes
					

	    } else {
		buf = NULL;								// TODO
		printf(" -- UNKNOWN PACKET -- \n");					// DEBUG
		
	    }
	    


	    // Gain sole access to the queues
	    pthread_mutex_lock(&mutex);				
	    // Add the received message to the receive queue
	    g_queue_push_head(&recv_que, buf);		
	    // Flag the radio as ready
	    SET_STATUS_RADIO_READY(status);			
	    // Free up access to the queues
	    pthread_mutex_unlock(&mutex);			
	} // if
    } // while
    return NULL;
} // receive()

// Function for the user input thread
// Runs cocurrently with the receive thread. 
void *user_input(void *ptr) {
	
	printf("User input...\n");
    while (!STATUS_NON_INIT) {
	char *message = (char*)malloc(PAYLOAD_SIZE * sizeof(char));
	char *messageBig = (char*)malloc(1024 * sizeof(char));
	
	// When the user has pushed in a message, copy it into another area
	// of the stack then push the access to the user input queue
	if (fgets(messageBig, 1024, stdin)) {
	    strncpy(message, messageBig, PAYLOAD_SIZE);
	    pthread_mutex_lock(&mutex);

	    g_queue_push_head(&user_que, message);
	    SET_STATUS_USER_READY(status);
	    pthread_mutex_unlock(&mutex);
	}
	free(messageBig);
    }
    return NULL;
} // user_input()

void initialize_board(void) {
    // Initialize:
    // 1. file descriptor
    // 2. the terminal device to talk to the radio
    // 3. smart packet radio board

    // Do not modify the code in this function. 
    // There are no user serviceable parts below. 

    char port_name[80] = "\0";
    sprintf(port_name, "/dev/radio%d", board_addr);
    spr_port = open(port_name, O_RDWR);
    if(spr_port == -1) {
	printf("Did not find device\n");
	exit(EXIT_FAILURE);
    }
    PRINTFDB(("Packet radio board is connected: %s\n", port_name));
    struct termios options;

    tcgetattr(spr_port, &options);
    cfmakeraw(&options); 

    options.c_cc[VTIME] = 0; // How long to wait for input before returning
                             // (units of deciseconds). 
    options.c_cc[VMIN] =  2; // Minimum number of characters for read() to
                             // return. 
    tcsetattr(spr_port, TCSANOW, &options);
    tcflush(spr_port, TCIFLUSH);

   // Reset the SPR board
    char reset_spr[] = {USB_PACKET_RESET};
    write(spr_port, reset_spr, sizeof(reset_spr));

    // Initialize SPR board address
    char set_address[] = {USB_PACKET_SET_ADDRESS, board_addr};
    write(spr_port, set_address, sizeof(set_address));
    
    // Enable direct mode (no MAC) 
    char set_mac[] = {USB_PACKET_SET_TX_MODE, SET_DIRECT_MODE};
    write(spr_port, set_mac, sizeof(set_mac));

    // Inquire about the firmware revision number
    char inquire_fw_version[] = {USB_PACKET_QUERY_VERSION};
    write(spr_port, inquire_fw_version, sizeof(inquire_fw_version));

    // Read the response from the packet radio
    char *revision_id = 
	(char *) calloc(USB_PACKET_QUERY_VER_SIZE + 1, sizeof(char));
    read(spr_port, revision_id, USB_PACKET_QUERY_VER_SIZE);
    printf("Monash WSRNLab Packet Radio Firmware %s\n", revision_id);

    free(revision_id);									// DEBUG
} // initialize_board()

// Queue checking functions
void check_recv_que(Status *status) {
    if (g_queue_is_empty(&recv_que)) {
	UNSET_STATUS_RADIO_READY(*status);		
    }
} // check_recv_queue()

void check_user_que(Status *status) {
	if (g_queue_is_empty(&user_que)) {
		UNSET_STATUS_USER_READY(*status);		
	}
} // check_user_queue()

void process_command_line(int argc, char **argv)
{
    // Dealing with options provided through the command line
    int c;
    while ((c = getopt(argc, argv, "a:d:h")) != -1) {
	switch (c) {
	case 'a': // -a option for setting the device number
	    board_addr = strtol(optarg, NULL, 10);	
	    break;
	case 'd': // -d option for setting the destination
	    dest_addr = strtol(optarg, NULL, 10);	
	    break;
	case 'h': 
	case '?': 
	default:
	    if (optopt == 'a' || optopt == 'd')	{
		fprintf(stderr, "Option -%c requires an argument.\n", optopt);
		print_help(argc, argv);
	    } else if (isprint(optopt)) {
		fprintf(stderr, "Unknown option `-%c'.\n", optopt);
		print_help(argc, argv);
	    } else {
		fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
		print_help(argc, argv);
	    }
	    exit(EXIT_FAILURE);
	    break;
	} // switch
    } // while
    if ((board_addr == 0) || (dest_addr == 0)) {
	fprintf(stderr, "You need to provide the device and \
                                                destination addresses.\n");
	print_help(argc, argv);
	exit(EXIT_FAILURE);
    }
} // process_command_line()

void print_help(int argc, char **argv) {
    printf("Usage: %s -a <board_addr> -d <dest_addr> where \n", argv[0]);
    printf("       board_addr is the address of the packet radio (0 < board_addr < 255)\n");
    printf("       dest_addr is the destination to talk to (0 < dest_addr < 255)\n");
}
