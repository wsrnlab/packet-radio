#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <stdlib.h>
#include "pklib.h"


/*
 * Given payload is a string.
 * Return the length of packet.
 */
int create_packet(int dest, int src, char control, const char *payload,
		PacketPtr ptr) {

	ptr->dest = dest;
	ptr->src = src;
	ptr->control = control;
	ptr->rxNode = 0xff;
	ptr->txNode = 0xff;
	ptr->length = strlen(payload);
	strncpy(ptr->payload, payload, PAYLOAD_SIZE);
	return ptr->length;

}

/**
 *	Function used to send an instruction over USB with the
 *	correct packet formatting
 */
int transmit(PacketPtr packet, int fd) {
	static int counter = 0;
	char *bytes, *current;
	current = calloc(OVERHEAD_SIZE + MAX_SEGMENT_SIZE, sizeof(char));
	bytes = current;
	if (bytes) {

		*(current++) = USB_PACKET_TRANSMIT;
		*(current++) = packet->dest;
		*(current++) = packet->length + HEADER_SIZE;

		pk2chars(packet, current);
		
		 printf(" -- SENT PACKET -- \n");			        		// DEBUG
		 debugPackage(NULL,bytes,SENT_PACKET);						// DEBUG

		int n = write(fd, bytes, OVERHEAD_SIZE + MAX_SEGMENT_SIZE);
		if (n < 0) {
			printf("Error when writing to the board.");
			exit(n);
		}
		free(bytes);
		current = bytes = NULL;
	}
	return ++counter;
}

/**
 *	Function used to translate a char array into a Packet Structure
 *
 */
int chars2pk(PacketPtr pk, char *buffer) {
	
	buffer++;		
	pk->dest =    *(buffer++);
	pk->src =     *(buffer++);
	pk->control = *(buffer++);
	pk->length =  *(buffer++);
	pk->txNode =  *(buffer++);
	pk->rxNode =  *(buffer++);
	strncpy(pk->payload, buffer, pk->length + 1);

	

	return 1;
}

/**
 *	Function used check if the packet is an ACK packet
 *
 */

int is_ack(PacketPtr pk) {
	return pk->control & 0x90;
}

/**
 *	Function used to translate a Packet Structure into a char array
 *
 */
void pk2chars(PacketPtr packet, char *buf) {
	sprintf(buf, "%c%c%c%c%c%c", packet->dest, packet->src, packet->control,
			packet->length, packet->txNode, packet->rxNode);
	buf += HEADER_SIZE;
	int i;
	for (i = 0; i < PAYLOAD_SIZE; i++) {
		*(buf + i) = *(packet->payload + i);
	}
}

// DEBUG
void debugReceivedPacket(char *buffer,int type){
	type = (type & 0xFF);

	switch(type){
	     case USB_PACKET_VALID_RX:
		printf("Type of packet:    %X\n", 	  *(buffer) 	& 0xFF);
		printf("Packet length:     %X\n", 	  *(buffer + 1) & 0xFF);		    
		printf("Destination:       %X\n", 	  *(buffer + 2) & 0xFF);
		printf("Source: 	   %X\n",         *(buffer + 3) & 0xFF);
		printf("Control:           %X\n",	  *(buffer + 4) & 0xFF);
		printf("Payload length:    %X\n", 	  *(buffer + 5) & 0xFF);
		printf("RX_node: 	   %X\n",         *(buffer + 6) & 0xFF);
		printf("TX_node: 	   %X\n", 	  *(buffer + 7) & 0xFF);
		printf("Message: 	   %s\n",	   (buffer + 8));
		break;
             case USB_PACKET_STATS_RX:
		printf("Type of packet:    %X\n", 	  *(buffer) 	& 0xFF);
		printf("Source 1:          %X\n", 	  *(buffer + 1) & 0xFF);		    
		printf("Source 2:          %X\n", 	  *(buffer + 2) & 0xFF);
		printf("0x00: 	           %X\n",         *(buffer + 3) & 0xFF);
		printf("0x06:              %X\n",	  *(buffer + 4) & 0xFF);
		printf("Source 3:          %X\n", 	  *(buffer + 5) & 0xFF);
		printf("DELAY: 	  	   %X\n",         *(buffer + 6));
		break;
	     case USB_PACKET_REPLY_ADXL345:
		printf("Type of packet:    %X\n",         *(buffer) 	& 0xFF);
		printf("DATAX0:     	   %X\n", 	  *(buffer + 1) & 0xFF);		    
		printf("DATAX1:            %X\n", 	  *(buffer + 2) & 0xFF);
		printf("DATAY0: 	   %X\n",         *(buffer + 3) & 0xFF);
		printf("DATAY1:            %X\n",	  *(buffer + 4) & 0xFF);
		printf("DATAZ0:            %X\n", 	  *(buffer + 5) & 0xFF);
		printf("DATAZ1: 	   %X\n",         *(buffer + 6) & 0xFF);
		break;
	     case USB_PACKET_REPLY_DS18B20:
		printf("Type of packet:    %X\n",         *(buffer) 	& 0xFF);
		printf("DIGIT:     	   %X\n", 	  *(buffer + 1) & 0xFF);		    
		printf("DECIMAL1:            %X\n", 	  *(buffer + 2) & 0xFF);
		printf("DECIMAL0: 	   %X\n",         *(buffer + 3) & 0xFF);
		break;
	     case USB_PACKET_REPLY_VERSION:
		printf("Type of packet:    %X\n",         *(buffer) 	& 0xFF);
		printf("FIRMWARE VERSION:  %s\n", 	   (buffer + 1));		    
		break;
	     default: printf("ERRO!\n");
	}
}

// DEBUG (FUNCTION INCLUDED TO CHECK THE SEND AND RECEIVED PACKETS
void debugPackage(PacketPtr pk,char *buffer,int type){
	
	switch(type){
	     case PACKET:
		
		printf("Dest:    %X\n", pk->dest);
		printf("Source:  %X\n", pk->src);
		printf("Control: %X\n", pk->control);		    
		printf("Length:  %X\n", pk->length);
		printf("RX_NODE: %X\n", pk->txNode);
		printf("TX_NODE: %X\n", pk->rxNode);
		puts(pk->payload);		

		break;
	
	     // Packet to be transmitted
	     case SENT_PACKET:
		printf("Type of packet:    %X\n", 	  *(buffer)     & 0xFF);
		printf("Destination:       %X\n", 	  *(buffer + 1) & 0xFF);
		printf("Packet length:     %X\n", 	  *(buffer + 2) & 0xFF);		    
		printf("Destination:       %X\n", 	  *(buffer + 3) & 0xFF);
		printf("Source: 	   %X\n",         *(buffer + 4) & 0xFF);
		printf("Control:           %X\n",	  *(buffer + 5) & 0xFF);
		printf("Payload length:    %X\n", 	  *(buffer + 6) & 0xFF);
		printf("RX_node: 	   %X\n",         *(buffer + 7) & 0xFF);
		printf("TX_node: 	   %X\n", 	  *(buffer + 8) & 0xFF);
		printf("Message: %s\n",(buffer + 9));		
	
		
		break;
	     
             //Packet to be received
	     case RECEIVED_PACKET:
		
		printf("Type of packet:    %X\n", *(buffer) 		& 0xFF);
		printf("Packet length:     %X\n", 	  *(buffer + 1) & 0xFF);		    
		printf("Destination:       %X\n", 	  *(buffer + 2) & 0xFF);
		printf("Source: 	   %X\n",         *(buffer + 3) & 0xFF);
		printf("Control:           %X\n",	  *(buffer + 4) & 0xFF);
		printf("Payload length:    %X\n", 	  *(buffer + 5) & 0xFF);
		printf("RX_node: 	   %X\n",         *(buffer + 6) & 0xFF);
		printf("TX_node: 	   %X\n", 	  *(buffer + 7) & 0xFF);
		printf("Message: %s\n",(buffer + 8));
		
		break;
	     
		default: printf("DEBUG FAILED!");
	}

}
