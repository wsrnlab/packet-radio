/** Temperature Sensor Activity Example
 *
 *	This file contains example code to interface with the 
 *	Smart Packet Radio for reading the temperature sensor values
 *
 *	Authors:
 *		Moses Wan
 *              Ahmet Sekercioglu
 * $Revision$
 */

#include <fcntl.h>
#include <stdlib.h>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <strings.h>
#include <ctype.h>
#include "../firmware/SmartPacketRadio/USB/USB_packet_defs.h" 
#include "debug.h" 

// Function prototypes
static void process_command_line(int, char **);
static void print_help(int, char **);
static void initialize_board(void);
static float read_temperature_sensor(void);

// File scope global variables
static int spr_port = 0; 
static int board_addr = 0;

int main(int argc, char **argv) {
    float current_temp;
    int i = 0;

    process_command_line(argc, argv);
    initialize_board();
    for(;;) {
	current_temp = read_temperature_sensor();
	printf("%f\n", current_temp);
	// Change the color of LED1 every time we read a value
	char set_led_color[] = {USB_PACKET_LED, LED1, i++%8};
	write(spr_port, set_led_color, sizeof(set_led_color));
	sleep(1);
    } 
    return(EXIT_SUCCESS);
} // main()

float read_temperature_sensor(void)
{
    int chars_read;
    unsigned char response[USB_PACKET_REPLY_DS18B20_SIZE] = {0}; 
    unsigned char query_temp_sensor[] = {USB_PACKET_QUERY_DS18B20};
    float temperature;

    // Query the temperature sensor
    write(spr_port, query_temp_sensor, 1); 
    // Read the USB port for received information
    chars_read = read(spr_port, response, USB_PACKET_REPLY_DS18B20_SIZE);	
    if (chars_read > 0) {
	// If there is information in the USB port and it is a reply packet 
	if (response[0] == 0xF2) {
	    // Temperature sensor reply (see documentation for the format)
	    PRINTFDB(("sensor reply: %d %X %X %X %X %X\n", 
                       response[1], response[2], response[3], 
                       response[4], response[5], response[6]));
	    temperature = response[1];
	    temperature += ((float) ((response[2] >> 3) & 0x01)) * 0.5 +
		                    ((response[2] >> 2) & 0x01)  * 0.25 +
		                    ((response[2] >> 1) & 0x01)  * 0.125 +
		                    ((response[2] >> 1) & 0x01)  * 0.0625;  
	} else {
	    printf("Response is not recognized\n");
	    temperature = -100.0;
	} 
    } else {
	PRINTFDB(("chars_read = %d\n", chars_read));
	perror("Invalid response");
	temperature = -100.0;
    }	
    return(temperature);
} // read_temperature_sensor()

void initialize_board(void) {
    // Initialize:
    // 1. file descriptor
    // 2. the terminal device to talk to the radio
    // 3. smart packet radio board

    // Do not modify the code in this function. 
    // There are no user serviceable parts below. 

    char port_name[80] = "\0";
    sprintf(port_name, "/dev/radio%d", board_addr);
    spr_port = open(port_name, O_RDWR);
    if(spr_port == -1) {
	printf("Did not find device\n");
	exit(EXIT_FAILURE);
    }
    PRINTFDB(("Packet radio board is connected: %s\n", port_name));
    struct termios options;

    tcgetattr(spr_port, &options);
    cfmakeraw(&options); 

    options.c_cc[VTIME] = 0; // How long to wait for input before returning
                             // (units of deciseconds). 
    options.c_cc[VMIN] =  2; // Minimum number of characters for read() to
                             // return. 
    tcsetattr(spr_port, TCSANOW, &options);

    // Flush needs some time to clear the buffers (Renan)
    usleep(200000); 
    tcflush(spr_port, TCIOFLUSH);

    // Reset the SPR board
    char reset_spr[] = {USB_PACKET_RESET};
    write(spr_port, reset_spr, sizeof(reset_spr));
    
    // Initialize SPR board address
    char set_address[] = {USB_PACKET_SET_ADDRESS, board_addr};
    write(spr_port, set_address, sizeof(set_address));
    
    // Enable direct mode (no MAC) 
    char set_mac[] = {USB_PACKET_SET_TX_MODE, SET_DIRECT_MODE};
    write(spr_port, set_mac, sizeof(set_mac));

    // Inquire about the firmware revision number
    char inquire_fw_version[] = {USB_PACKET_QUERY_VERSION};
    write(spr_port, inquire_fw_version, sizeof(inquire_fw_version));

    // Read the response from the packet radio
    char *revision_id = 
	(char *) calloc(USB_PACKET_QUERY_VER_SIZE + 1, sizeof(char));
    read(spr_port, revision_id, USB_PACKET_QUERY_VER_SIZE);
    printf("Monash WSRNLab Packet Radio Firmware %s\n", revision_id);
} // initialize_board()

void process_command_line(int argc, char **argv)
{
    // Dealing with options provided through the command line
    int c;
    while ((c = getopt(argc, argv, "a:h")) != -1) {
	switch (c) {
	case 'a': // -a option for setting the board address
	    board_addr = strtol(optarg, NULL, 10);	
	    break;
	case 'h': 
	case '?': 
	default:
	    if (optopt == 'a')	{
		fprintf(stderr, "Option -%c requires an argument.\n", optopt);
		print_help(argc, argv);
	    } else if (isprint(optopt)) {
		fprintf(stderr, "Unknown option `-%c'.\n", optopt);
		print_help(argc, argv);
	    } else {
		fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
		print_help(argc, argv);
	    }
	    exit(EXIT_FAILURE);
	    break;
	} // switch
    } // while
    if (board_addr == 0) {
	fprintf(stderr, "You need to provide the board address.\n");
	print_help(argc, argv);
	exit(EXIT_FAILURE);
    }
} // process_command_line()

void print_help(int argc, char **argv) {
    printf("Usage: %s -a <board_addr> where \n", argv[0]);
    printf("       board_addr is the address of the packet radio (0 < board_addr < 255)\n");
}


