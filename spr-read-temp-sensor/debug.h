/* $Id: debug.h,v 1.1 2014/04/23 14:53:11 ahmet Exp $ */

#ifdef DEBUG
#define PRINTFDB(x) printf x
#else
#define PRINTFDB(x) /* */
#endif

/* sample usage:
   main()
   {
     int i=5,j=6;
     printf("hello1 i=%d\n", i);
     PRINTFDB(("debug i=%d j=%d \n", i, j));
   }
*/
